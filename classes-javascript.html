<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="X-UA-Compatible" content="ie=edge" /><meta name="description" content="Classes in Javascript allow a more object-oriented approach, creating instances with methods and properties." /><!-- Bing --><meta name="msvalidate.01" content="45CBBE1BD8265A2217DFDA630EB8F84A" /><title>Tiny Brain Fans - Classes (Javascript)</title><link rel="stylesheet" href="tinystyle.css" /><link rel="stylesheet" href="prism.css" /></head><body class="theme">
<main id="main"><article id="content"><h1 id="title">Classes (Javascript)</h1><p><a href="object-oriented-programming.html">Classes</a> in <a href="javascript.html">Javascript</a> generally are created with two different sections:</p>
<ol>
<li>The <code>prototypes</code> or <code>methods</code> will be how all the instances of the class should be <strong>similar</strong> (things like <code>addYear</code> for a person class). This is commonly done in the <code>prototype</code> object.</li>
<li>The constructor will define the properties and how the instances are <strong>different</strong> (the <code>name</code> property of a person class). This is commonly done through the invocation of the constructor, like <code>function (name) { this.name = name }</code>.</li>
</ol>
<h2>Class Types</h2>
<h3>Decorators</h3>
<p>A decorator is a function that accepts an object and <strong>adds more properties or functionality</strong> to it. It's common to use <strong>adjectives</strong> as the names of these decorators. Decorators allow for more DRY code and help keep all elements together that should be, allowing little ambiguity in execution.</p>
<pre><code class="language-javascript">// this decorator makes obj more &#x27;carlike&#x27;
var carlike = function (obj, loc) {
  // Properties
  obj.loc = loc;
  // Methods
  obj.move = function () {
    obj.loc++;
  };
};

var newObj = {};
var newCar = carlike(newObj, 0);
</code></pre>
<h3>Functional Classes</h3>
<p>A functional class is a construct that is <strong>capable of making a fleet of objects</strong> that conform to the same interface. They are commonly capitalized in name. The functions that produce these functional classes are called <strong>constructors</strong>. The object that is returned is called an <strong>instance</strong> of that class.</p>
<p>The difference between a decorator and a functional class is that a decorator accepts their target object as input, whereas the class builds and returns the object it's augmenting.</p>
<pre><code class="language-javascript">var Car = function (loc) {
  // Properties
  var obj = {loc: loc};
  // Methods
  obj.move = function () {
    obj.loc++;
  }
  return obj;
}

var newCar = Car(0);
</code></pre>
<h3>Functional Classes with Shared Methods</h3>
<p>Using shared methods allows you to save space in memory, as only one instance of a method needs to exist, as opposed to one for every single instance.</p>
<pre><code class="language-javascript">// Methods
var carMethods = {
  move() {
    this.loc++;
  }
}

var Car = function (loc) {
  // Properties
  var obj = { loc: loc };
  // Methods
  return Object.assign(obj, carMethods);
}

var newCar = Car(0);
</code></pre>
<h3>Prototypal Classes</h3>
<p>Using <code>Object.create()</code>, we can create an object that inherits all properties of the enclosed object into its <code>prototype</code>. This will mean that on a failed lookup, it will search within this object for a reference, inheriting the old objects as they were at the time of inheritance.</p>
<pre><code class="language-javascript">var Car = function (loc) {
  var obj = Object.create(Car.prototype);
  // Properties
  obj.loc = loc;
  return obj;
}

// Methods
Car.prototype.move = function () {
  this.loc++;
};

var newCar = Car(0);
</code></pre>
<h3>Pseudoclassical Classes</h3>
<p>The pseudoclassical class is a <a href="javascript.html">Javascript</a> syntactic sugar that allows a more conventional style of object oriented programming to be implemented.</p>
<p>By running a function with the <code>new</code> keyword before it, the interpreter runs the program in a special &quot;construction&quot; mode. Since you will always want to be creating an object and returning it when you are finished, the <code>new</code> keyword adds these two lines at the beginning and end, respectively, to your function. The object created will be automatically bound to <code>this</code> and will use the <code>prototype</code> property found inside that function.</p>
<p>The two different parts of a pseudoclassical class are doing two distinct roles:</p>
<ol>
<li>the constructor is defining what is different about each instance. In this example, what <code>loc</code> is.</li>
<li>the methods are defining what is similar about each instance. In this example, what <code>move</code> does.</li>
</ol>
<pre><code class="language-javascript">// The commented lines in this function are being run &quot;under the hood&quot;
var Car = function (loc) {
  // var this = Object.create(Car.prototype);
  this.loc = loc;
  // copy all prototype methods to `this`
  // return this;
};

// Methods
Car.prototype.move = function () {
  this.loc++;
};

var newCar = new Car(0);
</code></pre>
<h3>ES6 Classes (<code>class</code>)</h3>
<p>The ES6 implementation is extremely similar to the pseudoclassical implementation, but uses more syntactic sugar to make it more readable and more similar to other object-oriented languages.</p>
<pre><code class="language-javascript">class Car {
  constructor(loc) {
    // var this = Object.create(Car.prototype);
    this.loc = loc;
    // copy all prototypes to `this`
    // return this;
  }

  move() {
    this.loc++;
  }
}

var newCar = new Car(0);
</code></pre>
<h2>Polymorphism</h2>
<p>Polymorphism is the design of objects to be able to share behaviors and override certain shared behaviors to work more specifically on the new object. There is a parent/super class and a child/sub class. The sub class inherits the properties and methods from the super class. In the following examples, the super is <code>Shape</code> and the sub is <code>Square</code> or <code>Triangle</code>.</p>
<h3>Functional Classes</h3>
<pre><code class="language-javascript">var Shape = function (name, sides, sideLength) {
  var obj = {
    name: name,
    sides: sides,
    sideLength: sideLength
  };

  // Abstract method
  obj.calcArea = function () {
    throw new Error(&#x27;Cannot calculate area of shape.&#x27;);
  };

  obj.calcPerimeter = function () {
    return obj.sides * obj.sideLength;
  };

  return obj;
};

// Equilateral Triangle
var Triangle = function (name, sideLength) {
  var obj = Shape(name, 3, sideLength);

  obj.calcArea = function () {
    return 0.25 * (Math.sqrt(3) * Math.pow(this.sideLength, 2));
  };

  return obj;
}

var newTriangle = Triangle(&#x27;triangle&#x27;, 10);

var Square = function (name, sideLength) {
  var obj = Shape(name, 4, sideLength);

  obj.calcArea = function () {
    return Math.pow(obj.sideLength, 2);
  };

  return obj;
}

var newSquare = Square(&#x27;square&#x27;, 5);
</code></pre>
<h3>Functional with Shared Methods</h3>
<pre><code class="language-javascript">var Shape = function (name, sides, sideLength) {
  var obj = {
    name: name,
    sides: sides,
    sideLength: sideLength
  };

  return Object.assign(obj, shapeMethods);
};

var shapeMethods = {
  calcArea: function () {
    throw new Error(&#x27;Cannot calculate area of shape.&#x27;);
  },
  calcPerimeter: function () {
    return this.sides * this.sideLength;
  }
};

var Triangle = function (name, sideLength) {
  var obj = Shape(name, 3, sideLength);
  return Object.assign(obj, triangleMethods);
}

var triangleMethods = {
  calcArea: function () {
    return 0.25 * (Math.sqrt(3) * Math.pow(this.sideLength, 2));
  }
};

var newTriangle = Triangle(&#x27;triangle&#x27;, 5);

var Square = function (name, sideLength) {
  var obj = Shape(name, 4, sideLength);
  return Object.assign(obj, squareMethods);
}

var squareMethods = {
  calcArea: function () {
    return Math.pow(this.sideLength, 2);
  }
};

var newSquare = Square(&#x27;square&#x27;, 5);
</code></pre>
<h3>Prototypal Classes</h3>
<p>Using <code>Object.create()</code>.</p>
<pre><code class="language-javascript">var Shape = function (name, sides, sideLength) {
  var obj = Object.create(Shape.prototype);
  obj.name = name;
  obj.sides = sides;
  obj.sideLength = sideLength;
  return obj;
};

Shape.prototype.calcArea = function () {
  throw new Error(&#x27;Cannot calculate area of shape.&#x27;);
};

Shape.prototype.calcPerimeter = function () {
  return this.sides * this.sideLength;
};

var Triangle = function (name, sideLength) {
  var obj = Object.create(Triangle.prototype);
  obj.name = name;
  obj.sides = 3;
  obj.sideLength = sideLength;
  return obj;
}

Triangle.prototype = Object.create(Shape.prototype);
Triangle.prototype.constructor = Triangle; // otherwise is `Shape`
Triangle.prototype.calcArea = function () {
  return 0.25 * (Math.sqrt(3) * Math.pow(this.sideLength, 2));
};

var newTriangle = Triangle(&#x27;Triangle&#x27;, 5);

var Square = function (name, sideLength) {
  var obj = Object.create(Square.prototype);
  obj.name = name;
  obj.sides = 4;
  obj.sideLength = sideLength;
  return obj;
}

Square.prototype = Object.create(Shape.prototype);
Square.prototype.constructor = Square; // otherwise is `Shape`
Square.prototype.calcArea = function () {
  return Math.pow(this.sideLength, 2);
};

var newSquare = Square(&#x27;square&#x27;, 5);
</code></pre>
<h3>Pseudoclassical Classes</h3>
<p>Using the <code>new</code> keyword.</p>
<pre><code class="language-javascript">var Shape = function (name, sides, sideLength) {
  // this = Object.create(Shape.prototype);
  this.name = name;
  this.sides = sides;
  this.sideLength = sideLength;
  // return this;
};

Shape.prototype.calcArea = function () {
  throw new Error(&#x27;Cannot calculate area of shape.&#x27;);
};

Shape.prototype.calcPerimeter = function () {
  return this.sides * this.sideLength;
};

var Triangle = function (name, sideLength) {
  this.name = name;
  this.sides = 3;
  this.sideLength = sideLength;
};

Triangle.prototype = new Shape();
Triangle.prototype.constructor = Triangle; // otherwise is `Shape`
Triangle.prototype.calcArea = function () {
  return 0.25 * (Math.sqrt(3) * Math.pow(this.sideLength, 2));
};

var newTriangle = new Triangle(&#x27;Triangle&#x27;, 5);

var Square = function (name, sideLength) {
  this.name = name;
  this.sides = 4;
  this.sideLength = sideLength;
};

Square.prototype = new Shape();
Square.prototype.constructor = Square; // otherwise is `Shape`
Square.prototype.calcArea = function () {
  return Math.pow(this.sideLength, 2);
};

var newSquare = new Square(&#x27;square&#x27;, 5);
</code></pre>
<h3>ES6 Classes</h3>
<p>To create a class that inherits all the properties of another class, use <code>extends</code> after defining the <code>class</code> name. You can utilize the parent's constructor by using <code>super()</code> with the arguments expected in the parent class.</p>
<pre><code class="language-javascript">class Shape {
  constructor (name, sides, sideLength) {
    this.name = name;
    this.sides = sides;
    this.sideLength = sideLength;
  }

  calcArea () {
    throw new Error(&#x27;Cannot calculate area of shape.&#x27;);
  }

  calcPerimeter () {
    return this.sides * this.sideLength;
  }
};

class Triangle extends Shape {
  constructor (name, sideLength) {
    super(name, 3, sideLength);
  }

  calcArea () {
    return 0.25 * (Math.sqrt(3) * Math.pow(this.sideLength, 2));
  }
}

var newTriangle = new Triangle(&#x27;Triangle&#x27;, 5);

class Square extends Shape {
  constructor (name, sideLength) {
    super(name, 4, sideLength);
  }

  calcArea () {
    return Math.pow(this.sideLength, 2);
  }
}

var newSquare = new Square(&#x27;square&#x27;, 5);
</code></pre>
<h2>Property Lookup/Prototype Chains</h2>
<p>If you are looking for a given property of an object, the interpreter will first look at the object itself, and if it fails on that lookup, will look at any other objects that are associated via prototype chain.</p>
<p>To have an ongoing prototype chain, where one object will always default on a failed lookup to searching within another object, you can use <code>var newObj = Object.create(oldObj)</code>. <code>newObj</code> will now default to looking up any failed lookups in <code>oldObj</code>. The values will be calculated during the lookup time, as the values are not stored or copied into the new object. The new object has a link to the old object and will perform on a lookup on the old object in its current state.</p>
<h2>References</h2>
<ol>
<li><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign" target="_blank">https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign</a></li>
<li><a href="http://underscorejs.org/#extend" target="_blank">http://underscorejs.org/#extend</a></li>
<li><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create" target="_blank">https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create</a></li>
<li><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes" target="_blank">https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes</a></li>
<li><a href="https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Test_your_skills:_Object-oriented_JavaScript" target="_blank">https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Test_your_skills:_Object-oriented_JavaScript</a></li>
<li><a href="https://stackoverflow.com/questions/44391149/es6-classes-ability-to-perform-polymorphism" target="_blank">https://stackoverflow.com/questions/44391149/es6-classes-ability-to-perform-polymorphism</a></li>
<li><a href="https://radialglo.github.io/blog/2014/11/24/understanding-pseudoclassical-inheritance-in-javascript/" target="_blank">https://radialglo.github.io/blog/2014/11/24/understanding-pseudoclassical-inheritance-in-javascript/</a></li>
</ol>
<section id="incoming"><details open><summary>Incoming Links</summary><ul><li><a href="object-oriented-programming.html">Object-Oriented Programming</a></li><li><a href="websites.html">Websites</a></li></ul></details></section><p class="last-modified">Last modified: 202401040446</p></article></main><footer><nav><p><a href="index.html">Home</a></p><ul><li>
Built using
<a href="http://codeberg.org/stringbone/swiki" rel="noopener"
>{{SWIKI}}</a
></li><li><a href="http://codeberg.org/stringbone/" rel="noopener"
>Codeberg</a
></li><li><a href="http://milofultz.com/" rel="noopener">milofultz.com</a></li><li><a href="https://merveilles.town/@milofultz" rel="me noopener"
>Mastodon</a
></li><li><a href="https://fediring.net/previous?host=milofultz.com">←</a>
&nbsp;<a href="https://fediring.net/">Fediring</a>
&nbsp;<a href="https://fediring.net/random">(random)</a>
&nbsp;<a href="https://fediring.net/next?host=milofultz.com">→</a></li></ul></nav></footer><script src="prism.js"></script></body></html>
