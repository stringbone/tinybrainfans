<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="X-UA-Compatible" content="ie=edge" /><meta name="description" content="The shell is the terminal of your operating system. This is the *nix shell." /><!-- Bing --><meta name="msvalidate.01" content="45CBBE1BD8265A2217DFDA630EB8F84A" /><title>Tiny Brain Fans - Shell</title><link rel="stylesheet" href="tinystyle.css" /><link rel="stylesheet" href="prism.css" /></head><body class="theme">
<main id="main"><article id="content"><h1 id="title">Shell</h1><p>The shell is the terminal of your operating system. This is the <code>*nix</code> shell.</p>
<h2>Executing Complex Commands In The Shell</h2>
<p>If you are writing out a more complex command, you have a few options to make it easier for you:</p>
<ol>
<li>You can use <code>&lt;c-x&gt;</code> followed by <code>&lt;c-e&gt;</code> to open your default editor and run the contents in your shell on close</li>
<li>You can use <code><a href="fc.html">fc</a></code> in the same way as above</li>
<li>You can make an alias or function in your rc file (e.g. <a href="zsh.html">zsh</a>'s <code>.zshrc</code>, or <a href="bash.html">bash</a>'s <code>.bashrc</code>), or shell script and execute that if you are going to need it multiple times or over a long period of time (see below)</li>
</ol>
<h2>Basic <a href="shell-tools.html">Shell Tools</a></h2>
<ul>
<li><code>cp [-r] ./source ./destination</code> - Copy file or directory <code>[-r]</code> from source to destination</li>
<li><code>mv [-r] ./source ./destination</code> - Move/rename file or directory <code>[-r]</code> from source to destination</li>
<li><code>rm [-r] ./file</code> - Remove file or directory <code>[-r]</code> <strong>NOTE: THIS IS PERMANENT. There are no trash cans or recycle bins here.</strong></li>
</ul>
<h2>Set Options[x]</h2>
<p>Using <code>set</code> to set or unset options in your shell script can solve a <strong>lot</strong> of problems. It is used like this example: <code>set -a</code>.</p>
<table>
<thead>
<tr>
<th>Option</th>
<th>Effect</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>-e</code> / <code>-o errexit</code></td>
<td>Stop running the script once an error is encountered</td>
</tr>
<tr>
<td><code>-u</code> / <code>-o nounset</code></td>
<td>Stop running the script once an unset variable is encountered</td>
</tr>
<tr>
<td><code>-o pipefail</code></td>
<td>Stop executing the rest of the pipe if any command fails</td>
</tr>
</tbody></table><p>These <code>set</code> args can be combined like this: <code>set -euo pipefail</code>.</p>
<blockquote>
<p>Have an explicit and informed decision about: <code>shopt -s nullglob</code>, <code>shopt -s extglob</code>, and <code>shopt -s globstar</code>.[26,27]</p>
</blockquote>
<h2>Aliases, Functions, and Scripts[28]</h2>
<p>Within your <code>~/.bashrc</code> file, you can use the keyword <code>alias</code> or a function to map a command to any other set of shell commands. Aliases and functions have different quirks[28], so be aware of them before choosing which to use.</p>
<p><strong>Your RC file</strong></p>
<pre><code class="language-bash">alias hello=&quot;say &#x27;Hello world!&#x27;&quot;

goodbye() {
  say &#x27;Goodbye world!&#x27;
}
</code></pre>
<p><strong>Your shell</strong></p>
<pre><code class="language-bash">$ hello   # says hello world
$ goodbye # says goodbye world
</code></pre>
<p>When complete, run <code>source ~/.bashrc</code> or <code>source ~/.zshrc</code> and restart bash to have them take effect.</p>
<h3>Shell Script</h3>
<p>The first line of your shell script should be a shebang (<code>#!</code>) followed by the path of the shell you want to use (<code>/bin/sh</code>). Then all shell commands should follow. Once this is created, you need to change the mode (<code>chmod</code>) of your shell script to executable (e.g. if you have <code>script.sh</code>, you might use <code>chmod u+x script.sh</code>, with <code>u+x</code> meaning make this file e<b>x</b>ecutable for <strong>u</strong>sers). To run your new shell script, you need to preface the script's filename with <code>./</code>.</p>
<pre><code class="language-bash">echo &quot;#!/bin/bash\n\necho &#x27;Hello world!&#x27;&quot; &gt; script.sh
chmod u+x script.sh
./script.sh
</code></pre>
<h2>Background Tasks[21]</h2>
<p>You can run tasks in the background within a <a href="terminal.html">terminal</a> window by placing an <code>&amp;</code> at the end of the command you want running. You can see these background jobs with <code>jobs</code> and kill the job that you want with <code>kill %</code> followed by the index, or just <code>kill %</code> to kill all jobs.</p>
<p>You can also bring these background tasks to the foreground by typing <code>fg %</code> followed by the index. Or send a suspended job to the background by typing <code>bg %</code> followed by the index.</p>
<p>To suspend a process, you can use <code>&lt;c-z&gt;</code> (for instance, in suspending a <a href="vim.html">Vim</a> process).</p>
<h2>Variables</h2>
<p>Variables are all defined by a non-spaced variable name followed by an equals sign. Variables are recalled/invoked using the dollar sign followed by the variable name.</p>
<p>Variables that are within a string and directly next to another character that is not a space need to be enclosed within a dollar sign and curly braces, e.g. <code>${var_name}</code>.</p>
<pre><code class="language-bash">var_name=&quot;Bob&quot;
number_var=123

echo &quot;$var_name&quot;
echo &quot;$number_var&quot;
echo &quot;There are $number_var cans in ${var_name}&#x27;s closet.&quot;
</code></pre>
<p>Single quotes will preserve the literal value of all characters within it, while double quotes will allow the expansion/interpolation of the variables.[10] <strong>Always use double quotes when expanding your variables.</strong> Using double quotes around a variable will ensure that it will not be split up like a series of arguments[11].</p>
<pre><code class="language-bash">var_name=&quot;Bob&quot;
number_var=123

literal_values=&#x27;$var_name $number_var&#x27;
expanded_values=&quot;$var_name $number_var&quot;

echo &quot;$literal_values&quot;  # $var_name $number_var
echo &quot;$expanded_values&quot; # Bob 123
</code></pre>
<h3>Parameter Expansions</h3>
<p>Variables in the shell also have a variety of parameter expansions[12] that allow pattern matching, replacement, string slicing, and more.</p>
<table>
<thead>
<tr>
<th>Symbol</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>${var:-rep}</code></td>
<td>If <code>var</code> is null or unset, replace it with literal <code>rep</code> or variable named <code>$rep</code> if preceded by a <code>$</code> (e.g. <code>${var:-$rep}</code>)[13]</td>
</tr>
<tr>
<td><code>${#var}</code></td>
<td>Length of <code>var</code> in bytes</td>
</tr>
<tr>
<td><code>${var#pattern}</code></td>
<td>Remove shortest match of <code>pattern</code> if at start of <code>var</code>.</td>
</tr>
<tr>
<td><code>${var##pattern}</code></td>
<td>Remove longest match of <code>pattern</code> if at start of <code>var</code>.</td>
</tr>
<tr>
<td><code>${var%pattern}</code></td>
<td>Remove shortest match of <code>pattern</code> if at end of <code>var</code>.</td>
</tr>
<tr>
<td><code>${var%%pattern}</code></td>
<td>Remove longest match of <code>pattern</code> if at end of <code>var</code>.</td>
</tr>
<tr>
<td><code>${var/pattern/replacement}</code></td>
<td>Replace first <code>pattern</code> with <code>replacement</code></td>
</tr>
<tr>
<td><code>${var//pattern/replacement}</code></td>
<td>Replace all <code>pattern</code>'s with <code>replacement</code></td>
</tr>
</tbody></table><h2>Conditionals</h2>
<p>There are a zillion operators to use in conditionals that are all slightly different than the usual <code>&lt;</code>, <code>&gt;</code>, <code>==</code>, etc.[14] But here is the general construction of if/then conditionals in shell.</p>
<p>The big footgun is that <code>-eq</code> is used for integers and <code>=</code>/<code>==</code> is used for string comparison.</p>
<pre><code class="language-bash"># If var_name is equal to other_var
if [[ &quot;$var_name&quot; -eq &quot;$other_var&quot; ]]; then
  # stuff happens
fi

# If var_name is NOT equal to other_var
if [[ ! &quot;$var_name&quot; -eq &quot;$other_var&quot; ]]; then
  # stuff happens
elif [[ &quot;$var_name&quot; -eq &quot;$another_different_var&quot; ]]; then
  # different stuff happens
else
  # other stuff happens
fi
</code></pre>
<p>There is also <code>while</code> and <code>until</code>, which executes until the exit status of the condition is non-zero or zero, respectively.</p>
<pre><code class="language-sh">i=3
j=3

while test $i -ne 0; do
    i=$((i - 1))
done
echo &quot;i is &#x27;$i&#x27;&quot; # i is &#x27;0&#x27;

until test $j -ne 0; do
    j=$((j - 1))
done
echo &quot;j is &#x27;$j&#x27;&quot; # j is &#x27;3&#x27;
</code></pre>
<h3><a href="regular-expressions-posix.html">Regular Expressions</a>[15]</h3>
<p>Regular expressions in shell use <a href="regular-expressions-posix.html">POSIX style</a> patterns. A check can be made by using <code>=~</code> between the variable and the pattern.</p>
<p>For instance, if we wanted to check if the date in a variable was between April 1 through April 3, we would use this:</p>
<pre><code class="language-bash">if [[ &quot;$var_name&quot; =~ &#x27;April [1-3]&#x27; ]]; then
  #stuff happens
fi
</code></pre>
<p>If you want to put the regex pattern into a variable, be sure to use single quotes in it's initialization and be sure to <strong>not</strong> use quotes around the variable when it's used. Double quotes disable the shell from recognizing it is a regex.[16]</p>
<pre><code class="language-bash">re=&#x27;April [1-3]&#x27; # use single quotes

# no quotes on $re
if [[ &quot;$var_name&quot; =~ $re ]]; then
  #stuff happens
fi
</code></pre>
<h2>Arrays[22]</h2>
<p>Arrays can be made in a shell script by using parentheses with spaces separating the elements.</p>
<pre><code class="language-bash">arr=(&quot;A&quot; &quot;B&quot; &quot;Cat&quot; &quot;Delaware&quot; 3)

# Subscripts ([index])
echo &quot;${arr[1]}&quot; # B

# Slices ([@]:start:length)
echo &quot;${arr[@]:2:1}&quot; # Cat Delaware

# Iterate
for element in &quot;${arr[@]}&quot;; do
  echo &quot;$element&quot;
done

# Iterate through script arguments
for arg in &quot;$@&quot;; do
  echo &quot;$arg&quot;
done

# Test if item is in array
value=&quot;A&quot;
if [[ &quot; ${arr[*]} &quot; =~ [[:space:]]${value}[[:space:]] ]]; then
    echo &quot;Found &#x27;$value&#x27;.&quot;
fi
</code></pre>
<p>This is also a useful way of saving and passing command line arguments to a function. The following would add the date as a markdown header to the diary file.</p>
<pre><code class="language-bash">args=(-c &quot;let @a=\&quot;\n## $(date +%Y%m%d)\n\n\n\&quot;&quot; -c &quot;silent put a&quot;)
$EDITOR diary.md -c &quot;$&quot; &quot;${args[@]}&quot;
</code></pre>
<h2>Iteration</h2>
<p>To iterate over a series of files, you can use a <code>for</code> loop:</p>
<pre><code class="language-bash"># Files in directory include: a.txt, b.jpg, c.exe, d.txt
# This will only iterate through a.txt and d.txt .

for file in *.txt; do
  echo -e &quot;${file}:\n$(cat $file)&quot;
done
</code></pre>
<p>To iterate over each line in a file:</p>
<pre><code class="language-bash">while read -r line; do
  do echo $line
done &lt; path/to/file
</code></pre>
<p>To iterate over numbers:</p>
<pre><code class="language-bash">for i in {0..10}; do
  echo -e &quot;The number ${i}!&quot;
done

# or

for ((i=0; i&lt;=10; i+=1)); do
  echo -e &quot;The number ${i}!&quot;
done
</code></pre>
<p>To iterate over an array:</p>
<pre><code class="language-bash">for name in John Jane Bailer Arin; do
    echo &quot;$name&quot;
done

arr=(John Jane Bailey Arin)

for name in &quot;${arr[@]}&quot;; do
    echo -e &quot;The name ${name}!&quot;
done
</code></pre>
<h2>Event Designators</h2>
<p>An event designator is a reference to a command line entry in the history list. Unless the reference is absolute, events are relative to the current position in the history list.</p>
<ul>
<li><code>$_</code> Repeat the last argument used, e.g. <code>mkdir folder-name &amp;&amp; cd &quot;$_&quot;</code></li>
<li><code>!!</code> Repeats the previous command</li>
<li><code>!10</code> Repeat the 10th command from the history</li>
<li><code>!-2</code> Repeat the 2nd command (from the last) from the history</li>
<li><code>!string</code> Repeat the command that starts with “string” from the history</li>
<li><code>!?string</code> Repeat the command that contains the word “string” from the history</li>
<li><code>^str1^str2^</code> Substitute str1 in the previous command with str2 and execute it</li>
<li><code>!!:$</code> Gets the last argument from the previous command.</li>
<li><code>!string:n</code> Gets the nth argument from the command that starts with “string” from the history.</li>
<li><code>!^</code> first argument of the previous command</li>
<li><code>!$</code> last argument of the previous command</li>
<li><code>!*</code> all arguments of the previous command</li>
<li><code>!:2</code> second argument of the previous command</li>
<li><code>!:2-3</code> second to third arguments of the previous command</li>
<li><code>!:2-$</code> second to last arguments of the previous command</li>
<li><code>!:2*</code> second to last arguments of the previous command</li>
<li><code>!:2-</code> second to next to last arguments of the previous command</li>
<li><code>!:0</code> the command itself</li>
</ul>
<h2>Redirection and Pipe</h2>
<p>To send the <code>STDOUT</code> of one <strong>command</strong> to a <strong>file</strong>, use the redirection operator <code>&gt;</code>. To append the <code>STDOUT</code> to a file, use two redirection operators <code>&gt;&gt;</code>.</p>
<p>To read a <strong>file</strong> into a <strong>command</strong> as <code>STDIN</code>, use the reverse redirection operator or &quot;less than&quot; operator, <code>&lt;</code>.</p>
<p>To connect the <code>STDOUT</code> of one <strong>command</strong> to the <code>STDIN</code> of <strong>another</strong> use the | symbol, commonly known as a pipe.</p>
<pre><code class="language-bash"># thing1 outputs data to STDOUT and thing2 takes in input from STDIN
# long way
$ thing1 &gt; tempfile
$ thing2 &lt; tempfile

# shorter
$ thing1 &gt; tempfile &amp;&amp; thing2 &lt; tempfile

# shortest
$ thing1 | thing2
</code></pre>
<h3>Routing Different Outputs</h3>
<p>To route <code>STDOUT</code> to different areas, you can precede the redirection operator with a <code>1</code>. To route <code>STDERR</code>, use <code>2</code>. To route both, use <code>&amp;</code>.</p>
<pre><code class="language-bash">$ cat file.txt 1&gt;output.txt # STDOUT
$ cat file.txt 2&gt;output.txt # STDERR
$ cat file.txt &amp;&gt;output.txt # Both
</code></pre>
<h2>Heredocs[30]</h2>
<p>You can use heredocs for multiline strings. It is signified using <code>&lt;&lt;</code> followed by the delimiter, often <code>EOF</code>; then the multiline string must be ended with a single line containing your delimiter. If you want to not allow parameter substitution, at the start, surround the delimiter with double quotes.</p>
<pre><code class="language-bash">cat &lt;&lt;EOF
This is a long string. It allows variables, like $HOME.

It can contain any characters, as long as it isn&#x27;t a line containing only the delimiter. Even special characters, like these:   
EOF

cat &lt;&lt;&quot;EOF&quot;
This doesn&#x27;t allow variables, like $HOME.
EOF
</code></pre>
<p>You can write a heredoc to a file using <code>tee</code>[31]:</p>
<pre><code class="language-bash">tee newfile &lt;&lt;EOF
line 1
line 2
line 3
EOF
</code></pre>
<h2>Command Substitution</h2>
<p>Placing an argument within backticks or <code>$(...)</code> will execute the command first and insert the result. e.g.</p>
<pre><code class="language-bash">$ vim `find &quot;start/path&quot; -name &quot;filename.txt&quot;`
$ # the same as
$ vim $(find &quot;start/path&quot; -name &quot;filename.txt&quot;)
$ # resolves to
$ vim &quot;start/path/filename.txt&quot;
</code></pre>
<h2>Using <a href="test-driven-development.html">TDD</a> with Shell Scripts</h2>
<h3>DIY</h3>
<p>Luckily, shell scripts are so simple and rely pretty much entirely on globals, so things like mocks, before each/all, and test runners are all pretty straightforward. If your script is simple, I think rolling your own tiny framework is a good solution. I made one sufficient for a project in a couple hours, but now I can use it for anything going forward, assuming the project and requirements are sufficiently simple.</p>
<h3>Third-Party</h3>
<p>There are many libraries that can help ensure your app is well tested and make development akin to other paradigms. The most promising I have seen is ShellSpec[17], but there are lots[18-19].</p>
<h2>Troubleshooting</h2>
<h3>Errors from Windows</h3>
<p>If you got a script that looks totally fine but is throwing errors that make very little to no sense, like failing <code>cd</code> and <code>cp</code>, it's probably containing <code>\r</code> from a Windows computer.</p>
<pre><code class="language-bash">tr -d &quot;\r&quot; &lt; oldname.sh &gt; newname.sh
</code></pre>
<h2>References</h2>
<ol>
<li><a href="https://www.serverlab.ca/tutorials/linux/administration-linux/how-to-base64-encode-and-decode-from-command-line/" target="_blank">https://www.serverlab.ca/tutorials/linux/administration-linux/how-to-base64-encode-and-decode-from-command-line/</a></li>
<li><a href="https://linuxize.com/post/how-to-create-bash-aliases/" target="_blank">https://linuxize.com/post/how-to-create-bash-aliases/</a></li>
<li><a href="https://askubuntu.com/questions/172982/what-is-the-difference-between-redirection-and-pipe/172989#172989?newreg=cfc8024a2d4b40daa24578e47df2b7cf" target="_blank">https://askubuntu.com/questions/172982/what-is-the-difference-between-redirection-and-pipe/172989#172989?newreg=cfc8024a2d4b40daa24578e47df2b7cf</a></li>
<li><a href="https://stackoverflow.com/a/11428439" target="_blank">https://stackoverflow.com/a/11428439</a></li>
<li><a href="https://unix.stackexchange.com/questions/19654/how-do-i-change-the-extension-of-multiple-files" target="_blank">https://unix.stackexchange.com/questions/19654/how-do-i-change-the-extension-of-multiple-files</a></li>
<li><a href="https://mywiki.wooledge.org/BashFAQ/030" target="_blank">https://mywiki.wooledge.org/BashFAQ/030</a></li>
<li><a href="https://devhints.io/bash" target="_blank">https://devhints.io/bash</a></li>
<li><a href="https://github.com/dylanaraps/pure-bash-bible" target="_blank">Pure Bash Bible</a></li>
<li><a href="https://linoxide.com/make-bash-script-executable-using-chmod/" target="_blank">https://linoxide.com/make-bash-script-executable-using-chmod/</a></li>
<li><a href="https://stackoverflow.com/a/6697781/14857724" target="_blank">https://stackoverflow.com/a/6697781/14857724</a></li>
<li><a href="https://guide.bash.academy/expansions/?=Command_Substitution#a1.3.0_2" target="_blank">https://guide.bash.academy/expansions/?=Command_Substitution#a1.3.0_2</a></li>
<li><a href="https://guide.bash.academy/expansions/?=Command_Substitution#p2.2.2_5" target="_blank">https://guide.bash.academy/expansions/?=Command_Substitution#p2.2.2_5</a></li>
<li><a href="https://stackoverflow.com/questions/27445455/what-does-the-colon-dash-mean-in-bash" target="_blank">https://stackoverflow.com/questions/27445455/what-does-the-colon-dash-mean-in-bash</a></li>
<li><a href="https://tldp.org/LDP/abs/html/comparison-ops.html#ICOMPARISON1" target="_blank">https://tldp.org/LDP/abs/html/comparison-ops.html#ICOMPARISON1</a></li>
<li><a href="https://stackoverflow.com/questions/18709962/regex-matching-in-a-bash-if-statement" target="_blank">https://stackoverflow.com/questions/18709962/regex-matching-in-a-bash-if-statement</a></li>
<li><a href="https://stackoverflow.com/questions/18709962/regex-matching-in-a-bash-if-statement#comment27568516_18709962" target="_blank">https://stackoverflow.com/questions/18709962/regex-matching-in-a-bash-if-statement#comment27568516_18709962</a></li>
<li><a href="https://github.com/shellspec/shellspec" target="_blank">https://github.com/shellspec/shellspec</a></li>
<li><a href="https://github.com/timurb/shell-test-frameworks" target="_blank">https://github.com/timurb/shell-test-frameworks</a></li>
<li><a href="https://thomaslevine.com/computing/shell-testing/" target="_blank">https://thomaslevine.com/computing/shell-testing/</a></li>
<li><a href="https://rhodesmill.org/brandon/2009/commands-with-comma/" target="_blank">https://rhodesmill.org/brandon/2009/commands-with-comma/</a></li>
<li><a href="https://www.maketecheasier.com/run-bash-commands-background-linux/" target="_blank">https://www.maketecheasier.com/run-bash-commands-background-linux/</a></li>
<li><a href="https://stackoverflow.com/a/18898718" target="_blank">https://stackoverflow.com/a/18898718</a></li>
<li><a href="https://www.shellcheck.net/" target="_blank">https://www.shellcheck.net/</a></li>
<li><a href="https://clig.dev/" target="_blank">https://clig.dev/</a></li>
<li><a href="https://wizardzines.com/comics/bash-errors/" target="_blank">https://wizardzines.com/comics/bash-errors/</a></li>
<li><a href="https://mastodon.social/@gnomon/108673882215603396" target="_blank">https://mastodon.social/@gnomon/108673882215603396</a></li>
<li><a href="https://www.gnu.org/software/bash/manual/html_node/The-Shopt-Builtin.html" target="_blank">https://www.gnu.org/software/bash/manual/html_node/The-Shopt-Builtin.html</a></li>
<li><a href="https://www.baeldung.com/linux/bash-alias-vs-script-vs-new-function" target="_blank">https://www.baeldung.com/linux/bash-alias-vs-script-vs-new-function</a></li>
<li><a href="https://github.com/dylanaraps/pure-sh-bible" target="_blank">Pure sh Bible</a></li>
<li><a href="https://tldp.org/LDP/abs/html/here-docs.html" target="_blank">Here Documents</a></li>
<li><a href="https://stackoverflow.com/a/17093489" target="_blank">Write heredoc to file</a></li>
</ol>
<section id="incoming"><details open><summary>Incoming Links</summary><ul><li><a href="ansi-escape-codes.html">ANSI Escape Codes</a></li><li><a href="bytebeat.html">Bytebeat</a></li><li><a href="crontab.html">crontab</a></li><li><a href="dotfiles.html">Dotfiles</a></li><li><a href="fc.html">fc</a></li><li><a href="glob.html">Glob</a></li><li><a href="low-no-tech-productivity.html">Low-/No-Tech Productivity</a></li><li><a href="my-git-aliases.html">My git Aliases</a></li><li><a href="php.html">PHP</a></li><li><a href="websites.html">Websites</a></li><li><a href="xargs.html">xargs</a></li></ul></details></section><p class="last-modified">Last modified: 202412060412</p></article></main><footer><nav><p><a href="index.html">Home</a></p><ul><li>
Built using
<a href="http://codeberg.org/stringbone/swiki" rel="noopener"
>{{SWIKI}}</a
></li><li><a href="http://codeberg.org/stringbone/" rel="noopener"
>Codeberg</a
></li><li><a href="http://milofultz.com/" rel="noopener">milofultz.com</a></li><li><a href="https://merveilles.town/@milofultz" rel="me noopener"
>Mastodon</a
></li><li><a href="https://fediring.net/previous?host=milofultz.com">←</a>
&nbsp;<a href="https://fediring.net/">Fediring</a>
&nbsp;<a href="https://fediring.net/random">(random)</a>
&nbsp;<a href="https://fediring.net/next?host=milofultz.com">→</a></li></ul></nav></footer><script src="prism.js"></script></body></html>
