<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="X-UA-Compatible" content="ie=edge" /><meta name="description" content="Authentication in web development is the way to verify that a given user is who they say they are." /><!-- Bing --><meta name="msvalidate.01" content="45CBBE1BD8265A2217DFDA630EB8F84A" /><title>Tiny Brain Fans - Authentication</title><link rel="stylesheet" href="tinystyle.css" /><link rel="stylesheet" href="prism.css" /></head><body class="theme">
<main id="main"><article id="content"><h1 id="title">Authentication</h1><h2>How It Works</h2>
<p>To authenticate that a user and their client are who they say they are, a few steps need to occur:</p>
<ol>
<li>User sends credentials (e.g. username and password) to server</li>
<li>Server verifies these credentials are accurate by checking their data store, usually via a test of the password's hash in their database[3]</li>
<li>Server returns to the client whether or not these credentials are accurate</li>
</ol>
<p>This is easy enough, but becomes unwieldy if the application needs you to verify your identity for various actions, as it will have to repeat these three steps for every request, which is very costly.</p>
<h3>Token</h3>
<p>One solution is to use a token protocol.</p>
<ol>
<li>User sends credentials to server</li>
<li>Server verifies the credentials are accurate by checking against the existing data in the database</li>
<li>Server creates a unique token with an optional expiration timestamp and stores it in a table of session rows</li>
<li>Server returns new token to client either via JSON body, as a <code>Bearer</code> token, or via cookie</li>
</ol>
<h4>Cookie</h4>
<p>If using a cookie for authentication, it is best to set an <code>HttpOnly</code> flag to avoid being accessed via remote or malicious <a href="javascript.html">Javascript</a>.</p>
<p>Cookies have a particular format for their <code>expires</code> field, using the <code>HTTP-date</code> or RFC5322[10]. This can also be accessed on a <a href="javascript.html">Javascript</a> <code>Date</code> object via <code>toUTCString()</code>. The date is formatted as follows:</p>
<p><code>foo=bar; expires=Sun, 06 Nov 1994 08:49:37 GMT; HttpOnly</code></p>
<h3>JWT[4]</h3>
<p>Another option is something like JWT, but note that this does have its issues[6]. This method stores three elements within it:</p>
<ol>
<li>Header - Identifies which algorithm is used to generate the signature</li>
<li>Payload - Contains data to be used for verification (e.g. username)</li>
<li>Signature - Securely validates the token</li>
</ol>
<p>This token takes a JSON structure and puts it into a string via base64 encoding. So now instead of having to send the username and password and check with the database every single request, we can verify that the token is accurate and contains the necessary information for the user to act (e.g. their username). The steps that occur now are a little different:</p>
<ol>
<li>User sends credentials (e.g. username and password) to server</li>
<li>Server verifies these credentials are accurate by checking their data store, usually via a test of the password's hash in their database</li>
<li>If accurate, server creates a JWT that proves this user is who they say they are</li>
<li>Server returns the JWT to the client</li>
</ol>
<p>For all further interactions:</p>
<ol>
<li>User sends JWT</li>
<li>Server verifies JWT is valid</li>
<li>Server performs requested action</li>
</ol>
<p>This is much simpler and less costly, as we only talk to the database one time. We don't need the password for every interaction since we verified it once that it is accurate and created a token that proves that on the server side.</p>
<h2>Cross Site Request Forgery (CSRF)</h2>
<blockquote>
<p>In a CSRF attack, the attacker's goal is to cause an innocent victim to unknowingly submit a maliciously crafted web request to a website that the victim has privileged access to.[7]</p>
</blockquote>
<p>This is done by utilizing a user's credentials or authorization tokens for site X while user is browsing site Y. Since the user has credentials for site X, site Y could have a form that will delete all privileged data that the user has access to, and if the user submits the form, then the request will be perceived as legitimate by the server, because the credentials are valid.</p>
<p>Some ways to avoid a CSRF attacki[8]:</p>
<ul>
<li>Use only JSON APIs (There is no way for a simple <code>&lt;form&gt;</code> to send JSON, so by accepting only JSON, you eliminate the possibility of the above form)</li>
<li>Disable CORS If you're going to allow CORS, only allow it on OPTIONS, HEAD, GET as they are not supposed to have side-effects. This doesn't apply to simple online forms, as they use no <a href="javascript.html">Javascript</a>, however.)</li>
<li>Check the referrer header (You could always block requests whose referrer headers are not from your site. This really isn't worth the trouble)</li>
<li>GET should not have side effects (GET requests should never do anything but retrieve data)</li>
<li>Avoid using POST (Because <code>&lt;form&gt;</code>s can only GET and POST, by using other methods like PUT, PATCH, and DELETE, an attacker has fewer methods to attack your site)</li>
<li>Don't use method override! (Many applications use method-override to use PUT, PATCH, and DELETE requests over a regular form. This, however, converts requests that were previously invulnerable vulnerable!)</li>
<li>Don't support old browsers (Old browsers do not support CORS or security policies. By disabling support for older browsers [which more technologically-illiterate people use, who are more (easily) attacked], you minimize CSRF attack vectors)</li>
<li>CSRF Tokens</li>
</ul>
<h3>CSRF Tokens[8]</h3>
<blockquote>
<p>How do CSRF tokens work?</p>
<ul>
<li>Server sends the client a token.</li>
<li>Client submits a form with the token.</li>
<li>The server rejects the request if the token is invalid.</li>
</ul>
<p>An attacker would have to somehow get the CSRF token from your site, and they would have to use JavaScript to do so. Thus, if your site does not support CORS, then there's no way for the attacker to get the CSRF token, eliminating the threat.</p>
<p>Make sure CSRF tokens can not be accessed with AJAX! Don't create a /csrf route just to grab a token, and especially don't support CORS on that route!</p>
<p>The token just needs to be &quot;unguessable&quot;, making it difficult for an attacker to successfully guess within a couple of tries. It does not have to be cryptographically secure. An attack is one or two clicks by an unbeknownst user, not a brute force attack by a server.</p>
</blockquote>
<h2>References</h2>
<ol>
<li><a href="https://kevin.burke.dev/kevin/things-to-use-instead-of-jwt/" target="_blank">https://kevin.burke.dev/kevin/things-to-use-instead-of-jwt/</a></li>
<li><a href="https://frontegg.com/blog/token-based-authentication" target="_blank">https://frontegg.com/blog/token-based-authentication</a></li>
<li><a href="https://auth0.com/blog/hashing-passwords-one-way-road-to-security/" target="_blank">https://auth0.com/blog/hashing-passwords-one-way-road-to-security/</a></li>
<li><a href="https://jwt.io/" target="_blank">https://jwt.io/</a></li>
<li><a href="https://en.wikipedia.org/wiki/JSON_Web_Token" target="_blank">https://en.wikipedia.org/wiki/JSON_Web_Token</a></li>
<li><a href="https://redis.com/blog/json-web-tokens-jwt-are-dangerous-for-user-sessions/" target="_blank">https://redis.com/blog/json-web-tokens-jwt-are-dangerous-for-user-sessions/</a></li>
<li><a href="https://en.wikipedia.org/wiki/Cross-site_request_forgery" target="_blank">https://en.wikipedia.org/wiki/Cross-site_request_forgery</a></li>
<li><a href="https://github.com/pillarjs/understanding-csrf" target="_blank">https://github.com/pillarjs/understanding-csrf</a></li>
<li><a href="https://github.com/pillarjs/csrf" target="_blank">https://github.com/pillarjs/csrf</a></li>
<li><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toUTCString" target="_blank">https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toUTCString</a></li>
</ol>
<p class="last-modified">Last modified: 202401040446</p></article></main><footer><nav><p><a href="index.html">Home</a></p><ul><li>
Built using
<a href="http://codeberg.org/stringbone/swiki" rel="noopener"
>{{SWIKI}}</a
></li><li><a href="http://codeberg.org/stringbone/" rel="noopener"
>Codeberg</a
></li><li><a href="http://milofultz.com/" rel="noopener">milofultz.com</a></li><li><a href="https://merveilles.town/@milofultz" rel="me noopener"
>Mastodon</a
></li><li><a href="https://fediring.net/previous?host=milofultz.com">←</a>
&nbsp;<a href="https://fediring.net/">Fediring</a>
&nbsp;<a href="https://fediring.net/random">(random)</a>
&nbsp;<a href="https://fediring.net/next?host=milofultz.com">→</a></li></ul></nav></footer><script src="prism.js"></script></body></html>
