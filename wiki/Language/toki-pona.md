---
title: Toki Pona
description: Toki Pona is a conlang with less than 150 words that is easy to learn.
---

## References

1. https://devurandom.xyz/tokipona/
2. http://nimi.li/
3. {{Anki}} [deck for learning the words and the glyphs](https://ankiweb.net/shared/info/1548566798)
