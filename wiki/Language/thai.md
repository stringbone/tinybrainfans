---
title: Thai
description: Thai is a tonal language and the native language of Thailand.
---

Thai is a tonal {{language}} and the native language of Thailand.

## Basics

Note that all of this should be a reference! As Thai is tonal, you should be listening to Thai to fully grok it. Check the references below for good Youtube videos to help you.

### Wai

The _wai_ is a gesture of respect, wherein you place your hands together at your chest, as if in prayer, and bow your head towards your thumb. It is often used in greetings and saying thank you.

> However, greeting people isn’t the only reason to Wai. Other reasons include
>
> -   To apologize
> -   To say Thank you
> -   To say goodbye
> -   In prayer
> -   Listening to monks
> -   Receive blessings
> -   Show and earn respect

_From [Thai With Grace](https://thaiwithgrace.com/thai-wai/)_

### Kráp/Kâ

The words _kráp/khráp_ and _kâ/khâ_ are multi-use words in Thai, where _kráp_ is used by men and _kâ_ for women. _Kráp_ can also sound like _krup_ or _kup_.

They can be used as ways to soften a phrase or question for politeness (think situations like speaking with store owners, waiters, reception at a hotel, elders, etc.), placed at the end of the statement.

> Informal: "Sawat-dee" (Hello/Goodbye)

> Formal: "Sawat-dee kráp" (Hello/Goodbye)

They can also be used as a word of acknowledgement or agreement, using it almost as an English "uh huh", "okay", or "yes".

### Basics

Remember that the politness particles above can be used here.

| English   | Thai                                                  | Script | Notes                                                                                                                     |
| --------- | ----------------------------------------------------- | ------ | ------------------------------------------------------------------------------------------------------------------------- |
| Yes       | [Châi](http://www.thai-language.com/id/131142)        | ใช่    | or _khrap/kha_, depending                                                                                                 |
| No        | [Mâi](http://www.thai-language.com/id/131129)         | ไม่    |
| Sorry     | [Khǎaw-thôot](http://www.thai-language.com/id/197305) | ขอโทษ  |
| Thank you | [Khàawp-khun](http://www.thai-language.com/id/196672) | ขอบคุณ | [There are many ways to say thank you in Thai](http://www.thai-language.com/id/590449/), including _khrap/kha_, depending |

## Greetings

| English       | Thai                                                         | Script    | Notes                        |
| ------------- | ------------------------------------------------------------ | --------- | ---------------------------- |
| Hello/Goodbye | [Sawat-dee/Sawaddee](http://www.thai-language.com/id/200149) | สวัสดี    | Accompanied often by a _wai_ |
| How are you?  | [Sá-baai-dee mǎi](http://thai-language.com/id/212425)        | สบายดีไหม |
| I'm fine      | [Sá-baai-dee](http://thai-language.com/id/197447)            | สบายดี    |

## References

-   http://www.thai-language.com/
-   https://iwillteachyoualanguage.com/blog/learn-to-speak-thai
-   https://www.youtube.com/user/ThaiPod101
-   https://omniglot.com/writing/thai.htm
-   https://slice-of-thai.com/tones/
-   https://www.expatden.com/thai/resources/learning-thai/
-   http://www.seasite.niu.edu/Thai/QuickThai/Intro.htm
-   https://www.youtube.com/watch?v=qSh8kITz8eI
-   https://mythailand.blog/2016/03/03/kao-jai-mai-understand/

### Letters/Tones

-   https://www.activethai.com/study-thai/reading-and-writing/learning-the-thai-vowels/
-   http://thai-language.com/ref/tone-rules
-   https://www.youtube.com/watch?v=zaO-aXuxzEI

### Numbers

-   https://www.youtube.com/watch?v=8UvP1TXz4s8
-   https://www.youtube.com/watch?v=VY3T4ZC91iY

### Kráp/Kâ

-   https://www.thethailandlife.com/krap-ka

### Phrases

-   https://www.youtube.com/watch?v=Gkj5AlcxmUE
-   https://www.youtube.com/watch?v=ZbnpsnBBczM
-   https://www.youtube.com/watch?v=J6zpns1A08c
-   https://omniglot.com/language/phrases/thai.php
