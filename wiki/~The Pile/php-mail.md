https://code.tutsplus.com/tutorials/introduction-to-the-php-configuration-file-phpini--cms-34452

https://mailtrap.io/blog/php-email-sending/

https://www.hostinger.com/tutorials/send-emails-using-php-mail

https://stackoverflow.com/questions/24644436/php-mail-function-doesnt-complete-sending-of-e-mail

https://stackoverflow.com/questions/49206701/how-to-send-an-email-using-sendmail-command

PHPMailer: https://mailtrap.io/blog/phpmailer/

We need it to send messages to Josh. *Maybe* to the sender as a copy, that would be smart for confirmation purposes. How can we confirm the email is getting to Josh at all? Unblock the sender and ensure it doesn't go to spam in his email client maybe.

(Maybe) How to:

* Get domain
* Create domain email
* Ensure a flavor of Linux is the server base, so it has sendmail
* Install Lichen
* Ensure Lichen is serving properly
* Install [PHPMailer](https://github.com/PHPMailer/PHPMailer) (using composer? no? why?)
* Add a test on the main page to send a test email using a fake email as TO and the real email as FROM

