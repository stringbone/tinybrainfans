---
title: Martial Arts Games
description: 'These are "games" to isolate certain techniques and make it fun.'
---

-   [Chi push? (no real name given)](https://inv.tux.pizza/watch?v=mWO68FayDRk&listen=false&t=426)
-   [Aiki training](https://inv.tux.pizza/watch?v=Vi5-r1oivek)
-   [Kuzushi training](https://inv.tux.pizza/watch?v=zmIQ3zepDRA)
-   [Push hands (from tai chi)](https://www.youtube.com/watch?v=2kI3OdfqQBM)
