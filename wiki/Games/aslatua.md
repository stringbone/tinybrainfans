---
title: Aslatua
description: Also known as kashaka or cas cas, these are two shakers attached by a string.
---

Also known as kashaka or cas cas, these are two shakers attached by a string. Similar to a {{begleri}}.

## References

1. https://www.youtube.com/watch?v=VDbFOJf__jw
2. https://www.youtube.com/watch?v=YhkbI77BnlU
3. https://www.youtube.com/watch?v=bX2C4LuhFCk
4. https://www.youtube.com/watch?v=PPlwAxzX_xE
5. https://www.youtube.com/watch?v=6F3yIp74i0I
6. https://www.youtube.com/watch?v=2I8sysK15lk
7. https://www.youtube.com/watch?v=PcMixI97NY0

