---
title: Card Games
description: These are games to play with standard 52-card playing cards.
---

These are games to play with standard 52-card {{playing cards}} or other common decks like Uno.

## Solitaire

-   [Canfield](<https://en.wikipedia.org/wiki/Canfield_(solitaire)>)

## Multiple Players

-   [Nerts](https://en.wikipedia.org/wiki/Nerts)
-   {{Dutch}}
-   [Spades](<https://en.wikipedia.org/wiki/Spades_(card_game)>)

### House Uno

It's normal Uno played with normal Uno cards, but every round, the winner gets to instantiate a new rule or remove a previous rule for all subsequent rounds. This makes a whole lot less sense as time goes on, but it is good for people like me who prefer a ton of stimulus.

### Youno

It's normal Uno played with your cards facing away from you. You play from someone else's hand on your turn. If you can't play, the next person draws a card, which you must play if it is playable.

## References

1. [Pagat.com - Card Games and Tile Games from around the World](https://www.pagat.com/)
2. [Youknow](https://www.pagat.com/invented/uno_vars.html#youknow)
