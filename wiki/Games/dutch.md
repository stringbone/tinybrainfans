---
title: Dutch
description: A card game to be played with two or more people.
---

Dutch is a {{card game|card games}} played with a standard {{playing card}} deck. The objective is to end the game with the fewest total points

## Starting the round

Each player is dealt 4 cards face down. The remaining cards go in a draw pile in the center of the table and the top card is placed next to it, creating the discard pile.

The dealer determines how many cards, between 0 and 4, each player can look at before the game begins. After this point, players can not look at their cards again outside of switching cards on their turn.

## Play

The player left of the dealer plays first. During their turn, each player can either pick up the card on top of the discard pile, or pick up the top card of deck. Once they have chosen their card, they must switch out that card with a card from their hand.

If a player has a card that matches the one in the discard pile (e.g. a 3, a King, etc.), they can place their matching card on top of the matching card in the discard pile.

If a player discards a Queen, they can look at any one card from their or another person's hand.

If a player discards a Jack, the player who plays it can switch any two player's cards, including their own. Neither player can look at the new card; if one of the cards was owned by the player who discarded the Jack, the _other_ player can look at their new card.

## Finishing the round

If a player gets rid of all their cards, play stops immediately, with all players counting their remaining points.

### Calling "Dutch"

-   If a player (the "caller") believes they have the lowest score, they can knock or call "Dutch".
-   Each remaining player gets one more round until it gets back to the caller.
-   Cards are flipped and counted.
-   If the caller has the lowest score, they get zero points and everyone else counts the points in their hand.
-   If the caller did _not_ have the lowest score, the player with the lowest score gets zero and everyone else counts their points; the caller gets a penalty card at the start of the next round.

## Penalties

A player will receive a penalty card from the top of the deck which they cannot look at if

-   they flip a card from their hand to match the card on the discard pile and it does not match,
-   they look at a card in their hand a second time, after it is face down, or
-   they call "Dutch" but don't have the lowest score at the end of the round.

## Points/Card values

-   King of a red suit: 0 points
-   King of a black suit: 13 points
-   Queens: 12 points
-   Jacks: 11 points
-   All other cards are worth face value

## Game end

The game ends once someone reaches 100. When this happens, the player with the lowest score wins.

## References

1. https://inv.tux.pizza/watch?v=-DFavWxVs-4
