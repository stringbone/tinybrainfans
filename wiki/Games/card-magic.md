---
title: Card Magic
description: A bunch of tricks and resources for uncle magic.
---

These all use a standard 52-card deck of {{playing cards}}.

## Books

* The Royal Road to Card Magic[1]

## Simple Tricks

* [The EASIEST card trick you will ever learn (Four aces)](https://www.youtube.com/watch?v=DJrA0V6lSF8)
* [This EASY card trick will fool your friends (Is this your card)](https://www.youtube.com/watch?v=HDooBdbj2jM)
* [THE TOPSY TURVY CARDS in-depth tutorial](https://www.youtube.com/watch?v=B_BpzYY_Qw0) from "The Royal Road To Card Magic"[1]
* [Pretty cool Biddle trick](https://www.youtube.com/watch?v=rx7LIFUtmlk)
* [Sweet 16](https://www.youtube.com/watch?v=FGF-qa-maRg)
* [Tell the truth](https://www.youtube.com/watch?v=e15MXNXbB4A)

## False cuts

* [from Alex Pandrea](https://www.youtube.com/watch?v=UV9_qgA4YGQ)
* [from Rich Ferguson](https://www.youtube.com/watch?v=wqDSkueGV2s)

## References

1. [The Royal Road to Card Magic (free)](https://archive.org/details/theroyalroadtocardmagic)
2. [Mismag on youtube](https://www.youtube.com/@mismag822)
3. [52kards on youtube](https://www.youtube.com/@52kards)
4. [Alex Pandrea on youtube](https://www.youtube.com/@AlexPandrea)

