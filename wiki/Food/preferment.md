---
title: Preferment for bread
description: A preferment is important for good flavor without a sourdough starter.
---

{{Sourdough}} starters are fun, but can be daunting. If you want to easily upgrade your loaves, one of the easiest things you can do is a poolish or biga. This helps quite a bit with flavor and gluten development.

> Firm/dry pre-ferment:
>
> -   Biga - made from scratch as a pre-ferment, no salt, as little as .5% yeast
> -   Pate Fermentee - extra dough saved for leavening, contains salt
>
> Wet pre-ferment:
>
> -   Poolish - equal weights water and flour with .25% yeast, usually requires more yeast at final mix
> -   Sponge - faster than a poolish, loads all or most of the yeast in the pre-ferment[1]

For a poolish for a "standard" 900g loaf of {{bread}}, I have done around 100g {{water}} and {{flour}} with 1/8 or 1/16 tsp {{yeast}}. Though I've also read[2] that using half the flour in the recipe and equal amount water, with a pinch of yeast has a similar effect. Let it sit for a while (see below). Then add it to the rest of the ingredients when it's time to combine.

## How much yeast relates to wait time

The percentages here[1] are baker's percentages, meaning percentage of the total flour weight used (e.g. 100g of flour would use .6% for 3 hours, which would be .006 \* 100 = .6g).

> 3 hours: 0.6%
> 7-8 hours: 0.28%
> 12 - 15 hours: .04%

Obviously these are _very_ small weights, so you could also disperse larger amounts in water, mix and then separate. For instance, if you need .6g, measure 6 grams into 100g of water, mix, and then save 10g of the water, leaving you with around .6g of yeast.

## References

1. https://www.thefreshloaf.com/comment/20549#comment-20549
2. [Poolish recipe from Fresh Loaf](https://www.thefreshloaf.com/comment/299678#comment-299678)
