---
title: Foraging
description: Food foraging is finding and eating food that is out there in the wild. DO NOT EAT WHAT YOU CANNOT IDENTIFY 100%!
---

<span style="color: red;">**DO NOT EAT WHAT YOU CANNOT IDENTIFY 100%!**</span> If you can't for SURE identify what the plant or food is without any ambiguity, IT CAN KILL YOU. See [Atomic Shrimp's video on beginning foraging to understand the risks](https://www.youtube.com/watch?v=iHPW8Z323F0). This is not a harmless activity, it can be life or death.

## References

1. https://www.youtube.com/watch?v=iHPW8Z323F0
2. https://yewtu.be/watch?v=gzJg4yuEi2c
3. http://wildfoodsandmedicines.com/fir-hemlock-and-spruce-tips/
4. https://nittygrittylife.com/douglas-fir/
5. http://wildfoodsandmedicines.com/douglas-fir/
6. https://defiel.com/how-to-harvest-tree-sap/
7. https://old.reddit.com/r/foraging/comments/wgmjsb/portland_oregon/
8. https://cascadiaprairieoak.org/documents/field-guide-to-weeds-of-eastern-oregon
9. https://pfaf.org/user/
