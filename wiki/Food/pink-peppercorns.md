---
title: Peruvian Peppercorn
description: This fragrant peppercorn can be found in Southern California, Hawaii, and Florida.
---

In America, this tree can be found in Southern California, Hawaii, and Florida. They are quite abundant in Los Angeles and the bay area of San Francisco, so it's quite an easy one to {{forage|Foraging}}.

### [Identification](https://en.wikipedia.org/wiki/Schinus_molle)

_Schinus molle_ is a quick growing evergreen tree that grows up to 15 meters (50 feet) tall and wide. It is the largest of all _[Schinus](https://en.wikipedia.org/wiki/Schinus)_ species and potentially the longest lived. The upper branches of the tree tend to droop. The tree's [pinnately compound](https://en.wikipedia.org/wiki/Pinnate) leaves measure 8–25 cm long × 4–9 cm wide and are made up of 19-41 [alternate](https://en.wikipedia.org/wiki/Leaves#Arrangement_on_the_stem) leaflets. Male and female flowers occur on separate plants ([dioecious](https://en.wikipedia.org/wiki/Dioecious)). Flowers are small, white and borne profusely in panicles at the ends of the drooping branches. The [fruit](https://en.wikipedia.org/wiki/Fruit) are 5–7 mm diameter round [drupes](https://en.wikipedia.org/wiki/Drupe) with woody seeds that turn from green to red, pink or purplish, carried in dense clusters of hundreds of berries that can be present year round. The rough grayish bark is twisted and drips sap. The bark, leaves and berries are aromatic when crushed.

See the Wikipedia page for pictures of the distinctive bark and berries.

### Food

The ripened peppercorns can be used as a fruity spicy addition to dishes, particulary for seafood, salads, curries, cheese, chocolate, or popcorn. "spicy and peppery, but have a very fragrant, sweet-tart and rosy tone."

### Preparation

-   Pick the berries from the branches once they are ripe and bright pink, trying to remove as much branch as possible, but it's not a biggie if some are left.
-   Set them on a single layer sheet or plate and let them dry out over a few days. This should result a dried fruit outer layer around the seed inside.
-   Store these dried peppercorns away from direct sunlight and heat to maintain freshness.

### Use

Use these dried peppercorns with mortar and pestle or crushed with the flat side of a knife to release their oils. They can also be roasted prior to use to release more aromatics. They can also be mixed with regular peppercorns, but can easily be overwhelmed, and the thin skin can clog the pepper grinder. Interestingly, they can also be used whole, as they are milder than other types of peppercorns.

The potency will start to decline six months or so after preparation.

## References

1.  https://en.wikipedia.org/wiki/Schinus_molle
2.  https://www.gardenbetty.com/peruvian-pink-peppercorns/
