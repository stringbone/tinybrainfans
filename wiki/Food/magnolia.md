---
title: Magnolia
description: Magnolia blossoms are edible and delicions, tasting of floral ginger.
---

Magnolia buds and petals can be used raw, {{pickled|pickles}}, or dried. I've used them to make an {{all-vinegar quick pickle|Magnolia pickles}}, tea, and raw in soups.

## Flavors and varieties by color

As always, if you are {{foraging}} this plant, ensure you are 100% about your identification. This list is from Handmade Apothecary's page[1]:

-   Dark pink: bitter, gingery-chilli
-   Pink/White: the gingery, cardamommy flavours
-   White: more lemony, subtle, sometimes floral

## References

1. [Different types of edible magnolia](https://www.handmadeapothecary.co.uk/blog/2019/3/16/magnolia-a-foraged-spice-cupboard?rq=magnolias)
2. [Ways to use magnolia](https://www.wildwalks-southwest.co.uk/wild-food-magnolia-petals/)
