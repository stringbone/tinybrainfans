---
title: Hydration Drink
description: Like Pocari Sweat, this is a hydrating drink with electrolytes, and not super sweet like Gatorade.
---

Like Pocari Sweat, this is a hydrating drink with electrolytes, and not super sweet like Gatorade.

## Ingredients

-   1L {{water}}
-   Juice (2-3 Tbsp) of 1 {{lemon}}, {{lime}}, grapefruit, or other citrus
-   1/4-1/2 tsp {{salt}}
-   2 tsp sweetener (e.g. {{honey}}, {{sugar}}) (optional)

### Substitutions

-   You can substitute a lemon-lime or citrus fizzy drink for the sweetener, if you use a ratio of around 2 to 1 for the water to the fizzy drink.

## Cookware

-   Jar or closed-top container for mixing/storage
-   Measuring cups/spoons
-   Wooden spoon or something for mixing.
-   Refrigerator for storage, if storing

## Instructions

1. Mix everything together using wooden spoon or by shaking the closed container.
1. Drink.

## References

1. [DIY Electrolyte Drink](https://www.raisinggenerationnourished.com/2014/12/diy-electrolyte-drink/)
2. [Hydra Booster](https://www.drinkcollectiv.com/2018/05/09/hydra-booster/)
