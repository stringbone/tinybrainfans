---
title: Pesto with Greens
description: Most pesto is made with basil, but really any greens will do.
---

Most pesto is made with basil, but really any greens will do.

## Ingredients

-   2 cups greens (washed ({{beet}} with stem removed, {{carrot}}, {{basil}}, {{spinach}}, etc.)
-   1/3 cup {{nuts}} (walnuts, pine nuts, etc.) or sunflower {{seeds}}
-   2 cloves {{garlic}}
-   120ml extra virgin {{olive oil}}
-   1/3 cup {{nutritional yeast}}
-   1/8 tsp {{salt}}
-   1/8 tsp {{fresh ground black pepper}} (opt.)

## Cookware

-   Blender or food processor

## Instructions

1. Put greens, nuts, and garlic in a blender and pulse until finely chopped.
1. Slowly add olive oil while continuing short pulses. We want the oil combined but we don't want a homogenous mixture.
1. Add nutritional yeast, pepper and salt and pulse some more. Add to taste.
1. Eat immediately or store in the fridge for short-term or the freeze for the long-term.

## References

1. https://www.forkintheroad.co/beet-greens-pesto/
