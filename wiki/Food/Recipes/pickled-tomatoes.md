---
title: Fermented Tomatoes
description:
---

## References

1. [Fermented Tomatoes: A Traditional Russian Recipe](https://healingharvesthomestead.com/home/2016/10/15/make-fermented-tomatoes-filled-with-probiotic-goodness-and-so-delicious)
2. [The Easiest Ferment in the World: Cherry Tomatoes](https://zerowastechef.com/2022/07/12/the-easiest-ferment-cherry-tomatoes/?utm_source=substack&utm_medium=email)

