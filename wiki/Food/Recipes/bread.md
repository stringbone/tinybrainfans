---
title: Bread
description: Bread is a lot easier to make than you think.
---

Bread is easier than you think to make. It's largely passive time, waiting for the next step, so it's best suited for when you have time at home. That said, I have definitely taken loaves that are proofing to friend's houses and left them in their fridge, grabbing them before I head home.

## Basics

The basic bread process is:

-   {{Prefermentation|Preferment for bread}}, e.g. {{sourdough}}, poolish, biga, autolyse sort of (optional)
-   Combining ingredients
-   Bulk fermentation
-   Divide/shape
-   Proof (second fermentation)
-   Bake

### Autolyse

Autolyse is not really a prefermentation, but it is helpful for flavor and texture.

In your mixing bowl, mix your flour and water until combined. Then let rest for 20 minutes to an hour before adding the rest of the ingredients.

## References

1. [Bread in five minutes a day basic recipe (a great starter recipe from a great book)](https://artisanbreadinfive.com/2013/10/22/the-new-artisan-bread-in-five-minutes-a-day-is-launched-back-to-basics-updated/)
