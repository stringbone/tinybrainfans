---
title: Bread for Bread Machines
description: Bread machines are weird, but they're nice.
---

Making bread in a bread machine is _way_ different than making it from scratch. Mainly in that it's almost ballistic how you just throw a bunch of shit together and then bread comes out. Usually you can adjust things in the middle of the process to make different stuff happen. But the upside is if you don't have an oven, you can still make bread. So here we are.

## Basic White Bread (with optional {{preferment}})

This recipe I adapted from the recipe book for the Zojirushi BBCC-S15, a 1-1.5 pound bread machine. This recipe may need to be scaled for your machine.

If you don't want a preferment, just mix everything at once, in the order your bread machine recommends (likely water, fats, sugar, salt, flour, yeast).

If you want a _slightly_ improved version without the 8-12 hours wait, you could do the preferment for about 30 minutes or so, for more of an {{autolyse}}.

## Ingredients

### The {{Preferment|Preferment for bread}}

-   180g {{water}}
-   180g {{flour}}
-   1/16 teaspoon (a pinch) {{yeast}}

### The Rest of the Dough

-   240g water
-   6g {{salt}}
-   24g {{sugar}}
-   28g (2T) {{butter}} or {{olive oil}}
-   390g flour
-   1 teaspoon/3.5g yeast

## Cookware

### The Preferment

-   Bread machine's bread pan
-   Small mixing utensil (spoon or spatula)
-   Measuring spoons
-   Scale

### The Dough

-   Bread machine
-   Scale

## Instructions

### The Preferment

1. Add the three ingredients into your bread machine's bread pan. Mix them just until well combined (no chunks of flour).
1. Put in the bread machine (without initiating any cycles or anything, powered off) for about 8-12 hours.

### The Rest of the Dough

1. Add the water into the pan with the preferment, to release it from the sides.
1. Add the rest of the ingredients.
1. Start the white bread cycle. Done.
