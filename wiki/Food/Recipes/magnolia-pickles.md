---
title: Magnolia Quick Pickles
description: Pickled magnolia blossoms and buds taste like pickled ginger.
---

{{Magnolia}} is everywhere in the pacific northwest and when it blooms it is absolutely abundant. Use these blossoms and buds to preserve some of the goodness.

## Ingredients

This recipe scales easily, so don't be afraid to go bigger.

-   9-10 {{magnolia}} buds or blossoms (~80-100g)
-   150g {{vinegar}} (rice, white, apple cider, etc.)
-   50g {{sugar}}
-   Pinch of {{salt}}

## Cookware

-   Pot for boiling
-   Mason-style jar for storing, cleaned and sanitized
-   Refrigerator, if wanting long-term storage

## Preparing the food

1. If using whole buds, discard the outer green sepal. If only using petals, discard everything but the petals (outer green sepal, inner ovary, etc.). Wash and clean the food before preparation.
1. If using petals, Stack as many magnolia petals as you can and slice into strips lengthwise.

## Instructions

Follow the {{quick pickle|quick pickles}} recipe.

## References

1. https://www.kaveyeats.com/how-to-pickle-magnolia-blossoms
2. https://www.wildwalks-southwest.co.uk/wild-food-magnolia-petals/
