---
title: Tepache
description: Tepache is a Mexican fermented pineapple drink.
---

Tepache is a Mexican {{fermented}} {{pineapple}} drink. Think pineapple {{kombucha}}. It is often drank straight, mixed into a cocktail, or mixed with beer.

## Ingedients

- 1 whole {{pineapple}}
- 200g {{piloncillo}}, {{brown sugar}}, or {{sugar}}
- 10 {{cloves}}
- 1 {{cinnamon}} stick
- 5 {{ginger}} slices (optional)
- 2 quarts {{water}} (enough to cover material)

## Cookware

- Pot and spoon for boiling/mixing sugar water
- Knife that can cut through a pineapple
- Cutting board
- Kitchen scale
- Half gallon jar and lid
- Kitchen cloth/something open but tighter than cheesecloth to cover the jar
- Another storage container for the fruit
- Fine mesh strainer
- Funnel, if necessary
- If doing a second fermentation, fermentation-grade bottles
- Else, some clean container (another jar, pitcher, etc.)

## Instructions

1. Dissolve the sugar in around a quart of water.
1. Cut the top and bottom off the pineapple. Discard.
1. Stand up your barrel-shaped body of pineapple and remove the rind. Save the rind and put it into the jar.
1. Cut the fruit from the core. Save the fruit in a separate container and put the core in the jar.
1. Add the spices to the jar with the pineapple rind and core. Once the sugar water is room temperature, add it to the jar. Add rest of water, or enough water until there is around 1 or 2 inches of room.
1. Cover the jar with your semi-permeable material away from direct sunlight at room temperature.
1. Let it sit for 1 to 3 days. It should start to bubble and be very active from the yeast in the pineapple. It will happen more quickly in a warmer environment and slower in a colder one.
1. Give it a taste after around 24 hours, and then every 24 hours after that. When it's the flavor you desire, it's time to bottle. If you wait too long, it can get a bit too much vinegar flavor, so earlier is better, IMO.
1. Strain liquid into a clean pitcher. You can discard the remnants or use it to make another batch.
1. If you aren't doing a second fermentation, you're done! Put your pitcher in the fridge.

## Second Fermentation (optional)

This process is similar to the second fermentation of {{kombucha}}.

1. Put the liquid into individual containers, leaving around 1 to 2 inches free at the top.
1. Let sit at room temperature for 1 to 3 more days.
1. Every 24 hours or so, check the level of carbonation in one of your containers.
1. When it's at the carbonation level you want, put it in the fridge.

## References

1. https://fermenterskitchen.com/pineapple-tepache/
2. https://drinkinghobby.com/tepache-recipe/#tve-jump-172dca0214d
3. https://www.mexicoinmykitchen.com/homemade-pineapple-brew
