---
title: Fermented Brown Rice
---

This method removes the phytic acid and makes the nutrition more bioavailable. It sounds like this same effect could be applied to other grains, as specified in the study[2].

## Instructions[1]

1. Soak the {{rice}} in water: Put the brown rice in a bowl and add room-temperature water, until the water level is 2 inches above the rice. Let the rice soak. The February 2015 study published in the Journal of Food Science and Technology notes that grains that have been soaked for 12 to 24 hours saw a significant reduction in food inhibitors like phytic acid[2].
2. Keep aside some of the water: Drain some of the water from the rice and store it in a container in the fridge. The next time you want to ferment rice, add this water to the pot. The phytase compounds in the water will help break down the phytic acid in the fresh batch of rice, according to the February 2015 study published in the Journal of Food Science and Technology.
3. Cook the rice: Cook the fermented rice as you normally would, with the exception of cooking time. Brown rice that has been soaked cooks much faster, so keep an eye on it and take it off the heat when it is done, to keep it from getting mushy.

## References

1. https://www.livestrong.com/article/486594-how-to-ferment-brown-rice/
2. https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4325021/
