---
title: Do Nothing Bread
description: Yohan Ferrant's recipe for \"do nothing\" sourdough bread.
---

Yohan Ferrant's recipe for "do nothing" {{sourdough}} {{bread}}. This bread tastes GREAT and honestly really is easy. It can also scale, if you don't want to make so many loaves, since this recipe usually yields me 8 small loaves.

## Ingredients

### The Dough

-   10g {{sourdough starter}}
-   900g {{water}}
-   600g whole wheat {{flour}}
-   400g bread flour
-   20g {{salt}}

### Making The Bread

-   {{Cornmeal}}, if not using a {{dutch oven}}

## Cookware

### The Dough

-   Rectangular tall-ish container for mixing and resting the dough (~A3 paper size, can find at chef stores)
-   Cover for container (can use a plastic bag)
-   Wooden kitchen tool handle to mix (opt., I use the handle of my dough whisk)

### Making The Bread

-   Oven
-   Dutch oven, baking stone, or cookie sheet
-   Kitchen scale (opt.)
-   Kitchen shears (opt.)
-   Bench scraper (opt. but super useful)
-   Butcher's block or board for shaping the bread
-   Parchment paper if you don't want to use cornmeal
-   One of these proofing mediums
    -   Baking couche/silpat-style thing
    -   Proofing basket (with lots of flour!!)
-   _If not using dutch oven:_
    -   Pizza peel for resting bread and putting bread in oven (opt.)
    -   Spray bottle with water or broiler tray
-   Cooling rack, or somewhere the bread can cool and breathe after baking

## Instructions

### The Dough

1. Add everything into the mixing container (water and starter first) and mix until just combined. Cover container and wait.
1. After 12 hours, you can do an optional fold. Grab from the center lengthwise and gently lift the dough until it releases from the container. Then lower it back down into the container. Repeat widthwise. Cover the container

### Making The Bread

1. Preheat your oven and dutch ovens at 470F/243C.
1. 24 hours after the first mixing, dust the top of your dough with flour, particularly around the edges. Use a bench scraper to release the dough from the edges of the container. Flour your butcher's block well and flip your container over on top of the butcher's block. Your dough should slowly release. Release any stragglers manually. Note that this dough is _very_ wet! Act fast!
1. Dust the top of your dough lightly with flour, adding more flour where you will divide the bread. I usually divide the dough into 8 parts, with one division lengthwise and three widthwise. Use your bench scraper to divide the dough and put them on a floured surface away from the dough, as they will likely try and connect again.
1. Dust your proofing medium liberally. Shape the bread by grabbing the long end and folding it about a third over. Then grab this folded end and lightly roll it onto last third. Pick it up lightly and flip it over onto your proofing medium. Ensure the loaves don't stick together by using flour on their ends and using your couche (if using a silicon mat, you may be able to do the same thing, otherwise they will flatten a _lot_).
1. When it's time to bake, put them into your dutch oven for 15 minutes, taking the lid off after and baking for about 20 more minutes. I usually cycle the loaves when I take the lid off: one loaf out of the dutch oven onto the rack; one loaf off the rack, out of the oven, and onto the cooling rack; one new loaf into the dutch oven.
1. Let cool for at least an hour, of course.

## References

1. [Archived recipe from Teresa Greenway](https://web.archive.org/web/20170810090959/https://northwestsourdough.com/yohan-ferrants-do-nothing-bread/)
2. [Video from Teresa Greenway](https://www.youtube.com/watch?v=Wwu3KgbhTsU)
