---
title: Blackberry Leaf Tea
description: Blackberry leaves can be dried or fermented to make tea.
---

For both of these methods, you want young leaves, usually when they first start showing up. Basically, you want bright green leaves that aren't tough or thorny. The younger the better.

## Basic

To make basic {{blackberry}} leaf {{tea}}, you can simply pick the leaves, remove the stem, and dry them in a {{dehydrator}} or a very low temperature oven. Then to make the tea, put them in boiling water for a while. It's tea.

## Fermented

This takes a little more effort.

### Cookware/Tools

-   Bruising tool: mortar and pestle, rolling pin, pasta machine (as described in the post[1]), whatever
-   Small sealable container to pack the leaves (mason jar)
-   Some kind of follower to pack and cover (ziploc bag with marbles or beans or whatever works great)
-   Dehydrator or oven that can go low temp (optional)

### Instructions

1. Pick the leaves, remove the stems, and wash them.
1. Bruise the leaves with your bruising tool.
1. Tightly pack the leaves into your container.
1. Wait for two or more weeks. Original post[1] has six, with the longer time resulting in more tannins. The leaves should turn dark and get more floral smelling
1. When ready, remove leaves from the jar, separate them and dehydrate them. If you don't have a dehydrator or don't want to use an oven, just lay them out on a drying rack and wait.
1. Crush the leaves and put them into a container for storage.

When you want to make tea, treat it like you would any black tea.

## References

1. [Blackberry leaf tea](https://susansumptuousuppers.wordpress.com/2016/02/29/blackberry-leaf-tea/)
