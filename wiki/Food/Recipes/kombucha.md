---
title: Kombucha
description: Kombucha is a naturally fermented probiotic tea based on a SCOBY, or good fungus.
---

Kombucha is a naturally {{fermented}} probiotic {{tea}} based on a SCOBY, or good fungus. Long story short, you take tea and you add sweetener and a SCOBY and after a while, you get a probiotic beverage.

The recipe scales up or down easily.

## Ingredients

-   1L / 4c {{water}} at or around room temperature (not boiling)
-   70g / 1/4c {{sugar}}
-   3 bags / 1 Tbsp {{black tea}} (I've done loose or bags, both work)
-   1 {{SCOBY}} (ask your friends!)
-   1/2c SCOBY starter liquid
-   Something for second fermentation (fruit juice, fruit, {{ginger}}, {{sugar}}, etc.)

## Cookware

-   Measuring cups/spoons
-   Stove/range
-   Pot with lid
-   Spoon
-   Large mouth half-gallon jar (or more, if scaling)
-   Fine kitchen cloth or coffee filters larger than your jar opening
-   Rubber band
-   Smaller sterilized containers for second fermentation (round mason jars, flip top bottles, anything round and made to be pressurized)

## Instructions

### Starting the process

1. Bring about a quarter to a third of your water to a boil in your pot. Reserve the rest in your half gallon jar.
1. Before boiling, add your sugar and mix until fully dissolved.
1. When boiling, turn off the heat, add your tea and let steep for a while. You can do a standard two or three minutes, but I usually let it go for longer. There is not a "right" way to do this part. When done, remove the tea leaves/bags.
1. Slowly add the reserved water to the pot and stir to ensure liquid is around room temperature. (I've also added the boiling water very slowly to the jar. Don't think it matters, but it must be more dangerous in terms of glass shock.)
1. Combine SCOBY, starter liquid, and tea mixture in your half gallon jar. Stir gently to incorporate.
1. Place cloth or filter on the jar opening and use robber band to secure.

### Fermentation

1. Let sit in a warm (20-28 celsius/65-80 fehrenheit) part of your space out of direct sunlight for about a week.
1. When you think it's time to check, get a sterile/clean spoon/straw and taste it. If it's too sweet, wait a little bit. If you like how it tastes, you start the second fermentation.

### Second Fermentation

I _highly_ recommend asking around about what people you know in your area do for their second fermentation. So much relies on altitude, temperature, water, etc. that giving basic guidelines to beginners feels scary. Too much sugar and you could end up with an explosion! Just be aware.

If all else fails, try 1 teaspoon of sugar per half-liter container. Or check out the list of recipes from reddit[2].

1. Add your flavoring to each container and add kombucha. You don't want to fill it to the brim, but you don't want a ton of dead air. Usually around an inch from the top.
1. Let bottles sit for anywhere between 1 and 7 days. This is when your kombucha will develop carbonation. During this, check your bottles to see if they are developing carbonation. If you have multiple bottles, check one on one day, another on the next, etc.
1. When ready, throw them in the fridge.

## References

1. [r/Kombucha master recipe](https://old.reddit.com/r/Kombucha/comments/9udpx6/faq_and_master_recipe_check_here_first/)
2. [Kombucha flavor recipes](https://docs.google.com/spreadsheets/d/1tjfP4eqnbNh6WLyi_4blLhWhGVcADUUojS6iGRXbcPY/edit?gid=79949439#gid=79949439)
3. [(Untested) Hops flavored kombucha](https://revolutionfermentation.com/en/blogs/kombucha/local-hop-kombucha-recipe/)
