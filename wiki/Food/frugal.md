---
title: Eating Frugally
description: Good food doesn't have to be expensive nor wasteful.
---

<!--

## Fermentation/Pickling

## Foraging

## Compost

## Growing

## Meal Prep

## Bulk

-->

## References

1. https://zerowastechef.substack.com/p/7-ways-to-save-money-as-food-prices
2. https://zerowastechef.substack.com/p/7-homemade-drinks-that-beat-the-heat
3. https://zerowastechef.substack.com/p/5-simple-healthy-meals-that-cost
4. https://zerowastechef.substack.com/p/13-kitchen-gadgets-you-may-already

