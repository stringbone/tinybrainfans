---
title: Constraints
description:
---

> Without constraints, no solutions are interesting.
>
> How do you design a good house, if you don't know anything about the land it is going to be on, or the people who will be using it?
>
> How do you design a good program, if you don't know what machines it will be running on, or the people who will be using it?[1]

## References

1. https://hackers.town/@yojimbo/107759132494717182
