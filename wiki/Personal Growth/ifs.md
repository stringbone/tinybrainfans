---
title: Internal Family Systems
description: IFS is a framework to help understand our inner selves, as a series of parts.
---

Internal Family Systems is a way of thinking about our inner self. It is very easy to think of our self as a monolith, in one qualitative sweeping way. But IFS has us think of our self as composed of many parts, or "little guys" as I think of it. These little guys are created usually as deep grooves in our behavior, developed in situations that necessitate new ways of being to accommodate for novel and sometimes scary situations. These deep grooves are a kind of psychological muscle memory, similar to riding a bike.

When these parts or "little guys" take over or are running the show, this is called when you are "blending" with the part or parts. Learning to unblend is one of the main goals of IFS. To unblend, stand back with dispassionate curiosity, and listen to our parts instead of totally indulge or push them away.

The process continues, peeling back layer after layer. Eventually, the core self (or Self with a capital S) is revealed.

## Parts, or little guys

These little guys are important. They're around for a good reason. A little guy that is focused on proving you're worth the good things you get may be born from some trauma that happened in your life and is trying to stop that from happening again. They usually show up to protect those "exiled" parts of ourselves, to stop those bad experiences from happening again, and to further exile those memories. They show up in a couple different ways:

* Managers: they are spending their time managing how we present ourselves to the world. They make sure we don't end up in situations where we will be vulnerable.
* Firefighters: they show up when they feel you need to be protected, like my "smart and correct guy" that shows up when I'm feeling anxious and thinks the solution that will keep me safe is being "right".

## References

1. https://inv.tux.pizza/watch?v=tNA5qTTxFFA
2. https://inv.tux.pizza/watch?v=3bNHkg4ZPpA&listen=false
3. https://aryatherapy.com/wp-content/uploads/2019/07/Exiles-Managers-Firefighters.pdf
