---
title: Yak Shaving
description: "Yak shaving is when you start task X but task Y 'must' be finished first, and thus task Z 'must' be done, ad infinitum."
---

Yak shaving is when you start task X but task Y 'must' be finished first, and thus task Z 'must' be done, ad infinitum. Or another way, "doing some seemingly useless task that is necessary to complete another task, which is necessary to complete other tasks, which eventually will allow you to complete your initial goal"[2].

## References

1. https://yewtu.be/watch?v=AbSehcT19u0
2. https://softwareengineering.stackexchange.com/questions/388092/what-exactly-is-yak-shaving
