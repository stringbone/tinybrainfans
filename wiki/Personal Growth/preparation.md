---
title: Audition / Interview / Test Preparation
description:
---

A lot of this is transcribed from [trombonist Joseph Rodriguez's](https://www.trombonejoe.com/) handout from a music festival I went to ages ago. It's timeless. We're assuming here you've committed already to the event.

## Have a central space for all your information

Whatever it is, collate this information into some central space. A binder, a folder on your computer, a Google Doc, whatever, just make a central space for everything. Different events require different systems. Do you travel a lot? Make it effortless to integrate into that. Are you easily distracted? Maybe don't have it on your phone. Etc.

## Get the metadata

To get started, we need to know what is expected of us. If it's an audition, we need to know the repertoire, the location of the audition, date and time, etc. For a job interview, we need to know the position, the job expectations, the information given to us.

In general, what did they provide to you about the event? Get this into a format that makes sense to you, even if that means just copying it down from one doc to another.

When you commit to a new event (audition, test, interview, etc.), you need to know where you are so you can get to where you are going. This falls a little bit into the {{PI AT PC Framework}}.

-   Delve deep into the provided information. Do you fully understand all the information provided to you? If not, do more research until you do. Record everything!
-   Do you know what _your_ goal is for this event? (This may not be the obvious answer, so make sure you know what it is for you.) If you cannot explain this readily and easily, make it so that you can. This is helpful for ensuring you actually should do this thing, but also for helping center your process.
-   Do you know what metrics you will be assessed with? In an audition, they usually are looking for a specific style, tone, vibe, whatever. Whatever it is, try and get as much information as you can on it. If you are applying for a job, look up the employer and understand what they value, the job and what it entails in other companies, etc.
-   If you do not feel like you have enough information to answer these questions, what assumptions do you think you can make? If you are going to be taking a test and you don't know what the exact metrics are, look at similar tests and see if you can find something similar, or ask someone who has taken it before, or infer them based on values of the company/administers/etc.

Gather any remaining materials you don't have that came up in this process. Maybe a recording of a certain piece of music you need to learn, or a book that you need to know about, or a list of techniques you need to demonstrate. You want _everything_ that could help you within reach. Put this stuff into your space! You want this to be easy.

## Assess your current state

Now that you have all the information and materials, it is time to make a plan. The key to learning is truly understanding the subject and then spaced repetition for eventual recall (if you have not checked out this course about learning from Dr. Barbara Oakley[1], I highly recommend it). We want to go through all the things we will need to know for this event and put them into a spreadsheet.

The spreadsheet will have one row for each thing you will be practicing (a song, a technique, a part of the interview, whatever) and a column for each element of that thing. If you were a musician, you may have a row for each excerpt or song and a column for each piece of information. These pieces of information that are likely common for every event are

-   Level of knowledge/understanding/comfort (1-5)
-   Materials gathered (boolean)
-   Probability (1-5)

## Make a plan

This plan can be expanded or contracted however, but this is a good general rule.

-   Get three envelopes.
-   Write down each of the things you want to learn on to individual small pieces of paper or notecards that can fit into the envelopes.
-   Organize them all by your level of knowledge and/or probability (if you want to be nerdy, you could do K + P or K \* P). We want things on the high end to be very demanding or very likely, and vice versa.
-   Now deal out three piles from the top of your "deck" of items. When finished, you should have three piles of items that are on average about the same level of difficulty. Put each pile into an envelope.

### Make a calendar

Make a calendar that lays out what you will be doing each day. For each day of practice, go to an envelope and practice from there. Day one is A, day two is B, day three is C, day four is A, etc. Make sure you have rest days, but also don't neglect the time spent practicing. If it's all physical, take time to do mental practice.

Interspersed between these days of practice should be the following:

-   Mock events (mock audition, mock interview, etc.) to test yourself and understand what you need to improve.
-   Listening/observing/reflecting
-   Mental practice/visualization practice
-   Lessons/coaching/consulting
-   Rest days! Hang out with your friends and family. Understand why you give a shit about life.

When it gets close to the big day, merge your three envelopes into two envelopes by "dealing" envelope C into the A and B envelopes. We want to maximize our repetition now that less time is spent learning.

The days right before should be minimal. Resting, overview, drinking lots of water, exercise. The day before you should carbo load.

## Do the thing

Execute!

## References

1. [Learning how to learn](https://www.coursera.org/learn/learning-how-to-learn)
