---
title: Bicycles
description: Bikes are the best. Maintenance info and other stuff, too.
---

## Maintenance

### Brakes

#### Brake Adjustment for Rim Brakes[10]

1. Close the brake you want to adjust manually by holding it against the rim, as if you were pulling the brake. This should give more slack to your brake wire.
2. Where the brake lever on the handlebar meets the hinge, on the opposite side of the brake chassis is an adjustment screw with a locking nut attached. Loosen this until it stops or is about to come out. From here, your brakes will likely be _too_ tight.
3. Slowly tighten the screw until you can pull the brakes tight and there is about two fingers' width between the handlebar and the pulled brake lever, or until it's where you want it.
4. Tighten the locking nut until it is tight against the brake chassis. Your brakes should be adjusted properly.

### Sticky/Unresponsive Road Shifters

If you are experiencing difficulty with your road shifters, it is likely that the grease inside of the mechanism has accumulated dust and no longer catches the shifting thingy. It may be good to mount the bike or place it upside down (much harder) so you can shift easily.

Squeeze the brakes until you see the inside of the shifting mechanism. Spray a liquid penetrant like Pb Blaster or Kroil (**NOT WD40**) inside of the mechanism and try shifting up and down repeatedly for a couple minutes. You should notice an improvement, but it probably won't be fixed. Wait and repeat in a couple of hours.

Once finished, get some compressed air and flush out any gunk in there, and some isopropyl alcohol if you want to be super thorough.

### Derailleur Adjustment/Indexing[13]

You will want the bike mounted or flipped upside down so you can pedal the bike for shifting gears.

#### Set the high limit screw

On each derailleur, there is a high or "H" and a low or "L" screw. These maintain the limits of how far your derailleurs will go towards or away from the frame.

Shift to the smallest cog on your derailleur. Once there, turn your barrel adjuster for your shifter clockwise a few times. This is to ensure that we are setting the limit screw _first_ and not accidentally conflating the indexing for the limit placement.

Adjust the "H" screw to where the derailleur is perfectly aligned with the gear. You will see it move side to side. If you don't, you're probably turning the wrong way. If it's not moving enough, you may need to adjust the cable length.

#### Set the gear indexing

This will calibrate the gear shifting to not skip or go halfway on shift.

Turn your barrel adjuster counter-clockwise to make up for the adjustment you did earlier. It should be returned to it's initial setting before you turned it clockwise.

Start at the lowest gear. While "pedaling", shift to the next gear. This is when you are testing the fluidity of the shifting, so stop once the shifting is not occurring as expected.

If the shifting is not occurring as you'd like, you will want to adjust the barrel connectors until the derailleur is directly above the desired gear. On the front derailleur, it will be on the downtube; on the rear, it will be attached to the derailleur. As you adjust, you should see the derailleur move side to side.

Once the index is accurate, continue on to the next gears until complete.

#### Set to low limit screw

Adjust the "H" screw to where the derailleur is perfectly aligned with the gear. You will see it move side to side. If you don't, you're probably turning the wrong way. If it's not moving enough, you may need to adjust the cable length.

### Laterally Truing Your Wheels

#### Tools

-   Spoke wrench: Mine cost around 10 bucks from REI and looks like a circle with three nubs on it. Anything will do as long as it matches the size of your tire nipples, which can be figured out through the use of a caliper.
-   Zip Ties: Any will do as long as they are long enough to wrap around the fork/tube tightly and still have some length on the other side ([look at this video for reference](https://www.youtube.com/watch?v=Z-cPq5n-LlM)).
-   Needlenose Pliers: Only necessary if you have flat spokes.
-   Lubricant (e.g. Tri-Flow [something that is not *just* a penetrant]): The spoke nipples may be super tight or stuck, so it's worth having something like this to keep things loose. I have also read of using some kinds of oil, but this lubricant will do the trick.

#### Set Up

Start off by taking the tire and innertube off of the tire. Sometimes the tire and tube can obfuscate the actual shape and curvature of the rim.

Place the zip ties on the fork (for the front tire) or the frame (for the back) perpendicular to the rim. You want to place them and clip their length so they can eventually be rotated closer and closer to the rim as your rim gets more and more straightened out.

````
    / _______ \   <-- FRAME
   / /  TIRE \ \
  / /  /```\  \ \
  | |  |\/\|  | |
  )-(--|/\/|--)-( <-- ZIP TIES
````

Mount your bike or flip it over onto the seat so that the tires can rotate freely during testing.

#### Process

In short the process is:

-   find the area where the rim is most out of alignment
-   make a **very small** adjustment towards the desired alignment
-   repeat both steps until finished

Here is the process in detail:

1. Set the zip tie a few millimeters away from the edge of the rim. The goal at this step is to ensure that through a full rotation, the zip tie doesn't touch the rim, but gets close.
2. Rotate the zip tie inward towards the tire about as little as you possibly can.
3. Spin the wheel slowly, watching and listening for the zip tie to make contact with the edge of the wheel.
4. Repeat steps 2 and 3 **just** until a zip tie makes contact with the rim. You want the least amount of contact with the rim possible.
5. Once you make contact, manually rotate the wheel, watching and listening for the loudest (most out of alignment) section. Aim for finding an area about as wide as one or two spokes.
6. Find the spoke nearest to this section that goes up towards the side of the hub _opposite_ of the zip tie that is touching. For instance, if the zip tie on the _right_ is touching the rim, you want to find the spoke that meets the hub on the _opposite_ side of that zip tie.
7. Place the spoke wrench on that spoke nipple and tighten it a quarter turn. Some important notes:
    - **Tightening and loosening are done from the perspective from the tire side of the rim.** It will probably feel like you are doing it wrong. Quoting [bikebooboos.com](https://bikebooboos.com/repairs/wheeltruing/), "Imagine using a screwdriver to tighten the nipple from the tire side of the rim, then turn the nipple the same way using a spoke wrench on the spoke side of the rim, and you’ll be fine."
    - You almost **always** want to tighten your spokes instead of loosening them. Avoid loosening as this could cause your spokes to loosen up while riding and destroy your wheel's integrity.
    - Don't do more than a quarter turn at a time! It's tempting, but be patient.
8. Check the rim again, rotating the wheel around the same section. If you still hear the contact, go back to step 5 and repeat. If it has gone away, go back to step 2 and repeat.
9. Once this process has been done repeatedly, you should have a relatively laterally true rim.

## References

1. https://www.youtube.com/watch?v=yzLxyrVQ_00
2. https://www.youtube.com/watch?v=Ea03ChN-7Vg
3. https://www.cyclist.co.uk/tutorials/74/how-to-adjust-a-rear-derailleur
4. https://www.parktool.com/blog/repair-help/how-wheel-truing-works
5. https://www.youtube.com/watch?v=Z-cPq5n-LlM
6. https://bikebooboos.com/repairs/wheeltruing/
7. https://bikepacking.com/gear/bikepacking-hacks/
8. https://www.kevincyr.net/home-in-the-weeds
9. [Convert multi- to single-speed](https://yewtu.be/watch?v=EvBoHqozStw)
10. [Adjust brake tightness](https://inv.tux.pizza/watch?v=DHqolo-V1RI)
11. [Brake tune up](https://inv.tux.pizza/watch?v=VO77mbB8w7I)
12. [Changing your brake cables](https://inv.tux.pizza/watch?v=1Gs-OscFGFk)
13. [How to Adjust a Rear Derailleur – Limit Screws & Indexing](https://inv.tux.pizza/watch?v=UkZxPIZ1ngY)
