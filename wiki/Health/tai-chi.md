---
title: Tai Chi
description: Tai chi is a Chinese internal martial art.
---

Tai chi is a Chinese internal martial art. I've taken very few classes, but I find it a good complement to {{aikido}} via the footwork and alignment of cause/effect in the body.

## References

1. [Song Gong exercises](https://www.youtube.com/watch?v=mPV1MfVyMEE)
