# Beginner Training

## Introduction

Indoor rowing training isn't rocket science, though you might think so if you try to understand some of the training plans "on the market" for indoor rowers. The 'Pete Plan' evolved as a competition training plan designed for continuous improvement, and as something easy to pick up, follow, and understand. The original 'Pete Plan' is an intense training plan based around 6 sessions per week, designed for regular competition and testing. However, it is followed in such a way that you always use previous training sessions to set targets for future sessions, enabling continuous improvement, and allowing even a complete beginner to follow the plan. Jumping straight in to the full volume of 6 sessions a week would be tough for a beginner though, and so the following 24 weeks plan gradually increases the volume, and will introduce you to some of the key training principles for the indoor rower.

## The 24 week 'Pete Plan'

The 24 week training plan set out here is aimed at the beginner to intermediate indoor rower. The training plan is set out in a simple to follow format, with each week containing 3 core sessions, and for the keen trainer, 2 additional optional sessions. See the next section where this training plan is reproduced along with explanations for how to complete each session, and the key aims behind them. As such each week is set out with the 5 workouts, the core workouts first (completed on separate days), followed by the two optional workouts in [square brackets]. The idea here is that you complete the core workouts every week, and when you have the time and motivation you might pick one of the optional workouts on occasional weeks.

Note: Below this summary of the sessions you will find a full explanation of how to follow each session in the plan, so do scroll down and use those notes as you follow through the sessions. If anything isn't clear to you, please do email me at <thepeteplan@gmail.com> to ask.

### Week 1

* Day 1 = 5000m
* Day 2 = 6 x 500m / 2min rest
* Day 3 = 5000m
* Optional Day 4 = [20min]
* Optional Day 5 = [2 x 10min / 2min rest]
 Note: If even this volume of sessions looks too much, simply halve each session for your first time through the plan and begin with a 2500m row on Day 1, 3 x 500m on Day 2 and so on.

### Week 2

* 5500m
* 4 x 750m / 2min rest
* 5500m
* [20min]
* [3 x 8min / 2min rest]

### Week 3

* 6000m
* 2 x 2000m / 4min rest
* 6000m
* [5000m]
* [6 x 500m / 2min rest]

### Week 4

* 6500m
* 3 x 1000m / 3min rest
* 6500m
* [6000m]
* [2 x 2500m / 2min rest]

### Week 5

* 7000m
* 4 x 800m / 2min rest
* 7000m
* [20min]
* [2 x 10min / 2min rest]

### Week 6

* 7500m
* 3 x 2000m / 4min rest
* 7500m
* [5000m]
* [6 x 500m / 2min rest]

### Week 7

* 8000m
* 7 x 500m / 2min rest
* 8000m
* [6000m]
* [3 x 1500m / 3min rest]

### Week 8

* 8500m
* 4 x 1500m / 3min rest
* 8000m
* [25min]
* [3 x 1k / 3min rest]

### Week 9

* 9000m
* 4 x 800m / 2min rest
* 8000m
* [8000m]
* [2 x 10min / 2min rest]

### Week 10

* 9500m
* 3 x 2000m / 4min rest
* 8000m
* [8000m]
* [7 x 500m / 2min rest]

### Week 11

* 10000m
* 8 x 500m / 2min rest
* 8000m
* [25mins]
* [4 x 1500m / 3min rest]

### Week 12

* 10000m
* 4 x 1500m / 3min rest
* 3 x 10min / 2min rest
* [8000m]
* [4 x 800m / 2min rest]

### Week 13

* 10000m
* 4 x 1k / 3min rest
* 2 x 15min / 2min rest
* [8000m]
* [3 x 2k / 4min rest]

### Week 14

* 10000m
* 3 x 2k / 4min rest
* 4 x 8min / 2min rest
* [30min]
* [7 x 500m / 2min rest]

### Week 15

* 10000m
* 5 x 750m / 2min rest
* 3 x 10min / 2min rest
* [8000m]
* [4 x 1500m / 3min rest]

### Week 16

* 10500m
* 5 x 1500m / 3min rest
* 30min
* [10000m]
* [4 x 1k / 3min rest]

### Week 17

* 10500m
* 8 x 500m / 2min rest
* 2 x 15min / 2min rest
* [30min]
* [4 x 8min / 2min rest]

### Week 18

* 11000m
* 4 x 2k / 4min rest
* 30min
* [10000m]
* [4 x 1k / 3min rest]

### Week 19

* 10000m
* 5 x 800m / 2min rest
* 3 x 10min / 2min rest
* [30min]
* [4 x 2k / 4min rest]

### Week 20

* 12000m
* 5 x 1500m / 3min rest
* 30min
* [10000m]
* [8 x 500m / 2min rest]

### Week 21

* 10000m
* 4 x 1k / 3min rest
* 4 x 8min / 2min rest
* [12000m]
* [5 x 1500m / 3min rest]

### Week 22

* 12000m
* 4 x 2k / 4min rest
* 30min
* [3 x 10min / 2min rest]
* [5 x 800m / 2min rest]

### Week 23

* 10000m
* 8 x 500m / 2min rest
* 2 x 15min / 2min rest
* [10000m]
* [4 x 2k / 4min rest]

### Week 24

* 12000m
* 5 x 1500m / 3min rest
* 30min
* [2 x 15min / 2min rest]
* [4 x 1k / 3min rest]

## The 24 week 'Pete Plan' explained

### Week 1

* 5000m - The three key points you want to concentrate on during all of your erg sessions are technique, relaxation, and efficiency. Use the single distance pieces to work on the different elements of the stroke you read about in the technique section. Aim for a feeling of smooth acceleration through the drive, and to be slow and relaxed during the recovery.
* 6 x 500m / 2min rest - Interval sessions allow you to work at a higher intensity by splitting the workout into smaller segments. Don't get carried away in the early intervals thinking it is easy though, you have many weeks ahead to increase the intensity. As you become more experienced at interval training over the coming weeks you will learn how to pace the different types of session.
* 5000m - Look back at the average pace of your last 5000m session a few days ago. Aim to row at that pace consistently through this session, and don't give in to any temptations to go faster, especially towards the start. Remember "technique, relaxation, efficiency".
* [20min] - First week and you're already thinking about doing extra sessions? Make sure you're not over-doing it early on, rest is a very important part of any training plan. Whether your 5000m sessions have been taking you more or less than 20mins, aim to row this session at a slower pace, concentrating on making each stroke more efficient than the last.
* [2 x 10min / 2min rest] - 5 sessions in the first week, you must be keen. Have a go at your first longer interval session. Breaking up the 20min row like this allows you a short break to regain your full concentration, and make sure to take on some water while you can. Row the first 10min at the same pace as your 20min session, and then aim to just beat the distance on the second 10min.

### Week 2

* 5500m - You'll see a pattern develop over the first few weeks of your new training plan. These single distance sessions will increase by 500m each week, building the duration gradually up. As your fitness will be increasing each week, and your technique improving, you should find that despite the increase is distance, the pace can be kept the same as the 5000m rows from last week.
* 4 x 750m / 2min rest - The intervals are 50% longer this week, but the total distance of the session is the same. Look back at your average pace from the 6 x 500m session last week, and try to row each interval at exactly that pace. Don't go faster, no matter how tempted you feel!
* 5500m - Same story as last week, aim for the same pace as the 5500m row a few days ago, but aim to row it as consistently as possible, keeping the pace the same on every stroke.
* [20min] - It's very likely now that 20min is less than it took you to row the 5500m this week, so aim to row this at the same pace as the 5500m rows this week.
* [3 x 8min / 2min rest] - If you did the 5th session last week, aim for the same pace, if not, a similar pace to the 20min session will be about right.

### Week 3

* 6000m - The distance is still increasing by 500m each week, but your fitness and technique will be progressing rapidly too, so I bet you want to row further than this? Well keep building up gradually, and keep logging your sessions in the training diary, and you can look back in a few weeks time at the progress you've made. Again, same pace as the 5500m last week, if you can.
* 2 x 2000m / 4min rest - This will be the first time you've tried longer intervals with a reasonably long rest time. Not too dissimilar to the 5th sessions of weeks 1 and 2 if you attempted those, but the longer rest and slightly lower distance mean you can push the pace a little more. Still focus on technique and efficiency, but try to go about 2seconds faster pace than the 6000m session of a day or two ago.
* 6000m - Again, aim for the same pace as your first 6000m session a few days ago. Don't worry if these sessions feel harder some days and you don't make your target. Many things can affect your performance from fatigue from previous sessions, to environmental conditions such as temperature, to hydration and nutrition. Any day you don't perform so well, try to pinpoint anything you might have done differently, and note it down.
* [5000m] - Aim for the same pace as you did for your 6000m in the last session, and try to speed up a little then within the last 1000m.
* [6 x 500m / 2min rest] - Look back in your training diary to week 1 when you last did this session. Start out 1second faster pace and see how you get on.

### Week 4

* 6500m - You should be getting the idea with these session now, 500m more each week, aiming for the same pace. These two sessions are the core of your training at the moment, and in a few weeks time you'll realise just how much improvement you've made.
* 3 x 1000m / 3min rest - A new interval session now, working around the same intensity as the 6 x 500m session you did in week 1 (and perhaps as an optional session last week). Try for the same pace you did for that session in week 1.
* 6500m - As day one of this week.
* [6000m] - You might be starting to develop some judgement by now of how to pace the sessions as the distance changes slightly. Aim just to go for the same pace as your 6500m sessions this week for this one.
* [2 x 2500m / 2min rest] - Another different interval session to try, but very similar to session 5 in week 1 if you attempted that. When you're looking at how to pace a session, always look back in your diary for the most recent similar session, and see what the differences are. For this session, aim to go 1 second faster pace than the 6000m in the last session.

### Week 5

* 7000m - You know what to do on these now.
* 4 x 800m / 2min rest - You've been training for a month now, and your muscles are getting used to the workload you're putting them under, so now it's time to start challenging yourself a little more each week. Go for the same average pace you managed in the 3 x 1000m session last week, but when it comes to the last interval, see how fast you can do it, but maintaining good technique.
* 7000m - As always.
* [20min] - On these supplementary sessions judge by how you feel, and how hard you've worked on the other sessions. If you're pretty tired still from the last session, but want to train, just concentrate on technique, and don't worry about the pace. At this stage all technique work is good. If you feel good, go for 1 to 2 seconds faster pace than you did for the 7000m rows this week.
* [2 x 10min / 2min rest] - Just aim for a similar pace to the last session.

### Week 6

* 7500m - Who would have thought 6 weeks ago that you'd be rowing 50% further on these sessions now, but still at the same pace? Use the great improvements you've made so far as motivation to add in one or two of the optional sessions this week if you've not tried them yet.
* 3 x 2000m / 4min rest - Back in week 3 you did a 2 x 2000m session. Aim for the same pace, and see if you can manage to maintain it on that 3rd rep.
* 7500m - You know what to do.
* [5000m] - You want to do more? Aim to start this one at the same pace as the 7500m sessions this week, but once you get past half way try speeding up the pace by 1 second every time you feel able to.
* [6 x 500m / 2min rest] - These should feel quite short after those 800's you did last week. Use your judgement for how fast to pace them, but beware going too fast on the early reps or you might meet Mr Lactic Acid, who you will become well acquainted with in the weeks to come.

### Week 7

* 8000m - You've been doing these sessions for 6 weeks now, so it's time to start taking note of some other parameters as well as the pace. Look back in your training log to see the average stroke rate you've been doing for these sessions so far. If it's 24 or under, that's great, but if it's over 24, try to focus during this session on making every drive count, and being relaxed and slower on the recoveries. Don't go for a big jump in rate if you've been rowing above 24spm, but try just to lower it by 1 or 2 by slowing down the recovery.
* 7 x 500m / 2min rest - One more rep than you did for this session in week 1 (and possibly in the optional sessions in weeks 3 and 6). If you did the 6 x 500m session last week, aim for the same pace, but if you've not done this session since week 1, aim to be 1 to 2 seconds faster pace, but use your judgement as to whether this is too fast on the early reps.
* 8000m - Again, concentrate on bringing your stroke rate down to 24 or below now for these sessions.
* [6000m] - On the optional sessions remember to judge how well recovered you are from the previous sessions in the week, and aim for a pace based on that. Perhaps aim for the same pace as the 8000m sessions this week, but at a lower stroke rate.
* [3 x 1500m / 3min rest] - Last week you did 2000m reps, so go for a pace 1sec faster than you managed for that last week. Remember to try to keep the pace consistent across each repetition.

### Week 8

* 8500m - Technique, relaxation, and efficiency.
* 4 x 1500m / 3min rest - If you did the optional session last week you'll have a good idea how to pace this one. If not, aim for the same pace as you did the 2000m reps in week 6. See if you can go slightly faster on the 4th rep - although there are 4 reps this week, the total distance is still 6000m.
* 8000m - Your second single distance session this week is slightly shorter than the first. Aim for the same pace over the first 6000m, then see if you can speed up a little over the final 2000m.
* [25min] - Rowing to a set time, rather than distance, doesn't affect how to pace a training row. Still aim for a stroke rate of 24 or just below, and a constant pace throughout, perhaps 1sec faster than the previous 8000m session.
* [3 x 1k / 3min rest] - See if you can row this at the same pace as the 7 x 500m last week. Start to notice the stroke rate on these interval sessions too now, and how it compares to the (slower paced) single distance or time sessions.

### Week 9

* 9000m - You've been going for a full 2 months now, and you're probably starting to look ahead and wonder when these sessions will stop getting longer! Don't worry, you'll get a break from the increasing distance soon, and then you can start increasing the pace instead!
* 4 x 800m / 2min rest - Week 5 was the last time you did this session, a full month ago. Look back in your training diary and the pace you did then will probably seem like an easy target now. Don't get too ambitious yet though. If you really feel you've come a long way over the past 4 weeks, aim for 1sec faster pace, otherwise go for the same pace and use whatever energy you have left for the final rep. This session should be the one where you're breathing the hardest each week, and where you're always looking back for a target to beat.
* 8000m - Don't forget your main aim is always to work on technique. Periodically have a look back in the technique section of this guide to make sure you're not picking up any bad habits.
* [8000m] - More of the same.
* [2 x 10min / 2min rest] - Aim to row the first piece just a second or so faster than the 8000m row, and then beat it on the second piece.

### Week 10

* 9500m - Have a look back through your training diary and see how the pace and rate of these single distance rows has been progressing. You should have been holding the pace the same week by week as the distance has increased, but your stroke rate has probably been dropping gradually too.
* 3 x 2000m / 4min rest - Look back to week 6 when you last did this session, with a similar session in week 8. Go for a pace 1 second faster than you did in week 6, and faster still for the last rep if you can.
* 8000m - While the second steady distance sessions are a little shorter than the first each week, aim to row the first 6000m at the same pace as the earlier row, and then gradually speed up to the end, known as "negative splitting".
* [8000m] - As the last session.
* [7 x 500m / 2min rest] - Look back to week 7 when you last tried this session. Aim to row the first 6 reps at the same average pace you managed last time, and then go faster for the final rep.

### Week 11

* 10000m - You should feel very good about yourself after this one. Double the distance of your first session back in week 1.
* 8 x 500m / 2min rest - An extra rep this week on your 500m intervals. This is a great session for working on your basic speed, and very good for preparing you to row the golden distance, 2000m. Aim for the same pace you managed back in week 7 on the first 7 reps, and then faster for the final rep.
* 8000m - You're up to a full program of work now, especially if you're doing one or two of the optional sessions each week. If you don't want to row too hard on this session, use a rate restriction to limit yourself. Limit yourself to a maximum rate of 2 or 3 lower than you would normally for these sessions, and see how this affects the pace. (Hint - you will probably have to go a little slower!)
* [25mins] - Try to play little games to make the time pass quicker, and to keep working on your technique and efficiency. Count how many strokes in a row you can hold your target split, once you miss it start counting again, and try to beat your previous total.
* [4 x 1500m / 3min rest] - Week 8 was the last time you did this session. Now that you're doing the same session every few weeks, use the following method to know how to pace them. Take your average pace from the previous time you did the session, and row that pace for all but the last rep. On the final rep try to go a little faster, and note down your new best average pace for the session.

### Week 12

* 10000m - For the first time in the training plan you aren't increasing the distance on these sessions any more, what a relief! You'll be sticking to this distance for another 3 weeks too. Each time aim to improve on the average pace slightly, even if only by a tenth of a second.
* 4 x 1500m / 3min rest - If you did this as the optional session last week, go for the same pacing plan again. If you've not done it since week 8, see the description for how to pace it last week.
* 3 x 10min / 2min rest - On all interval sessions, aim to either go at the same pace during all intervals, or gradually increase the pace through the reps. Try never to go at such a pace that makes you slow down by the end of the session. Do the first two reps at the same pace as your 10k this week, and see if you can go a little faster on the final one.
* [8000m] - Now that you're up to a full volume training plan, and beginning to increase the intensity, you need to take some sessions a little easier. For the optional single distance sessions try to keep the stroke rate right down, between 20 and 22spm, to limit your pace on them.
* [4 x 800m / 2min rest] - Look back to week 9 in your training diary, and remember the principles for how to pace these sessions now. Go at the average pace for that session for the first 3 reps, and then try to go a little faster on the final one.

### Week 13

* 10000m - Again, try to improve on your average pace from the 10000m row last week. To ensure you are never in danger of not completing a session, set out at the same pace you achieved last time for the first three quarters or so of the row, and only then begin to speed up gradually.
* 4 x 1000m / 3min rest - Slightly longer reps that the session in week 9 (or the optional session last week), but also the rest time is increased by a minute. Try to go for the same pace you managed in week 9, and your increased fitness should see you do a fast last rep!
* 2 x 15min / 2min rest - Go for the same pace you achieved on the 3 x 10min last week, and remember to try to make the second rep slightly faster than the first.
* [8000m] - Keep the stroke rate low, and concentrate on technique.
* [3 x 2000m / 4min rest] - Aim for the same pace as the 4 x 1500m last week.

### Week 14

* 10000m - Keep those improvements coming, the faster you row, the quicker you finish!
* 3 x 2000m / 4min rest - Aim for the same pace as the 4 x 1500m in week 12.
* 4 x 8min / 2min rest - Go for the same pace on the first 3 reps as the 2 x 15min last week, and see if you can build the pace during the last rep to finish fast.
* [30min] - With a low rate, and concentrating on technique, have a look back in your training diary a few weeks and you'll probably find you're going faster than you were doing this sort of duration at then, at a lower stroke rate, and without trying too hard!
* [8 x 500m / 2min rest] - In week 11 you completed this session. Do the first 7 intervals at the same pace you averaged for that session, and aim to go faster on the last rep.

### Week 15

* 10000m - You're probably getting into this indoor rowing lark by now, impressing your mates with split times and stroke rates. You must really want a new 10000m personal best to tell them about tomorrow. Aim for the stroke rate of 24spm, and go for the same split time as last week for the first 8000m, and then speed up over the last 2000m just for those bragging rights!
* 5 x 750m / 2min rest - This is similar to the 4 x 1000m session from week 13. Aim for the same pace as you achieved then over the first 4 reps, and then try to go faster for the final rep.
* 3 x 10min / 2min rest - Very similar to the 4 x 8min last week. Aim for the same pace you achieved then for the first 2 reps, and then faster on the final rep.
* [8000m] - You went quite hard on the 10000m session this week to show off to your mates, so take it easier on this one. Restrict the stroke rate as you have done before to a maximum of 22spm, focus on technique, and don't worry too much about the pace.
* [4 x 1500m / 3min rest] - You'll be doing 5 reps of this session next week, so use this as a good indicator of how fast you can pace that. Go for the same pace as you managed for the 3 x 2000m last week, but resist the temptation to do a faster last rep, unless it feels really easy!

### Week 16

* 10500m - Oh no, an increase in distance again! 10000m is a great longer distance training session for indoor rowers, but going "over distance" sometimes and rowing a bit further brings the 10000m further inside your capability range.
* 5 x 1500m / 3min rest - Did you have a taster of this session last week on the optional session? If not, look back to week 13 and the 3 x 2000m session. Aim for the same pace as you achieved then.
* 30min - Aim for the same pace you achieved on the 10000m row in week 15. 30mins is less time than it took you to cover 10000m, so you should have some energy left to speed up in the final minutes.
* [10000m] - You're putting in a lot of distance each week now, so it is becoming even more essential to know when to take it easy. Restrict the rate again to a maximum of 22spm, and concentrate on technique, and an efficient delivery of power during the drive.
* [4 x 1000m / 3min rest] - You need to know when to take it easy, but you also don't want to go slower than you've gone on a session before! You haven't done this session since week 13, so look back in your diary at the pace you achieved then, and stick to that for the first 3 reps, with a faster final rep.

### Week 17

* 10500m - Consolidate your distance work by going for the same split time you managed last week for this session.
* 8 x 500m / 2min rest - Look right back to week 11 for the last time you did this session. I bet that pace look easy now, 6 weeks on, doesn't it? Small improvements are the way to go, however, so aim for the same average pace as last time for the first 7 reps, and then give everything you have for a fast final rep!
* 2 x 15min / 2min rest - Aim for the same pace you managed for the 3 x 10min in week 15.
* [30min] - Use the rate restriction and again work on technique. Keeping at a maximum of 22spm, think about the correct sequence of actions during the recovery, and maintaining relaxation.
* [4 x 8min / 2min rest] - Look back to week 14 for a reference pace from your training diary, you know what to do.

### Week 18

* 11000m - As before, aim for the same pace as you did for the 10500m session last week.
* 4 x 2000m / 4min rest - In week 14 you did this session with 3 reps. Do the first 3 reps at the pace you managed for that session, and the month of work you've done since will allow you the extra rep.
* 30min - Row the same pace you did for the 11000m for the first 20mins, and then speed up every couple of minutes through to the end.
* [10000m] - Try restricting the stroke rate to a strict 20spm for the first half of this row, working on technique. From half way allow yourself to increase the rate only with a corresponding increase in pace.
* [4 x 1000m / 3min rest] - You last did this session in week 16, or week 13. Aim for the same average pace you managed on your last attempt for the first 3 reps, and then all out for the final rep.

### Week 19

* 10000m - For the past 3 weeks you have rowed over 10000m for your first session of the week. Look back in your diary to week 15 when you last did this, and see if you can beat that personal best time. If you ever have a session where you feel you can't maintain the pace, just back off slightly and slow down, but try never to fail to complete the session.
* 5 x 800m / 2min rest - Back in week 15 you did 5 x 750m. This session only has another 6 or 7 strokes per rep, and 6 or 7 strokes is nothing really, is it? Just row the first 4 reps at the same pace you managed in that session, and show yourself how much mental toughness you've built up over the last 19weeks by how fast you can do that last rep.
* 3 x 10min / 2min rest - You should have got the hang of these sessions by now. They are effectively the same as the single time or distance pieces, with the short rest breaks allowing for you to take on some water, stretch your legs for a few seconds, and refocus. If you're ever training on a very hot day, you can split up any long distance piece with short rests like this, just try to make sure the rest is never longer than a quarter of the previous piece.
* [30min] - Restrict the stroke rate to 22 or lower, and concentrate solely on technique, not worrying about the split time at all.
* [4 x 2000m / 4min rest] - You only did this session last week, so aim for the same pace if you feel up to it. If you don't feel physically or mentally up for equaling a recent time for one of the interval sessions just restrict the stroke rate, and aim for a slower target pace. Just make sure to note in your training diary exactly what you did so you know when reading back.

### Week 20

* 12000m - This is the further you'll have rowed in a session before. Ease into the row nice and slowly, perhaps 5 seconds slower in pace than you did the 10000m row last week. Just slowly increase the pace over the first half, and aim to be hitting that 10000m pace by about 5000m to go. If you can, keep "negative splitting" for the remainder of the distance.
* 5 x 1500m / 3min rest - You did this session last in week 16, but since then you have done 4 x 2000m once or twice. Compare the paces from the two different sessions, and you should be fairly similar in pace between the two sessions. Use the fastest pace of the two as your target for the first 4 reps, and as always, aim to go faster on the last rep if you can.
* 30min - You started off slowly on the 12000m row this week, so try to make this one steady all the way. Aim for the best pace you've held yet over 10000m, and try the game of counting how many strokes in a row you can hold the same split time for. Do you remember how many you managed last time? Can you beat it?
* [10000m] - Restrict the rate, work on technique, relaxation and efficiency, don't worry about the pace.
* [8 x 500m / 2min rest] - Look back to week 17 when you last did this session. Can you beat the average pace you managed that time? This session will be a good indicator of the pace you could try a single 2000m row at, after you've completed the full 24 weeks of this training plan.

### Week 21

* 10000m - Begin this row at a pace 2 seconds slower than your best 10000m previously. Gradually increase the pace up to half way, aiming to be at your pb pace by this stage. Hold this pace for another 2000m, and then see if you can gradually increase the pace for the final 3000m - how close can you get to your pb pace in the end?
* 4 x 1000m / 3min rest - Look back to find the last time you did this session, and also the last time you did 8 x 500m, and see how the paces compare between them. 8 x 500m will be faster, but by how much? You have probably realised that the sessions all fit into a few groups of similar workouts, and it is useful to see the relationships between the paces of sessions in these groups, so you can compare to more recent sessions, not necessarily to exactly the same session.
* 4 x 8min / 2min rest - Start thinking of these sessions as a steady distance piece with short rests to re-hydrate and refocus, rather than an interval session. Aim for a similar pace to that you would for a 30min single piece.
* [12000m] - Aim for a steady stroke rate of 20spm, and don't worry about the pace. Switch the display to read calories or watts if you want to so that your focus isn't on how fast you're going. Just concentrate on easy efficiency.
* [5 x 1500m / 3min rest] - You only completed this session last week, so don't expect big improvements like you'd get if it was 3 weeks ago you last did it. Aim for the same pace you managed last week.

### Week 22

* 12000m - Aim for a steady pace a couple of seconds slower than you managed on the 10000m row last week. When you do these longer rows, look back through the memory on the machine afterwards to see if you rowed it at as consistent a pace as you thought you did.
* 4 x 2000m / 4min rest - As discussed in week 20, this session and the 5 x 1500m session are very closely related. You should be able to get a good idea how the pace of the two sessions compare to each other now, but if in doubt, always aim for the same average pace you last managed on the exact session, for all but the last rep. On the last rep of these sessions always try to go slightly faster than the previous reps.
* 30min - Try a strict rate restriction of 20spm for this row. The 30min at 20spm, or 30r20, session is a popular one with rowers and ergers alike, and it's another good one to compare your performance with others, and baffle your mates with why it makes a different what your spm was.
* [3 x 10min / 2min rest] - You know now to treat these sessions like any other distance piece. Put a rate restriction on and don't worry about pace if you feel you need an easier session, or aim for a solid 30min pace, close to your best, if you feel good.
* [5 x 800m / 2min rest] - This session fits in the same group as the other 1000m and less reps sessions, working on your higher end pace. Look back to week 19 for the last time you did this session, and dig deep for the desire to destroy the pace you did then. For the first 4 reps, hold back that desire by going at the same pace, then unleash it all on the final rep and show the erg who's boss!

### Week 23

* 10000m - How did you row your best 10000m time so far? Was it by rowing at a steady pace all the way, or negative splitting and getting gradually faster as you did 2 weeks ago? Whichever method you found best for you, try that again with this session and see if you can set a new 10000m pb.
* 8 x 500m / 2min rest - Look back in your training diary for your last completed go at this session, and find the target pace as you have before.
* 2 x 15min / 2min rest - Do you feel good? If so, row the first 15min slightly slower than your best ever 30min row, then try to beat yourself on the second 15min!
* [10000m] - Take it easy, especially if you plan on doing the 5th session this week. Restrict the rate to 22 or less, with a corresponding pace target.
* [4 x 2000m / 4min rest] - You only did this session last week, can you match it, or better still, beat it by doing the final rep faster?

### Week 24

* 12000m - Keeping the rate at 24 or less, always work on technique during these longer pieces. Don't work too hard to make sure you recover well for the interval sessions.
* 5 x 1500m / 3min rest - Look back to week 20. You're into the final week of the training plan now, so you want to finish on a high with a pb on one of the key sessions in your training plan. Finish the first 4 reps with the minimum effort possible to hit your target, and then prove how tough you are by beating the target on the final rep.
* 30min - You should be experienced at using your judgement now, so pace this according to how you are feeling from the training volume so far this week.
* [2 x 15min / 2min rest] - Remember to treat this as though a single distance session, and aim for the second piece to be slightly faster than the first.
* [4 x 1000m / 3min rest] - Week 21 was the last time you completed this session. Your final session of the 24 week plan, so think about the improvements you've made during the past 6 months, and think back to those first few sessions in week 1.

## The desired training effect

Read this short section if you're interested in the training effect you are gaining for each group of sessions. You learnt whilst completing the 24 week training plan (or from reading through what you'll be doing over the next 24 weeks) that the sessions fit into a few distinct groups. There are only three groups overall, so you'll now find out which sessions fit together, and what the desired training effect is from those sessions.

### Group 1 - Endurance (distance) training

Endurance training is really where you make the big gains in fitness, and should always be the backbone of any training plan. Without the basic fitness, no amount of power or strength is going to help. Towards the later stages of the 24 week 'Pete Plan' these endurance sessions were in two formats, single pieces (such as 10000m & 30min) and longer intervals split by short rests (such as 2 x 15min & 3 x 10min). Remember that the rest times when you split the distance into intervals should be no more than a quarter of the preceding interval time.

### Group 2 - Speed Endurance (AT) training

You might hear other people talk of this group of training sessions as anaerobic threshold (AT) sessions, as they are very good at improving your AT. As you're unlikely to have had the necessary physiological testing to be training to strict heart rate limits, you can just think of them as speed endurance training sessions. They are there to improve your endurance at faster paces, by rowing for extended periods followed by comparatively long rests compared to the pure endurance sessions. The total distance tends to be less than the distance sessions too, to enable you to work at a higher pace. The sessions you have been rowing in this group are 4 x 2000m and 5 x 1500m. Generally the rest time between this intervals will be close to half of the preceding interval time.

### Group 3 - Speed training

The final group of sessions is designed to work on your pure speed, around the pace you would row a single 2000m test piece at. Doing one of these sessions (such as 4 x 1000m, 5 x 800m & 8 x 500) once every 1 to 2 weeks is a good part of a balanced training plan, but if you're not planning on doing a test piece for 2000m in the near future, these are the least vital sessions in the plan. The rest time between intervals on these sessions would generally be around 1 to 1, ie resting for a similar time to the preceding work interval.

## What next?

You've completed the 24 week plan, so what do you do next? If you've not tried a 2000m timed piece yet you might like to try one now, and then you'll know how you compare to other people in your peer group. If you tried a 2000m row before beginning the 24 weeks of training, you can see the improvement you've made. Look back to week 23, and the 8 x 500m session you completed. This is roughly the pace you should be able to hold for a 2000m row now, so that will give you a good idea how to pace it. Think about some of the different pacing strategies you tried during the plan, and whether to negative split, or go for a constant pace. Finally, believe in your training, and treat it as any other session - make sure to do a good warm up though!

If you have found this document useful please feel free to make a small donation to aid the development of further training information for thepeteplan.com, thanks!

<https://thepeteplan.wordpress.com/beginner-training/>
