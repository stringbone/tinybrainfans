---
title: Rowing
description: Rowing is a low impact cardio workout.
---

## Beginning

-   Focus on form[1]. It's weird and unintuitive until you figure it out. Push don't pull.
-   Don't overdo it. Do beginner workouts from people like Dark Horse Rowing[2] and be a beginner.
-   Slower stroke rates are harder in a way, but they let you focus on form.

## References

1. https://www.youtube.com/watch?v=zQ82RYIFLN8
2. https://www.youtube.com/@DarkHorseRowing/videos
3. [The Pete Plan](https://thepeteplan.wordpress.com/beginner-training/) (or [a Markdown file, ~38kb](_peteplan.md))
