---
title: Aikido
description: 'Aikido is a martial art, roughly translated to "the way of harmonious spirit".'
---

## Reading/Philosophy

-   [Aikido Training Needs an Overhaul by Hoa Newens - Aikido Journal](https://aikidojournal.com/2024/01/09/aikido-training-needs-an-overhaul-by-hoa-newens/)
-   [Aikidoists Beware, By T.K. Chiba](https://web.archive.org/web/20120303145110/www.aikidoonline.com/articles/shihankai_articles/chiba/Chiba_Aikidoists_Beware.php)

## Basics

> O'Sensei once said that a student could learn all the basics of aikido if he practiced just three techniques: tai-no-henko, morotedori-kokyuho, and suwariwaza-kokyuho.[6]

[Aikido Essential Teaching Syllabus : From Basic to Black Belt - Part 1](https://inv.tux.pizza/watch?v=El_1_0DBNds&list=PLw0_y1TbRNIydC0aJhWKeycTNcNY8DUWf&index=0)

### Stretches

-   [Solo practice: Aikido stretches and breathing exercises](https://inv.tux.pizza/watch?v=Lsokcpj-L9U)
-   [Aikido Jo Basics - Exercises for Warmup, Stretching, Acupressure - Aiki-Jo](https://inv.tux.pizza/watch?v=k8K7OSNLUTM&list=PLw0_y1TbRNIydC0aJhWKeycTNcNY8DUWf&index=10)

### Techniques

-   [Ashi-sabaki, the basic steps](https://www.youtube.com/watch?v=2Q00sIh3v4I)
-   [Rolling](https://inv.tux.pizza/watch?v=coBaGhZ3siI)
-   Katatedori kaitenage - [soto](https://inv.tux.pizza/watch?v=c1nhaYBZ2No) and [uchi](https://inv.tux.pizza/watch?v=WlBIXh4MAKo)
-   [Katatedori uchi kaitenage](https://inv.tux.pizza/watch?v=WlBIXh4MAKo)
-   [Katatedori soto kaitenage](https://inv.tux.pizza/watch?v=c1nhaYBZ2No)

#### 5th Kyu Training

-   [Birankai Kyu Test Guidelines](https://birankai.org/wordpress/wp-content/uploads/2019/01/Birankai_Kyu_Guidelines.pdf)
-   [Birankai 5th Kyu techniques](https://inv.tux.pizza/playlist?list=PLpCZoQA1qrB-VXtuNuB_E0KfXxyaYGj6o)

-   [Katatedori shihonage omote](https://inv.tux.pizza/watch?v=nWG6o7qsCHw)
-   [Katatedori shihonage ura](https://inv.tux.pizza/watch?v=RNWhnVq4GGg&list=PLpCZoQA1qrB-VXtuNuB_E0KfXxyaYGj6o&index=8)
-   [Kokyudosa/Kokyuho](https://inv.tux.pizza/watch?v=7Xb3W4Ky9_I) ~1:40
-   [Katadori ikkyo](https://inv.tux.pizza/watch?v=7tfW2XiGoFc&list=PLpCZoQA1qrB-VXtuNuB_E0KfXxyaYGj6o&index=6)
-   [Katadori ikkyo](https://inv.tux.pizza/watch?v=MxJhjE4-hgI)
-   [Katadori ikkyo omote](https://inv.tux.pizza/watch?v=8bPWEVyc4f4)
-   [Katadori ikkyo ura](https://inv.tux.pizza/watch?v=uJJNUuxAOAc)
-   [Katatedori ikkyo omote](https://inv.tux.pizza/watch?v=64-GOwQsJYU&list=PLpCZoQA1qrB-VXtuNuB_E0KfXxyaYGj6o&index=3)
-   [Katatedori ikkyo ura](https://inv.tux.pizza/watch?v=pmwRBUSjADQ)
-   [Shomenuchi ikkyo](https://inv.tux.pizza/watch?v=kGi_0CeBYdQ&list=PLpCZoQA1qrB-VXtuNuB_E0KfXxyaYGj6o&index=1)
-   [Shomenuchi ikkyo](https://inv.tux.pizza/watch?v=yl2vDO42WhY)

## Demos/Classes

-   [My old teacher John Brinsley](https://inv.tux.pizza/watch?v=C3OTa0bMg-w)
-   Hiroshi Ikeda videos
    -   https://hiroshiikeda.gumroad.com/l/HI-ZA?layout=profile
    -   https://hiroshiikeda.gumroad.com/l/HI-KOSHI?layout=profile
    -   https://hiroshiikeda.gumroad.com/l/HI-BUKI?layout=profile
    -   https://hiroshiikeda.gumroad.com/l/HI-IRIMI?layout=profile

## Suwari Waza

### Shikko

-   [Aikido: Shikko (movement on the floor)](https://inv.tux.pizza/watch?v=Cl7Zn9IzVqk)
-   [Shikko from Leonardo Sodre](https://inv.tux.pizza/watch?v=6prbbxFVUgw)
-   [Hiroshi Ikeda's "Za"](https://hiroshiikeda.gumroad.com/l/HI-ZA?layout=profile) ([youtube](https://inv.tux.pizza/watch?v=Rf7QcZGwNZg))
-   [Shikko Exercises](https://inv.tux.pizza/watch?v=gyw_i4ULc9c)
-   [Ushiro shikko](https://inv.tux.pizza/watch?v=55V73kNEdbs)

### Further technique

-   [Suwari Waza Kokyu-ho/Kokyudosa](https://blackbeltwiki.com/aikido-suwari-waza-kokyu-ho)

## Weapons

### Jo

-   [Staff spinning](https://inv.tux.pizza/watch?v=BeKEYPFW_bg)
-   [Muso Gonnosuke and the Shinto Muso-ryu Jo by Wayne Muromoto](https://kingfisherwoodworks.com/pages/jo-the-wooden-staff-of-japan)
-   https://blackbeltwiki.com/bo-staff-techniques

### Bokken

-   [Kirikaeshi](https://inv.tux.pizza/watch?v=esZDGv3QFno)

#### 8 Suburi[1-5]

Every step is done once in each stance (right then left hanmi) with each ending with the following steps:

-   Step forward and tsuki.
-   Tenkai while raising your bokken to the crown of your head and shomen.

These will be written as if you were in right hanmi, so change all references when doing the left side.

1. Step backward and shomen.
2. Step forward and shomen.
3. Tenkan while cutting upwards; then pivot with the bokken at the crown of your head and shomen.
4. Tenkai and step forward while cutting upwards; then pivot with the bokken at the crown of your head and shomen.
5. Drop the tip of your bokken to the right to guard against attack. While doing this, pivot on your front foot, moving your back foot until you face 90° to the right of where you started, and shomen.
6. Drop the tip of your bokken to the right to guard against attack. While doing this, bring your back foot just in front of your front foot, pivot 90° to the right of where you started, step back and shomen.
7. Drop the tip of your bokken to the right to guard against attack. While doing this, tenkan, bringing the bokken to the crown of your head as you finish, and shomen.
8. Bring your right foot out to the right side, placing your left in it's place. While doing this, drop the tip of your bokken to the left to guard against attack, bringing the bokken to the crown of your head at the end of the movement, finishing with a shomen.

### Biking with them

Bokken, just put in a bag in pannier or strap to back with rope. Jo, strap to top tube and rack. If it's raining, consider a waterproof container, like a PVC tube with a screw top for both options.

## Glossaries

-   https://asu.org/student-handbook/glossary-of-aikido-terms/
-   https://aikido-amsterdam.com/files/aikido-extended-glossary.pdf

## References

1. [8 Bokken suburi article](http://www.aikidofaq.com/practice/bokken_kata.html)
2. [8 Bokken Suburi with Chiba Sensei](https://inv.tux.pizza/playlist?list=PLZfH1IaqQe11pJhBu8_QW4YLgQdvJbeWT)
3. [Ep.002 Chiba Sensei 8 Bokken Suburi](https://inv.tux.pizza/watch?v=VKAQ_VWEtWc)
4. [Bokken Basics and 8 Suburi - Introduction to Chiba Sensei's Weapons System #1](https://inv.tux.pizza/watch?v=zYEHUeGgXU0)
5. [Suburi 1-8 Aikido Horii Etsuji Sensei](https://inv.tux.pizza/watch?v=O3U8AZ5bhpA&list=PLZfH1IaqQe11pJhBu8_QW4YLgQdvJbeWT&index=10)
6. [Three Techniques That Have Everything](https://web.archive.org/web/20130227233816/http://www.traditional-aikido.com/Technique/three_techniques.htm)
