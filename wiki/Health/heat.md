---
title: Living With Heat
description: Living with the heat is our future, and here are some ways to manage it.
---

## Signs of Heat Illness

### Heat Stroke

-   Extremely high body temperature (above 103 degrees F) taken orally
-   Red, hot and dry skin with no sweat
-   Rapid, strong pulse
-   Dizziness, confusion or unconsciousness

If you suspect heat stroke, call 9-1-1 or get the person to a hospital immediately. Cool down with whatever methods are available until medical help arrives. Do not give the person anything to drink.

### Heat Cramps/Heat Exhaustion

Cramps:

-   Muscle pains or spasms in the stomach, arms or legs

Exhaustion:

-   Heavy sweating
-   paleness
-   muscle cramps
-   tiredness
-   weakness
-   fast or weak pulse
-   dizziness
-   headache
-   fainting
-   nausea
-   vomiting

If you have signs of heat cramps or heat exhaustion, go to a cooler location and cool down by removing excess clothing and taking sips of sports drinks or water. Call your healthcare provider if symptoms get worse or last more than an hour.

## General Best Practices

From @welshpixie@mastodon.art[1]:

> -   Soak your feet in a tub of cold water.
>     -   Better if you've got a fan pointed in your general direction; soak your feet until they acclimatise to the water temperature, remove feet and let the fan cool your wet feet down with evaporation, place feet back in water, repeat.
> -   Soak a tea-towel and place it about your person; on your neck, forehead, top of head, chest, thighs, wherever. As one body part gets used to it, move it to another, and rotate.
> -   Water spritzer, especially in conjunction with a wind blowy device.
> -   Brief cold showers, focusing on your armpits, underboobs, subgroinular area, head, and feet. COOL YOUR PITS, PEOPLE
> -   If you have boobs, fill a cup with ice cubes and snug it in there betwixt the boobies.
> -   Drink lots of water, obvs.
> -   If you're at home and it's convenient, put a couple of inches of cold water in the bath and periodically lie in it. Make this process easier by wearing your bathers around the house. (Or just go nekkid if that's your thang.)
>
> You want to keep your core body temp as low as possible. The effects of suffering from the heat are cumulative; every day gets worse/harder to deal with. So do whatever you can to stay as cool as possible constantly. Read a book in a cold bath if you must.

## USGOV Best Practices[2]

### Prepare

> -   Learn to recognize the signs of heat illness.
> -   Do not rely on a fan as your primary cooling device. Fans create air flow and a false sense of comfort, but do not reduce body temperature or prevent heat-related illnesses.
> -   Identify places in your community where you can go to get cool such as libraries and shopping malls or contact your local health department to find a cooling center in your area.
> -   Cover windows with drapes or shades.
> -   Weather-strip doors and windows.
> -   Use window reflectors specifically designed to reflect heat back outside.
> -   Add insulation to keep the heat out.
> -   Use a powered attic ventilator, or attic fan, to regulate the heat level of a building’s attic by clearing out hot air.
> -   Install window air conditioners and insulate around them.
> -   If you are unable to afford your cooling costs, weatherization or energy-related home repairs, contact the Low Income Home Energy Assistance Program (LIHEAP) for help.

### During

> -   Never leave people or pets in a closed car on a warm day.
> -   If air conditioning is not available in your home go to a cooling center.
> -   Take cool showers or baths.
> -   Wear loose, lightweight, light-colored clothing.
> -   Use your oven less to help reduce the temperature in your home.
> -   If you’re outside, find shade. Wear a hat wide enough to protect your face.
> -   Drink plenty of fluids to stay hydrated.
> -   Avoid high-energy activities or work outdoors, during midday heat, if possible.
> -   Check on family members, seniors and neighbors.
> -   Watch for heat cramps, heat exhaustion and heat stroke.
> -   Consider pet safety. If they are outside, make sure they have plenty of cool water and access to comfortable shade. Asphalt and dark pavement can be very hot to your pet’s feet.
> -   If using a mask, use one that is made of breathable fabric, such as cotton, instead of polyester. Don’t wear a mask if you feel yourself overheating or have trouble breathing.

## References

1. https://mastodon.art/@welshpixie/108650959789535824
2. https://www.ready.gov/heat
