---
title: Exercise
description: Exercise is bodily activity that is usually used to maintain physical and mental wellness.
---

Exercise is bodily activity that is usually used to maintain physical and mental wellness.

Good advice from antranik[3]:

> If you cannot maintain proper form, [do] an easier variation. **If you feel pain, STOP. Never work through the pain. REST.**

If you can't do the exercise with good form, **you aren't doing the exercise**. Don't cheat, all you're doing is cheating yourself.

## Bodyweight Exercises/Calisthenics Routines

### Burpees

The _most_ minimalist routine is just to do one exercise just short of failure, rest, and repeat for 5-6 times. Some options are the 6-count burpee[17] and the Turkish getup[19].

#### 6-count burpees

1. From standing position, bend your knees and place your hands on the ground
2. Jump slightly from your feet and thrust your legs back, landing in a push-up or high plank position
3. Go down into a push-up, arms at a 45-degree angle to your body
4. Come back up into a high plank position
5. Deweight your legs with your arms and bring your feet back towards your body, like before
6. Stand up

#### Turkish getup

This is done with a kettlebell, but it is recommended to be done with something like a show balanced on your hand first to solidify your form.

1. Starting position (assuming right side is holding the weight)
    1. Laying down on the floor with both shoulders touching the ground.
    2. Left leg is flat on the ground. Right leg is bent with the foot flat on the ground.
    3. Left arm is across your chest with the hand towards the armpit (roughly).
    4. Right arm holding the weight with elbow locked and perpendicular to the floor. This arm should remain in the same position for the whole exercise!
2. Bring right shoulder off the ground, bringing your left elbow into the ground and maintaining your left shoulder contact.
3. Bring your left should off the ground, driving through your left elbow and resting on your forearm.
4. Put your weight into your left hand and drive through, bringing your left elbow off the ground, packing your left shoulder.
5. Pushing off of your left hand, bring your left hip off of the ground. Swing your left foot backwards to rest on your left knee and the tip of your left foot. Both legs should be at 90 degree angles, like a lunge position.
6. Bring your torso upright, ensuring your left leg is directly behind you and your right foot is directly in front of you.
7. Drive through your right foot to stand up fully, ending up with both feet side by side.

Reverse this to go back down.

1. Bring your left leg behind you into a lunge position.
2. Slide your left hand down your leg until you can place your palm on the ground to support you. Your torso should no longer be perpendicular to the ground.
3. Push off your left hand and swing your left leg in front of you, laying it flat on the ground in front of you.
4. Bring your left elbow to the ground.
5. Bring your left shoulder to the ground.
6. Bring your right shoulder to the ground.

### `bodyweightfitness` Minimalist Routine

The minimalist routine from Reddit's bodyweightfitness[2]:

> Complete 2-6 circuits of the following exercises, completing one set of each exercise comprises one circuit. Take little to no rest.
>
> Each set should be 1-2 repetitions short of failure. If you fail your last rep make a note of it and plan to do 1-2 fewer reps than that in future sets of that exercise. If completing a set felt too easy try adding more reps in future sets or, for push-up and rowing sets, progress to a more difficult variation of the exercise.
>
> 1. [Walking Lunges](https://www.youtube.com/watch?v=L8fvypPrzzs)
> 2. [Push-ups](https://old.reddit.com/r/bodyweightfitness/wiki/exercises/pushup)
> 3. [Rows](https://old.reddit.com/r/bodyweightfitness/wiki/exercises/row)
> 4. [Plank Shoulder Taps](https://www.youtube.com/watch?v=LEZq7QZ8ySQ)

### `bodyweightfitness` Recommended Routine

Here is a [PDF cheatsheet](reddit_bodyweightfitness_RR.pdf).

> Do this 3x a week, with at least one rest day (or skill day[x]) in between workout days.
>
> [I]n order to effectively increase or decrease the difficulty, you need to use different variations of a type of exercise [see below].
>
> When you get to the strength training, you will be greeted with progression exercises listed in order of increasing difficulty. Pick an appropriately difficult progression for your current level of strength, and perform 3 sets of 5 reps of that progression on your first session. In subsequent sessions you should try to add one rep per set until you are performing 3 sets of 8 reps with good form. From here you should move on to the next progression, but again at 3 sets of 5 reps. Note that this means that you only perform one of the exercises from each of the listed progressions in each session. Once you move up in the progression, there's no need to keep the easier exercises in your routine (except for using it as a warm-up if you feel like it). [...] Some of the exercises are static holds, such as the support holds or the "tuck front lever" in the rowing progression. Instead of dynamic reps, one set here consists of simply holding the position statically for 10-30 seconds. Move on to the next harder progression once you hit 30 seconds for all 3 sets.
>
> Warm-up: Dynamic Stretches (5-10min)
>
> Strength work (40-60 minutes)
>
> -   First Pair
>     -   3x5-8 Pull-up progression
>     -   3x5-8 Squat Progression
> -   Second Pair
>     -   3x5-8 Dip progression
>     -   3x5-8 Hinge Progression
> -   Third Pair
>     -   3x5-8 Row Progression
>     -   3x5-8 Push-up progression
> -   Core Triplet
>     -   3x8-12 Anti-Extension progression
>     -   3x8-12 Anti-Rotation progression
>     -   3x8-12 Extension progression
>
> These exercises are to be done in pairs and triplets to save time. Pairing two exercises means doing a set of the first exercise, resting 90 seconds, then doing a set of the second exercise, resting 90 seconds, and repeating until you've done 3 sets of that pair.
>
> Rest time: If 90 seconds is not enough, you can rest up to 3 minutes if you like.
>
> Tempo: Ideally, all these exercises are to be done in a "10X0" (1,0,X,O) tempo. If this looks confusing, don't worry. The numbers explain how long each phase should last, and go in the order of: On the way Down/Pause at the Bottom/On the way up/Pause at the top. So 10X0 means 1 'mississippi' second duration on the way down, no pause at the bottom, eXplode up and no pause at the top. When "exploding up", if the actual movement is slow, that's okay, it's the intent that matters.

For visual tips on technique, see this video from FitnessFAQs[14].

## Main Areas

-   Core
-   Chest
-   Back
-   Arms
-   Legs

## Moderating Difficulty

There are many ways to add/relieve difficulty for bodyweight exercises. Here are some of the levers you have:

-   Increasing/decreasing Body angle
-   More/fewer limbs
-   Destabilizing support (support limbs on balls or rings/suspension trainers)
-   Extra weight
-   Increasing/decreasing speed
-   Making the exercise plyometric or static
-   More/less repetitions

## Miscellaneous Exercise Programs and Methods

-   Parkour[7]
-   MovNat[8]
-   LiveAdept[9]
-   Methode Naturelle[10-11]

## References

1. https://en.wikipedia.org/wiki/Exercise
2. https://old.reddit.com/r/bodyweightfitness/wiki/minroutine
3. https://antranik.org/bodyweight-training/
4. https://nchrs.xyz/site/exercise.html
5. https://old.reddit.com/r/bodyweightfitness/wiki/routines/bwf-primer
6. https://www.calisthenics-gear.com/convict-conditioning-review/
7. https://en.wikipedia.org/wiki/Parkour
8. https://yewtu.be/channel/UChgSKQAuBPZIBAfokctA33g
9. https://yewtu.be/channel/UCGaWJABs8KOT4kUHlaNFgMg
10. http://www.alliancemartialarts.com/Georges%20Hebert%20Methode%20Natuelle.htm
11. https://www.movnat.com/the-roots-of-methode-naturelle/
12. [Which ab exercises are best and safe](https://yewtu.be/watch?v=_xdOuqokcm4)
13. ["Skill day"](https://old.reddit.com/r/bodyweightfitness/wiki/kb/skillday)
14. [RECOMMENDED ROUTINE - Reddit Bodyweight Fitness](https://yewtu.be/watch?v=VpobvFPR6hQ&t=7m49s)
15. [ExRx: lots of info on form, muscles used, etc.](https://exrx.net/)
16. Routines
    1. [8-Minute Abs](https://yewtu.be/watch?v=sWjTnBmCHTY), [Arms](https://yewtu.be/watch?v=CxYT5cS4ljA&listen=false), [Buns](https://yewtu.be/watch?v=n538bSON2kA&listen=false), [Legs](https://yewtu.be/watch?v=B0lF0gDzaAc&listen=false), [Stretch](https://yewtu.be/watch?v=79sXwpZUeBw&listen=false)
    2. [Movnat Routines](https://inv.tux.pizza/watch?v=o8KUBRp4xQs)
    3. [Yoga with Adriene](https://www.youtube.com/watch?v=v7AYKMP6rOE) (Beginner)
    4. [Sun salutations](https://inv.tux.pizza/watch?v=73sjOu0g58M)
    5. [Fightmaster Yoga](https://www.youtube.com/watch?v=FRAEaBtP2r4) (All levels)
    6. [A Year Of Burpees](https://old.reddit.com/r/bodyweightfitness/comments/18vdcp8/a_full_year_of_burpees)
17. Burpees: [6-count](https://redirect.invidious.io/watch?v=eroWyZxZNlA) and [10-count](https://redirect.invidious.io/watch?v=BqWQkblauo8)
18. [Welcome to Day 1 of the BWF Primer Community Workout!](https://old.reddit.com/r/bodyweightfitness/comments/kofo8l/bwf_primer_buildup_community_event_day_1_happy/)
19. [How to do the Turkish Getup](https://www.youtube.com/watch?v=2YollP91Wro)
20. {{Hydration drink}}
