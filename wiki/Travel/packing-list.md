---
title: Packing List
description: When I travel, this is what I need to check.
---

## DON'T BRING

-   Keys (you won't need them)

### Carry on

-   {{Safety razor|shaving}} (TSA took mine, even without blades)
-   Any liquid-ish material over 2oz (assume they won't let you take it on unless checked)
-   {{Peanut butter}} (TSA have always taken it)

## Travel Necessities

-   Printed copies of boarding passes, insurance documents, itineraries, visas, and other travel documentation (cover your ass!)
-   N95 Masks
-   Passport/ID
-   Phone and charger
-   Portable battery
-   Power plug adapters and converters (220?)

## Personal Necessities

-   Wallet with international credit card and some bills
-   Medication
-   Bathroom Bag
    -   Toothbrush
    -   Toothpaste (less than 2oz tube)/{{Baking soda}}
    -   Retainers
    -   Deodorant
    -   Travel towel
-   {{Water}} bottle
-   {{Journal(s)|journaling}} and pens
-   Earplugs
-   Buff/Kerchief/Sarong/{{Towel}}[2]
-   6 feet of {{paracord}} rope

### {{Clothing}}

Bring 1/3 to 1/2 less than you think you should bring. You can always wash your clothing in the sink or tub with shampoo and soap, and let it dry in the sun or the hotel room with the fan on. Most hotels also offer laundry service. If you do plan on hand washing, bring clothes that dry super fast, like synthetic or wicking materials, and consider bringing a little detergent.

-   Glasses
-   Sunglasses
-   Hat/Beanie
-   Coat/Sweatshirt
-   Shirts
-   Gloves
-   Shorts/Pants
-   Underwear
-   Socks
-   Shoes/Sandals
-   Sleepwear

## Work

-   Work computer and charger
-   Personal computer and charger
-   Headphones

## Fun

-   Paper and pen/pencil
-   Deck of {{playing cards}}
-   Books/Kindle
-   Portable music maker
-   Headphones
-   Swim trunks and towel
-   Fidget toys ({{komboloi}}, rubiks cube, etc.)

## References

1. [Rick Steves' Packing List](https://www.ricksteves.com/travel-tips/packing-light/ricks-packing-list)
2. [Rick Steves' carry on backpack](https://store.ricksteves.com/shop/p/carry-on-backpack)
3. [Shemagh (Keffiyeh) Scarf: Why I Travel With One](https://expertvagabond.com/shemagh-keffiyeh/)
