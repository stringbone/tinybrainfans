---
title: MacOS
description: MacOS is the operating system for Apple computers.
---

## Spotlight

### `mds` or `mds_stores` using tons of CPU

To stop it, run `sudo mdutil -a -i off`. To restart it again, use `sudo mdutil -a -i on`.

### Reindex

If you have problems where spotlight isn't finding applications or files for some reason, you can initiate the indexing manually via `sudo mdutil -E /`.

### High CPU Usage

1. Restart the computer.
1. Kill spotlight process in activity monitor: `corespotlightd`. It should restart with lower CPU usage.
1. If that didn't work, kill spotlight processes in the terminal: `killall corespotlightd`.
1. Restart again.

## References

1. [Reindexing for Spotlight](https://osxdaily.com/2012/02/02/reindex-spotlight-from-the-command-line/)
2. [mds and mds_stores diagnosing](https://apple.stackexchange.com/questions/144474/mds-and-mds-stores-constantly-consuming-cpu)
