---
title: Webrings
description: Webrings are a way to have adecentralized community of sites with similar topics or goals.
---

## References

1. https://fediring.net/
2. https://webring.dinhe.net/join
3. https://yesterweb.org/webring/
4. https://geekring.net/

