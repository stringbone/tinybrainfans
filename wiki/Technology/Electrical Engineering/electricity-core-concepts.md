---
title: Core Concepts of Electricity
description: These are the basics of electrical engineering; terminology and concepts, mainly.
---

<span style="color: red;">**Electricity can kill you!**</style> Use extreme caution at all times!

Keep antennas and feed lines far away from power lines and utility poles; at least ten feet away if it were to fall down.

## Current

Current (`I`) is the flow of electrons, and is measured in amperes (`A`) (or amps). Materials with ample free electrons are good conductors for electric current.

Current in a single direction is direct current or DC.  Current in alternating directions is alternating current or AC. The following examples show the dotted line as the midline and the solid line as the current.

```
DC:

 1  ───────────────────
 0  - - - - - - - - - -
-1

AC:

 1 ─┐ ┌─┐ ┌─┐ ┌─┐ ┌─┐
 0 -│-│-│-│-│-│-│-│-│-
-1  └─┘ └─┘ └─┘ └─┘ └─
```

Note that the current is not dropping *to* zero but *through* zero to -1 in alternating current.

The frequency of an alternating current is measured in hertz, which is how many oscillations occur within a second. If you are familiar with music, it is similar to a guitar string vibrating; the amount of times the string moves up and down after it is plucked constitutes the note's frequency, for instance the standard tuning note A being 440 hertz.

## Voltage

Voltage (`E`) is the electrical pressure, or difference in electric potential, that causes electrons to flow. It is measured in volts (`V`). The higher the voltage, the more electrical pressure is present. This is also known as the electric potential.

## Resistance

Resistance (`R`) is opposition to electric current (direct and alternating). It is measured in ohms (`Ω`).

Take a typical piece of electrical wiring. We will find two parts composing the wire:

1. The **conductor**, or wire (usually copper), and
2. The **insulator**, or plastic that surrounds the wire.

The conductor has very *low* resistance as to facilitate the transfer of electricity from end to end; this is because they have many free electrons to allow the current. The insulator has very *high* resistance as to protect the electricity from flowing where it shouldn't be flowing. This resistance to the flow of electricity results in the generation of heat.

A **resistor** is a component that provides a *specific amount* of resistance, *opposing* the flow of the current. Where insulators are trying to stop electrical flow completely, resistors are trying to limit it in a controlled way. The symbol in a schematic looks close to, but not exactly, `-\/\/\-`.

A variable resistor, or potentiometer, can change resistance based on the rotation of a knob or a slide switch (like a guitar's volume knob or stereo's EQ slider, respectively).

### Ohm's Law

An ohm is the resistance of a circuit in which 1 ampere current flows when 1 volt is applied: `E = I × R`

Where `E` is voltage in volts, `I` is current in amperes, and `R` is resistance in ohms. Because of this, the following are also true:

`R = E / I`

`I = E / R`

## Capacitance

Capacitance (`C`) is the ability to store energy in an **electric** field, and is measured in farads (`F`).

A capacitor is a component that can store energy and can be used to smooth out voltage over time. This is done by its two opposing plates, one that is being charged (positively or negatively), and a non-conductive region that is between them. Since one side is being charged, this causes the other side to become oppositely charged, creating an electrical field between them. The potency of this electrical field is measured in farads (`F`), with one farad meaning that one "coulomb" of charge on each conductor will cause a voltage of one volt across the device.[3-5]

A variable capacitor allows the capacitance to be changed by moving the conducting plates closer or farther away from each other.

**WARNING!** A capacitor can store power over a long period of time, even after being turned off, and thus must be handled with great care. The stored energy can hurt you; most notably, this can occur in old CRT TV's or computer monitors.

Reading a circuit with an ohmmeter, you can tell if there is a large discharged capacitor by an increasing resistance with time. This is because the capacitor is slowly charging, equalizing its own voltage with that of the ohmmeter's input.

## Inductance

Inductance (`L`) is the ability to store energy in a **magnetic** field. It is measured in henries (`H`).

An inductor is a component that stores enerrgy in an electric field. Commonly used is a coil of wire, or in audio an electromagnet for a speaker. The variation of current makes the speaker move forward and back to create sound waves.

A variable inductor allows changing of inductance by variation of the core placement within a coil.

A transformer contains two inductors with mutual inductance, which means they share the same magnetic field. If both sides of a transformer contain different amounts of loops, then the resulting voltage will change. Most common is *step-down* converters used in wall-warts for converting high voltage wall power (120 volts) into a suitable amount for a small appliance or device (e.g. 9 volts for guitar pedals).

## Reactance and Impedance

Reactance (`X`) is opposition to the flow of alternating current (AC) due to capacitance and inductance. It is measured in ohms (Ω).

Impedance (`Z`) is the combined opposition to the flow of alternating current from resistance and reactance. It is also measured in ohms.

## Energy and Power

Energy is the capacity for doing work, whereas electrical power (`P`) is the rate at which the work is done, or the rate at which energy is used. The energy is the potential that *could* be used, but the power is what *is* being used. Electrical power is rated in watts (`W`).

The power formula is "easy as PIE": `P = I × E`, where `P` is power, `I` is current in amperes, and `E` is voltage. Therefore the following are also true: `I = P / E`, and `E = P / I`.

### Power Supplies

Power supplies and wall warts convert electrical energy from one form to another; usually from alternating current found in the wall into direct current that is similar to what is provided by batteries. They **must** be the correct voltage. Power supplies are also never 100% efficient, so get a power supply that can deliver *at least* enough power if not more.

Oscillators convert a direct current into an alternating current, modifying it to fit the desired frequency of the alternating current.

A filter allows only electrical signals of desired frequencies to pass, and blocks the rest.

### Batteries

Batteries have their power measured in ampere-hours (`Ah`), which is a combination of current and the length of time that current can be sustained until depleted. The length that a battery can be used with a given device can be found by using the following formula with the device's current draw (`cd` in this example):

`length of time = Ah / cd`

## Decibel

Power levels are compared using a unit called the "bel" (`B`), but usually the decibel (`dB`) is used, which is one tenth of a bel. A bel is a factor of 10 increase/decrease in gain.

When power is doubled, there is a 3dB difference (not exactly, but for all intents and purposes). When power is increased by a factor of 10, there is a 10dB difference.

## Analogy: Electricity as Water

It is handy to have a more concrete grasp on electricity and how it flows via the analogy of water. It doesn't always offer an explanation, but it often does.

| Electricity                                                         | Water                                                                             |
|---------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| Electrical current moving through a wire                            | Water moving through a hose                                                       |
| Electrical voltage creating electrical pressure to create a current | A faucet creating water pressure to create a water flow in the hose               |
| Resistance to limit the electrical current                          | A kink in the hose to limit the water flow                                        |
| Capacitance to regulate voltage within a circuit                    | A membrane sealed inside a hose that stretches if pressure is exerted on one side |

## Series versus Parallel

Series is when current passes through a series of different components, whereas parallel is when current passes through multiple branches of different components.

```
Series:

──────/\/\/───/\/\/─┐
                    │
────────────────────┘

Parallel:

     ┌─/\/\/─┐
─────┤       ├──────┐
     └─/\/\/─┘      │
────────────────────┘
```

In a series circuit, the current remains the same between both components, but the voltage may differ, depending on how the components affect it.

In a parallel circuit, the current is divided among the components, depending on the behavior of the components, but the voltage remains the same.

(Note that these are not necessarily to be thought of as inverse rules, as they are not the same thing being measured. In both instances, the current going through both components is the same, but the *amount of current present* will differ from what it initially received before reaching the circuit. The voltages in both instances are **not** the same.)

If the current is the water going through a hose, you can imagine that if it is going through a **series** of kinks in the hose (resistors), the same amount of water will flow through each kink, but the pressure may be different in each one. If it is going through a **parallel** set of kinks, then the amount of water going through each kink will be less, but the pressure will be the same in both kinks.

## Short and Open Circuits

A short circuit is where a connection is made where it shouldn't be made. This can result in excess current, blown fuses, fires, exploding stuff. It is bad! Using a fuse or circuit breaker is a common way to protect from power overload via short circuits.

An open circuit is where a circuit is not complete, and so current cannot flow through.

## Equipment

### Schematic Diagrams

A schematic diagram is a way to show how different components of a circuit are connected and how the current should flow between these components. The map is not the territory, in that the diagram is only a representation and will not look like the actual circuit. There are standard symbols for common components[2].

### Meter

There are different meters for each of the electrical quantities:

* Ammeter for amperes
* Voltmeter for voltage
* Ohmmeter for resistance

And a multimeter can measure all of these on a single device.

**WARNING!** When using these devices, be **sure** that the correct voltage settings are set, or else you may fry your device by allowing too much voltage through, and never measure resistance when the device is powered. If you use the wrong unit of measurement, this can also cause damage, like using the resistance setting when measuring voltage.

#### Ammeter

When connecting an ammeter, ensure it is connected in series so that it will get the actual current. If connected in parallel, you are not guaranteed the actual current, since it is going to two different places and will thus be less.

#### Voltmeter

When connecting a voltmeter, ensure it is connected in parallel so that the voltage measured is what is actually present. If measured in series, the voltage passing through a component can affect the reading on either side, so must go in parallel to ensure the voltage read is actually what is going through the component.

### Switches

A switch is a component that has an intentional open circuit. Starting from the **pole** and moving to the **throw**, when the circuit is completed through the flipping of the switch, the current can flow from the pole to the throw.

The most common switch is a single pole single throw, where the throw can either be an open circuit or a closed one. Another common switch is a single pole double throw, where the current can be routed between two different outputs.

### Fuses and Breakers

A fuse is a device that prevents overloading of a short circuit by burning out and closing the circuit before current can become so much that it would cause further problems. Fuses must use the correct amp rating to ensure that a proper maximum can be maintained and further current will be refused, causing the fuse to fail (correctly). Fuses are meant to be destroyed by the excess electricity and are disposable and meant to be replaced.

A breaker is essentially a fuse that can be reset and reused repeatedly. This is the circuit breaker that most people have in their modern houses. Breakers should be installed **in series** with all **hot** wires, so that it will trip when the current exceeds a specified value.

### Household Wiring

The National Electrical Code standardized these colors for household wiring:

* Hot: black/red/bare
* Neutral: white/gray
* Ground: green/bare

### Amplifier

An amplifier increases the strength of an electronic signal. The amount of increase that is measured is called "gain". This is usually the final stage in your receiver before it goes to your ears or the computer.

Some amplifiers have a "SSB/CW-FM" switch that allows for the amplifier to properly operate in the given mode.  Without this, it may exhibit weird or unwanted behaviors.

## References

1. https://www.hamradiolicenseexam.com/study.jsp
2. https://en.wikipedia.org/wiki/Electronic_symbol
3. https://en.wikipedia.org/wiki/Capacitor
4. https://yewtu.be/watch?v=3L0E9fGI3ag
5. https://yewtu.be/watch?v=jOHRnZsbj40
6. https://www.fcc.gov/wireless/bureau-divisions/mobility-division/amateur-radio-service/examinations

