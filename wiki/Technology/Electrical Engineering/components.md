---
title: Electrical Components
description: Electrical components are the parts that make up modern electric circuits.
---

## Diode

A diode is a component that allows {{current|Core Concepts of Electricity}} to flow in one direction only. The symbol used for a diode is usually like this with a solid triangle: `-->|--`.

Diodes have two parts: an *anode* and a *cathode*. The anode is what precedes the (directional) diode (the triangle in schematics) and the cathode is what succeeds it (the bar in schematics).

When a forward bias is applied (positive on the anode, negative on the cathode), the current will flow. There is a forward voltage drop that occurs, and it varies by diode.

Rectifiers are diodes designed to handle large currents, used often in power supplies to convert AC into varying DC.

## Transistor[1]

A transistor is a component that controls the amount of current that flows through one terminal by current or voltage applied to another terminal. Transistors exhibit gain, so are often used as amplifiers.

### BJT

A common transistor is a bipolar junction transistor. It has three terminals: a base, a collector, and an emitter. The base acts as the input, and when voltage is applied to it, a small amount of current flows through the base-emitter junction (this is usually preceded by a resistor, to protect the transistor from burning out). This allows the emitter-collector current to flow. A transistor, in this case, is effectively a single-pole single-throw switch, allowing current to flow only when voltage is applied at the base.

### FET

A field-effect transistor operates similarly to the BJT, but has differently named terminals and a slightly different way of operating. The terminals used are  gate, drain, and source. Instead of *current* across the *base* to allow the *emitter-collector* current to flow, it uses *voltage* across the *gate* to allow the *source-drain* current to flow.

## Relay

A relay is an electrically operated switch. Early relays were operated by an electromagnet, but are now solid-state with semiconductors.

## References

1. https://yewtu.be/watch?v=YtM_MnM0qT4
