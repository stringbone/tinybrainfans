---
title: iLC Circuits
description: LC circuits are a combination of inductance (L) and capacitance (C).
---

LC circuits are a combination of inductance (L) and capacitance (C). A common instance of an LC circuit is the Varitone[1] knob of a Les Paul guitar.

Every LC circuit has a resonant frequency, which is the frequency at which the inductive {{reactance|Core Concepts of Electricity}} is the same as the capacitive reactance. When at "resonance", an LC circuit can store energy, as the energy oscillates between the magnetic field of the inductor and the capacitor.

**WARNING!** Even in low-power equipment, the energy stored inside an LC circuit can be dangerous!

## References

1. https://en.wikipedia.org/wiki/Guitar_wiring#Capacitors
2. https://yewtu.be/watch?v=0e03xw_GVyE

