---
title: Antennas
description: Antennas are devices meant to transmit and/or receive electromagnetic waves.
---

Antennas convert incoming electromagnetic waves into current, and outgoing current into electromagnetic waves. Antennas don't operate equally in all directions, but reception and transmission often work similarly in a given direction. Antennas all have a resonant frequency, where they can transmit and receive most effectively. Shorter antennas are more resonant at higher frequencies.

## Safety

Antennas have **power** going through them. People should **not** touch them while in transmitting, as doing so could result in an RF burn.

When working on or around antenna towers, take good care! It's easy to hurt yourself. Watch out for storms, turn off all transmitters when working on them, wear proper equipment, and always have a helper nearby.

Your station should be properly grounded to avoid potential voltage spikes and lightning strikes (see {{Ham radio}}).

## Gain

Antenna gain is a measurement of how well the antenna can send or receive in it's forward direction, or direction of maximum radiation. Gain is always given in reference to another antenna, known as the "reference antenna".

## Length For A Given Frequency

The formula for calculating the length of a 1/2-wavelength antenna to be resonant at a given frequency is:

`length (in feet) = 468 / frequency (in MHz)`

The same formula applies for 1/4-wavelength antenna, but by first halving the top number (half the wavelength, half the number).

## Types

### Directional/Beam

There are directional or "beam" antennas that are meant to concentrate energy in a single direction. Like a shotgun microphone, this is meant to isolate the signal from the surrounding noise.

### Dipole[1]

```
<- 1/2 wavelength  ->
──────────┬──────────
antenna ^ │
          │ < feed line
          │
```

A dipole antenna is a center-fed piece of straight wire. It will receive and transmit better on the broad side of the antenna, instead of off the ends.

### Dummy Antenna

A dummy antenna, or dummy load, is a resistor or set of resistors meant to mimic the resistance provided by an antenna. This is used for testing equipment like {{amateur radios|Ham radio}} without actually sending radio frequencies out over an antenna.

An example of a dummy antenna used by hams is a paint can filled with transformer oil, acting as a heat sink.

### Antennas for Handy Talkies

A handy talkie is a handheld {{amateur radio|Ham radio}} operating in the VHF and UHF bands.

These usually operate either with a "rubber duck", a short flexible antenna that usually comes with the talkie, though an extended antenna is often used.

Using a handy talkie in a car often leads to using a magnetic mount antenna, which is a vertical mounted antenna strapped to the roof of the car. This uses the chassis of the car as a ground plate. These often have a 5/8-wavelength pattern, which concentrates the signal at a lower angle that is closer to the Earth's surface.

## Standing Wave Ratio (SWR)[5]

Optimum power transfer between two systems (a source and it's load) requires a match of impedance. A mismatch causes energy to be reflected back down the feed line to the transmitter, causing standing waves of voltage and current. A

This is measured using SWR, a measure of the magnitude of the standing wave on the feed line. This is an indicator of how well the two systems are matched. SWR is measured as a ratio of X:1, where X is greater than 1. A lower value of X is desired (e.g. 1:1), where a higher value means you have an impedance mismatch (e.g. 4:1).

This can be measured using an SWR meter, ensuring that it is properly rated for the frequencies and power levels being used. To measure it, connect it in series with the feed line between the transmitter and antenna.

High SWR may indicate your antenna is not resonant on the transmitting frequency. An erratic SWR may mean you have a loose connection somewhere in your system.

Every feed line has a characteristic impedance. The characteristic impedance does not change significantly based on the length of the line or frequency used. Standard coaxial cables used by {{hams|Ham radio}} commonly have an impedance of {{50Ω|Core concepts of electricity}}.

Every antenna has a specific feed-point impedance, which depends on type and material of the antenna, distance from ground, and other factors.

## Towers[4]

Antenna towers should be properly grounded in accordance to your local codes. The goal is to create a safe path for the power to ground into the Earth and not into you or your equipment. Electricity and thus lightning takes the shortest path of least resistance, so be sure to do the following to ensure your safety:

* Ensure all connections are short and direct
* Avoid sharp bends
* Make connections using steel bolts, not solder, to better withstand the heat of a lightning strike

## Antenna Loading

Antenna loading is the addition of an {{inductor or capacitor|Core Concepts Of Electricity}} to change it's *electrical* length and resonant frequency without changing it's physical length.

A loading coil is an inductor inserted into the antenna to make it *electrically* longer, lowering it's resonant frequency. This is useful in mobile HF antennas.

## Feed Lines

A feed line or transmission line is a wire that connects the radio to the antenna. The two common types are the coaxial cable, a center wire surrounded by shielding and an insulating sleeve, and a twin-lead cable, which is two parallel wires held apart by insulating material.

No feed lines have a perfect transference of energy. There will always be *some* loss from one end to the other, but it can be minimized.

* Twin-lead has less loss but more interference than coax;
* Larger diameter wires have less loss;
* Air-insulated hard line coax has less loss but is more delicate;
* Losses increase on higher frequencies since insulating material has a tendency for AC to flow across it's surface;
* Connections exposed to the elements should be insulated and protected. Coax can break down if exposed to sunlight, for instance;
* Multiple connections can cause degradation of signal.

### Coaxial Connectors

UHF connectors are okay for MF-VHF frequencies, but **not** for UHF or higher. Type N are suitable for all ranges and are often preferred because they offer better moisture resistance.

PL-259 connectors are commonly used at HF and VHF frequencies. RG-213 cable has less loss than RG-58 cable.

A lightning arrester should be installed in a coaxial feed line on a grounded panel near where feed lines enter the building.

## Antenna Analyzers[6]

AN antenna analyzer, also known as a noise bridge or RX bridge, is used to measure impedance and SWR of your antenna system. The analyzer generates a low-power signal and can be used without any significant RF radiation. Because of this, the analyzer plugs directly in where your transmitter would go.

## RF Wattmeters, and Forward and Reflected Power

An RF wattmeter is designed to measure power at RF frequencies. To show this, connect it in the feed line between your transmitter or PA and the antenna.

RF wattmeters are directional, meaning it will show the forward and reflected power. SWR is calculated from the forward and reflected power values.

Reflected power can be turned into power down the feed line, so transmitting with a high SWR can burn out components in your device. Most modern devices have protections for his built in, but your transmitting efficiency will suffer if this problem exists, since most of your transmission energy is just being converted to heat or being limited artificially before getting to the antenna.

## Tuners[7]

An antenna tuner or coupler matches the transmitter's output impedance to the antenna system impedance. The sequence usually goes like this:

`transceiver -> SWR meter -> antenna tuner -> feedline -> antenna`

Tuners don't affect the SWR present between a feed line and an antenna in your system.

A well adjusted antenna tuner will re-reflect the power that is being reflected back down your feed line back up to the antenna, but in phase and reinforcing the forward power of the signal. This should get your SWR meter pretty close to 1:1, since almost all power is getting radiated out from the antenna. This helps also with lossy feed lines.

> An antenna tuner allows you to operate on a non-resonant antenna with a high SWR.  It's not as efficient as a resonant antenna and low SWR, primarily due to feed line losses, but it does work.[8]

## References

1. https://en.wikipedia.org/wiki/Dipole_antenna
2. https://en.wikipedia.org/wiki/Dummy_antenna
3. https://en.wikipedia.org/wiki/Loading_coil
4. http://astrosurf.com/luxorion/qsl-lightning-protection4.htm
5. http://en.wikipedia.org/wiki/Standing_wave_ratio
6. https://en.wikipedia.org/wiki/Noise_bridge
7. https://en.wikipedia.org/wiki/Antenna_tuner
8. https://www.hamradiolicenseexam.com/

