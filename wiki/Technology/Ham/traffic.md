---
title: Traffic
description: Handling traffic in amateur radio means relaying messages from station to station.
---

Handling traffic in amateur radio means relaying messages from station to station. Before cheap phone calls, this was a way to send messages from one person to another over very long distances. The {{American Radio Relay League}} considers this a large part of the ham radio tradition. This skill is still useful for emergency communications.

These relayed messages included a preamble, or the information needed to track the message, and a check, or a count of the number of words or word equivalents in the text portion of the message.

## References

1. [A radiogram form](http://www.arrl.org/files/file/Public%20Service/RADIOGRAM-2011.pdf)
2. http://www.arrl.org/files/file/Public%20Service/MPG104A.pdf
3. [National Traffic System](http://www.arrl.org/nts)

