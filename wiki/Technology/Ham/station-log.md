---
title: Station Log
description: A station log is used to record all HF contacts, and can end up being useful.
---

Many hams keep a log of all HF contacts, typically including:

* Date and time, usually in the “Zulu” timezone (UTC)
* Frequency
* Other operator's call sign
* Signal reports exchanged
* Notes

> When you allow someone else to operate your station, either as a control operator or third party, it's a good idea to note that fact in your station log.  If the FCC notifies you of a violation, at least you'll have documentation to show what was happening that day.
>
> The log comes in especially handy if you need to resolve an interference complaint or an information request from the FCC.  If you can pull out your logbook and show that you were not operating at the time in question, that usually clears up the issue.  On the other hand, if you were operating at the time, it lets you know that there may be a real problem to be dealt with.[1]

## References

1. https://www.hamradiolicenseexam.com/
2. http://www.arrl.org/logbook-of-the-world

