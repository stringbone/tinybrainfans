---
title: Web3 and Crypto
description: In it's current state, cryptocurrency is a Ponzi scheme meant to take advantage of those less fortunate, and web3 isn't actually decentralized.
---

Web3 Is Going Great[13]: An ongoing list of all the fucked up things happening with Web3, including the producers, the buyers, the victims and the perpetrators.

## Web3

> Most product ideas for web3 have preexisting counterparts that already work pretty well. [...] It’s not just that some of the analog, or web2, iterations of products work as well as, or better than, their web3 counterparts in some abstract or theoretical sense. It’s that real people *prefer* them as well.

-- Web3? I have my DAOts[2]

> The problem here is the profit motive: people who are working on web3 generally want to get paid for it, but it's fundamentally harder to extract rent from truly decentralized systems than it is from centralized ones. Because of that, people end up building systems that are centralized at their core, with some aesthetics of decentralization smeared on top, and call it web3.

-- web3 is Centralized[3]

> It’s probably good to have some clarity on why centralized platforms emerged to begin with, and in my mind the explanation is pretty simple:
>
> 1. It’s probably good to have some clarity on why centralized platforms emerged to begin with, and in my mind the explanation is pretty simple:
> 2. A protocol moves much more slowly than a platform.
>
> [...]
>
> “It’s early days still” is the most common refrain I see from people in the web3 space when discussing matters like these. In some ways, cryptocurrency’s failure to scale beyond relatively nascent engineering is what makes it possible to consider the days “early,” since objectively it has already been a decade or more.
>
> However, even if this is just the beginning (and it very well might be!), I’m not sure we should consider that any consolation. I think the opposite might be true; it seems like we should take notice that *from the very beginning*, these technologies immediately tended towards centralization through platforms in order for them to be realized, that this has ~zero negatively felt effect on the velocity of the ecosystem, and that most participants don’t even know or care it’s happening.

-- My first impressions of web3[6]

## Crypto

### It's a Ponzi Scheme

A more or less definitive video on NFTs and why they are so problematic: Line Goes Up – The Problem With NFTs[7]

> “This is a recurring pattern in the crypto economy,” says Lee Reiners, Policy Director at the Duke Financial Economics Center, who teaches cryptocurrency law at Duke Law. “This thing was set up to enrich the founders and early supporters at the expense of everyday people who bought hotspots thinking they were going to bring value."[18]

> Bitcoin and NFTs have been packaged as helping “the little guy” get ahead. This is called “class collaboration” and weaponizes working people on behalf of the ultra-rich. Mussolini did this too, under the name “class collaboration.”
>
> And in fact, most Bitcoin is controlled by just a few people; it’s not “democratized” in any way. Just 0.01% hold 27%. It’s just one more shot at the old “sound money” fever dream, floated on the backs of average people as accomplices.
>

-- Dave Troy on Twitter[4]

> Two executives at crypto trading platform Celsius cashed out at least $17 million and as much as $23 million in cryptocurrency shortly before the company halted withdrawals for all users earlier this year, according to a financial disclosure form filed in New York bankruptcy court late Wednesday.

-- Gizmodo[x]

> CryptoSlam says the vast majority of the hot NFT marketplace’s sales are illegitimate trades made to manipulate the token rewards model.

-- LooksRare Has Reportedly Generated $8B in Ethereum NFT Wash Trading[11]

> [I]t's very unfortunate and disappointing that Khan Academy allowed these for-profit enterprises to use KA's reputation to launder their own reputations, boost their brand on CNBC (and to Khan Academy's own userbase), and make this washtrading happen.
>
> When you consider Khan’s audience and user base, which is primarily kids and under-resourced people, promoting a loot crate NFT card game (again, a volatile and unregulated space!) is downright irresponsible and I don’t understand how this made it through the giving guidelines.

-- KhanAcademy taking donations from an NFT project[12]

### It's Not Decentralized

> It's worth noting that literally all of the things that I pointed out in my cryptocurrency threat model video[14] have now happened: actors immediately fell back on the government-run legal system as soon as problems arose. I am still waiting for someone to earnestly debate this.

-- Casey Muratori[15]

### Culture

> "Within our present oligarchic, exploitative, irrational, and inhuman world system, the rise of crypto applications will only make our society more oligarchic, more exploitative, more irrational, and more inhuman." - Yanis Varoufakis

-- Yanis Varoufakis on Crypto & the Left, and Techno-Feudalism[10]

> Anyone blindly promoting cryptocurrencies without understanding the full background of “sound money,” gold, race science, and white supremacy is unwittingly advancing an agenda they don’t comprehend. Pretending it isn’t there won’t make it go away.

-- Dave Troy on Twitter[4]

> The women on this boat are polished and perfect; the men, by contrast, seem strangely cured—not like medicine, but like meat. They are almost all white, between the ages of 30 and 50, and are trying very hard to have the good time they paid thousands for, while remaining professional in a scene where many thought leaders have murky pasts, a tendency to talk like YouTube conspiracy preachers, and/or the habit of appearing in magazines naked and covered in strawberries. That last is 73-year-old John McAfee, who got rich with the anti-virus software McAfee Security before jumping into cryptocurrencies. He is the man most of the acolytes here are keenest to get their picture taken with and is constantly surrounded by private security who do their best to aesthetically out-thug every Armani-suited Russian skinhead on deck. Occasionally he commandeers the grand piano in the guest lounge, and the young live-streamers clamor for the best shot. John McAfee has never been convicted of rape and murder, but—crucially—not in the same way that you or I have never been convicted of rape or murder. I do not interview John McAfee. He interests me less than he scares the shit out of me, though his entourage seems relaxed. They’re already living in the crypto-utopia behind his strange pale-blue eyes.
>
> [...]
>
> On this half-empty passenger ship with its swirling ’80s carpets right out of *The Shining*, there is very little sober talk of blockchain’s obstacles or limitations. Nobody mentions how wildly ecologically unsound the whole project is—some estimates have bitcoin burning as much energy as the entire nation of Ireland for a relatively small pool of users. Instead, the core and only existential question is which of the various coins and ICOs (initial coin offerings) will make you the richest the fastest before dawn.

-- Four Days Trapped at Sea With Crypto’s Nouveau Riche[8]

> The more you think about it, the more “it’s early days!” begins to sound like the desperate protestations of people with too much money sunk into a pyramid scheme, hoping they can bag a few more suckers and get out with their cash before the whole thing comes crashing down.

-- It's not still the early days[5]

### It Doesn't Solve Any Real or Yet Unsolved Problems

> 1. The technology does not solve a real problem.
> 2. So called “cryptocurrencies” aren’t actually currencies, and cannot fulfil the function of money.
> 3. The history of private money is one of repeated disasters that destroy public trust.
> 4. Crypto assets are all unregistered securities.

-- "The Case Against Crypto"[1]

> But if you want to create an Non-Fungible Token - there's no reason to pay inflated "gas" fees. You don't need any centralised brokers or platforms. You can own your own ledger and be completely decentralised for free.
>
> Web3 should be about *you* being in control - not endlessly paying micro-transactions to hundreds of gatekeepers.

-- An NFT without a Blockchain. No gas fees. No Eth. No gatekeepers[9]

> As a result of crypto being a failure in its original purpose of payments, the myth-making around it has since adapted to find a new story. Now, most people pitch their crypto tokens as an investment, a financial asset to be added to your portfolio alongside your stocks and bonds. However, as a financial asset, it is extremely difficult to analyze because the value of a crypto token is not tied to any underlying economic activity or physical thing. Neither is the dollar, but the dollar is not a financial asset or investment, the dollar is a *monetary object* which is an entirely different class of financial instrument whose efficacy is measured in how it fulfills the three properties of money. Almost all sophistry in the promotion of crypto assets is this superposition of these tokens as simultaneously both an investment and a monetary object when such a claim is logically impossible as both categories have diametrically opposite financial properties. This intellectual bait and switch of crypto is where the properties of opposite financial products are used interchangeably without regard for the coherence of the argument.

-- The Philosophical Argument Against Crypto[17]

## References

1. https://www.stephendiehl.com/blog/against-crypto.html
2. https://networked.substack.com/p/web3-i-have-my-daots
3. https://blog.wesleyac.com/posts/web3-centralized
4. https://twitter.com/davetroy/status/1478017698676228099
5. https://blog.mollywhite.net/its-not-still-the-early-days/
6. https://moxie.org/2022/01/07/web3-first-impressions.html
7. https://www.youtube.com/watch?v=YQ_xWvX1n9g
8. https://web.archive.org/web/20181229090358/https://breakermag.com/trapped-at-sea-with-cryptos-nouveau-riche/
9. https://shkspr.mobi/blog/2021/12/an-nft-without-a-blockchain-no-gas-fees-no-eth/
10. https://the-crypto-syllabus.com/yanis-varoufakis-on-techno-feudalism/
11. https://decrypt.co/91510/looksrare-has-reportedly-generated-8b-ethereum-nft-wash-trading
12. https://twitter.com/smlundberg/status/1486447254420566020
13. https://web3isgoinggreat.com/
14. https://yewtu.be/watch?v=kbYutOsrpvs
15. https://twitter.com/cmuratori/status/1529185876986646528
16. https://www.stephendiehl.com/blog/philosophy.html
17. https://www.stephendiehl.com/blog/philosophy.html
18. https://www.forbes.com/sites/sarahemerson/2022/09/23/helium-crypto-tokens-peoples-network/
19. https://gizmodo.com/celsius-execs-cashed-out-bitcoin-price-crypto-ponzi-1849623526
