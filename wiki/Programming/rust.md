---
title: Rust
description: Rust is a compiled low-level language. Aiming for similar applications as C, but safer.
---

Rust prioritizes precision. Lots of types which can be annoying, but makes for a totally clean and predictable code path, returns, behavior, etc.

## Raw notes

<!-- 20240205 -->

`cargo` is the package manager, I think. But you can also run `cargo run` to compile and run your thing so :shrug:.

### Example

```rust
use actix_web::{get, web, App, HttpServer, Responder};

#[get("/")]
async fn index() -> impl Responder {
    "Hello, world!"
}

#[get("/{name}")]
async fn hello(name: web::Path<String>) -> impl Responder {
    format!("Hello {}!", &name)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().service(index).service(hello))
        .bind(("127.0.0.1", 8080))?
        .run()
        .await
}
```

`impl Responder` is the "trait" or "interface" that is being returned. Not a type necessarily. Can be replaced with `String`, which is true, but isn't _accurate_.

`String` is not `str`.

## References

1. [Rust - Get Started](https://www.rust-lang.org/learn/get-started)
2. [Rust with Visual Studio Code](https://code.visualstudio.com/docs/languages/rust)
3. https://fasterthanli.me/articles/the-curse-of-strong-typing
4. https://github.com/rust-lang/rustlings
