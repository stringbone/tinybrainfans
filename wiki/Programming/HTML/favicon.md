---
title: Favicons
description:
---

To make the failed GET request error go away, you can put this in your `<head>`:

```html
<link rel="icon" href="data:," />
```

## References

1. https://gitlab.com/joshavanier/mortem/-/blob/master/index.html

