---
title: Animation (uxn)
description:
---

## Vector

The key to animation is to utilize the screen device's `vector`. 60 times per second, whatever address is found at the vector will be jumped to and ran, similar to an interrupt or IRQ found in the {{Minicube64}}. The following will put the absolute address of the `on-screen` label into the `vector`.

```uxntal
|00 @System  [ &vector $2 &pad $6  &r $2 &g $2 &b $2 ]
|20 @Screen  [ &vector $2 &width $2 &height $2 &pad $2 &x $2 &y $2 &addr $2 &pixel $1 &sprite $1 ]

;on-frame .Screen/vector DEO2

@on-frame
  #01 .Screen/pixel DEO
```

## References

1. https://compudanzas.itch.io/introduction-to-uxn-programming
2. {{uxn}}

