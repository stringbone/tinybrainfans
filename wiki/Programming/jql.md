---
title: JQL
description: Jira Query Language is baffling, but helpful.
---

JQL is {{Jira}}'s {{SQL}} language. It is somewhat baffling as a noob, so this will mostly be a list of resources and common queries I use.

## Queries

### See all issues touched/updated by a user[3]

> An update in this case includes creating an issue, updating any of the issue's fields, creating or deleting a comment, or editing a comment (only the last edit).[4]

```jql
issueKey IN updatedBy("username")
```

### Get all related tasks and subtasks of an issue or epic[5]

>

```jql
parent = EPIC-123 OR key in linkedIssues("EPIC-123")
```

## References

1. [JQL Cheat Sheet](https://atlassianblog.wpengine.com/wp-content/uploads/2017/01/atlassian_jql-cheat-sheet.pdf)
2. [JQL: Get Started with Advanced Search in Jira | Atlassian](https://www.atlassian.com/software/jira/guides/jql/overview#what-is-jql)
3. [How to find issues which are updated by a specific user using JQL?](https://community.atlassian.com/t5/Jira-questions/How-to-find-issues-which-are-updated-by-a-specific-user-using/qaq-p/933031#M373180)
4. [updatedBy(): Advanced searching - functions reference | Jira Software Data Center and Server 8.1 | Atlassian Documentation](https://confluence.atlassian.com/jirasoftwareserver081/advanced-searching-functions-reference-970611548.html#Advancedsearchingfunctionsreference-approverupdatedBy%28%29)
5. [Get all subtasks and linked issues to my issue](https://community.atlassian.com/t5/Jira-Software-questions/JQL-query-for-filtering-all-subtasks-of-an-issue/qaq-p/1199881)
