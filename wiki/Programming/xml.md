---
title: XML
description: XML is a verbose but human readable data format.
---

XML is a verbose but human readable data format.

## {{RSS}}

RSS is an XML document that is made to show a list of items that a user could subscribe to. This is what {{podcasts|podcast}} and {{blogs|blog}} are built with, and many services like Youtube and Reddit still use these. It's a really effective way to deliver content to your readers in a simple and effective way.

The below script is the most minimal possible RSS feed I made that passes the RSS validator[1].

```xml
<?xml version="1.0"?>
<rss version="2.0">
  <channel>
    <title>Title</title>
    <description>Description</description>
    <link>http://example.com</link>
    <item>
      <title>Title</title>
      <guid>http://example.com</guid>
      <pubDate>Sun, 23 Feb 2025 00:00:00 +0000</pubDate>
    </item>
  </channel>
</rss>
```

## References

1. [W3C RSS Feed Validator](https://validator.w3.org/feed/check.cgi)
