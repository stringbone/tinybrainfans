---
title: Calculate changes
description: At work, you need to calculate impact. Lines of code sucks, but it's better than nothing/
---

To calculate total changes, you can essentially go through all branches named XYZ, get the changes, and add them together. What you need is

-   Branch prefix
-   Base branch name
-   Files to exclude if any

```bash
git for-each-ref --format="%(refname:short)" refs/heads | \
  grep '$branch_prefix' | `# only get branches from ticket XYZ` \
  while read -r line; do
    git log \
      --numstat --pretty=format:"" `# get addition, deletion, filename` \
      "$(git merge-base $line $base_branch_name)..$line" `# from base to tip` \
      -- . ':(exclude)excluded/dir/*' `# excluded directories/files` | \
  done | \
  awk '(files = files + 1) (add = add + $1) (del = del + $2) { } END { print files " " add " " del }' `# add all data`
```

## References

1. https://www.baeldung.com/linux/add-column-of-numbers
2. https://stackoverflow.com/questions/46195495/how-to-skip-specific-files-while-calculating-lines-of-code
3. https://stackoverflow.com/questions/1549146/git-find-the-most-recent-common-ancestor-of-two-branches
4. https://git-scm.com/docs/git-for-each-ref
5. https://git-scm.com/docs/pretty-formats
6. https://stackoverflow.com/questions/1265040/how-to-count-total-lines-changed-by-a-specific-author-in-a-git-repository
7. https://stackoverflow.com/questions/5685007/making-git-log-ignore-changes-for-certain-paths
8. https://stackoverflow.com/questions/2528111/how-can-i-calculate-the-number-of-lines-changed-between-two-commits-in-git
