---
title: Programming Games
description: These are games about or to help you learn programming.
---

There are lots of games that have recently come out to make {{assembly}} a little more accessible, as well as programming in general. Here are the references I have found and heard about from others:

1. [Shenzhen IO](https://store.steampowered.com/app/504210/SHENZHEN_IO/)
2. [TIS-100](https://store.steampowered.com/app/370360/TIS100/)
3. [Human Resource Machine](https://store.steampowered.com/app/375820/Human_Resource_Machine/)
4. [7 Billion Humans](https://store.steampowered.com/app/792100/7_Billion_Humans/)
5. [Bots Are Stupid](https://lelegolla.itch.io/bots-are-stupid)

