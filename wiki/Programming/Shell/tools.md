---
title: Shell Tools
description: Interesting and useful third-party tools for the shell/command line.
---

## References

1. [Tool to run scripts on cue (file changes, etc.)](https://eradman.com/entrproject/)
2. [Testing](https://poisel.info/posts/2022-05-10-shell-unit-tests/)

