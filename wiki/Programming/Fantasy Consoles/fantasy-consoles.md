---
title: Fantasy Consoles
description: A fantasy console simulates virtual hardware, unlike emulators which emulate real hardware.
---

A fantasy console simulates virtual hardware, unlike emulators which emulate real hardware.

## References

1. https://paladin-t.github.io/fantasy/
2. {{PICO-8}}
3. {{Minicube64}}
4. {{TIC-80}}

