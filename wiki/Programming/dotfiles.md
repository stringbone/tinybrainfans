---
title: Dotfiles
description: Dotfiles are the config files for your computer that can help transitioning to a new device easier.
---

## Making a Dotfile Repo

This method will allow you to sync dotfiles on different computers with a small amount of effort. You still have to pull down new changes as they occur on different machines, which sucks, but it is what it is. I'm sure there is a way I could set up git hooks to automate a check for if the local is out of date before syncing, but haven't done that yet.

1. Create a new folder to keep your dotfiles (I use `$HOME/.dotfiles`).
1. Initialize a git repository there (`git init`).
1. For each dotfile/folder you want to track/sync:
    1. Move it to your repo.
    1. Make a {{symlink|symlinks}} to what you moved into the repo (`ln -s /path/to/moved/item`).
    1. Test that the original path leads to the moved item (e.g. if syncing the `vimrc` file: `cat ~/.vimrc`).
1. Push the repo to the remote.

## Different Settings on Different Computers

### Shell RC Files

You can break out different files for different machines. If the file doesn't exist, it will skip sourcing that file. 

For instance, treating your {{shell's|shell}} rc file (e.g. `{{zshrc|zsh}}`) as a global aliases file, we can `source` other files that only pertain to our machines, like so:

**.zshrc**

```bash
#!/bin/zsh

source "$HOME/.home_zshrc"
source "$HOME/.work_zshrc"
```

### {{Vim}} and Other Configs

In your shell's rc files, you can also set global environment variables that these external configs can use. For instance if you set an environment variable like `COMP` to the name of whatever machine you are using, then other programs can read that and conditionally make choices based on that value.

For instance, to do this in your `.home_zshrv`, use `export COMP="home"`. Then in {{Vim}}, you could use a check to only set certain configurations, like tabs/spaces, etc.

```vim
if $COMP == 'home'
    " do a thing
endif
```

## References

1. https://dotfiles.github.io/
2. https://evanhahn.com/a-decade-of-dotfiles/
3. {{Crontab}}

