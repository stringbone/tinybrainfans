---
title: Vim
description: Vim is a command-line editor that is included with most *nix machines and OSX.
---

Vim is a command-line editor that is included with most *nix machines and {{OSX|MacOS}}.

## VimTutor

Use it! It guides you through the {{basic commands|Vim Commands}} of Vim by *doing things* instead of reading random wikis on the web and trying to cobble together how to do stuff. You can access it by typing `vimtutor` at the CLI.

## References

1. https://www.vim.org/
2. https://danielmiessler.com/study/vim/
3. https://elijahmanor.com/blog/neovim-tmux
4. http://texteditors.org/cgi-bin/wiki.pl?VIM
5. https://www.vimgolf.com/
6. https://secluded.site/vim-as-a-markdown-editor/
7. https://shapeshed.com/vim-netrw/
8. https://github.com/tpope/vim-vinegar
9. https://andrew.stwrt.ca/posts/vim-ctags/
10. https://tbaggery.com/2011/08/08/effortless-ctags-with-git.html
11. https://joshrendek.com/2011/11/osx-vim-ctags-exuberant-for-fast-context-switching-in-large-projects/
12. https://vimtricks.com/p/word-wrapping/

