---
title: Minimum Viable Product
description: The idea of an MVP is to get something up that meets the base specs as fast as possible.
---

The idea of a Minimum Viable Product is that you want to meet the most basic specs for your product as quickly as possible, leaving out any and all unnecessary or unimportant details. The benefits of this are a quicker test of the viability of the product, a better implementation of the core features without any extra overhead, and freedom to change your ideas quickly and without having to manage extra dependencies or features.

> “what is the skateboard for this idea?” ... it immediately reframes the discussion around finding the heart of a product request. It helps teams find the simpler version of any idea to start with, if it exists.[4]

## Example

Let's say you wanted to build a blog. There are lots of features you might consider doing for this:

* Posting content
* Editing existing content
* Deleting existing content
* Having that content visible to the public
* A WYSIWYG editor for the author
* Good CSS styling
* Highly accessible functionality
* An RSS Feed
* Authorization to disallow non-authors from posting
* Login page
* 404 pages

Etc. To get a project like this up and running, we want to figure out which of these on this list are the most important for a **functioning** blog. Not the best blog, not a great blog, but one that does exactly what is expected of a blog and *nothing more*. Let's determine what those features are:

* Posting content
* Editing existing content
* Deleting existing content
* Having that content visible to the public

Essentially, we are paring this down to the most rudimentary CRUD app, making our development process much more lean and nimble.

## Tools

While working on your MVP, some choices may make your process more enjoyable or simple than others. For instance, spinning up a whole MongoDB or MySQL instance for a small project may be unnecessary for a quick implementation. Or you just don't want to look at default user styles on the browser the whole time. These tools, mainly for {{web development}}, will make life just a little better and maybe implementation a little quicker.

* {{Classless CSS}} - Simple CSS frameworks that work with HTML elements without adding class-specific styling.
* {{Progressive enhancement}}
* ldb[1] - A basic one table database for {{Javascript}} using local storage.
* Erlang ticket system - What I can only imagine is a tiny shell script to make a new ticket with a distributed version control system attached.

## References

1. https://git.sr.ht/~eli_oat/ldb
2. https://joearms.github.io/published/2014-06-25-minimal-viable-program.html
3. https://www.bedelstein.com/post/mcmaster-carr
4. [The Skateboard Mindset in Product Development](https://scribe.rip/@byrnereese/the-skateboard-mindset-in-product-development-ddf3409d5e98)

