---
title: Forth
description: Forth is a procedural, stack-oriented programming language and interactive environment.
---

Forth is a procedural, {{stack}}-oriented programming language and interactive environment.

I'm using gFORTH installed with `brew install gforth` on OSX to learn.

## Words

Words are things that the interpreter can understand and turn into computer actions. This includes things like numbers, operators (`+`, `-`, etc.), and other basic words, like `EMIT` and `SPACES`, which draw characters or spaces to the screen.

Most Forths start out with a set of words, which you can find by using the word `WORDS`. The word `WORDS` is interpreted as: `print all words that are understood by the interpreter`. A word can be anything, save for a few reserved words.

You can define your own words, as well, using the words `:` and `;`, which denote the beginning and end of a word definition, respectively. If you wanted to make a word that printed 30 spaces without having to write `30 SPACES` over and over, you could write:

```forth
: MARGIN   30 SPACES ;
```

(Note: extra space is ignored). Now if we write `MARGIN`, the interpreter will first interpret that as `30 SPACES`, and then execute what the interpreter finds when looking up those two words.

Forth definitions are usually added as a comment after the new word and before the other words it contains. This comment starts with what arguments it will consume off the stack, followed by hyphens and what it will put on the stack when it is finished. For the `MARGIN` word above, we would make something like the following, since it consumes nothing and creates nothing at end.

```forth
: MARGIN ( -- ) 30 SPACES ;
```

Another word, `+`, consumes two numbers off the stack and pushes the sum back onto the stack, so it would be written differently than `MARGIN`, like so: `( num1 num2 -- sum )`.

## {{Stack}}

Forth uses a stack, which you can think of like a space to put playing cards holding specific values. As you put more cards on, the previous cards get deeper in the stack. When you take things off the stack, you take them off in the reverse order you put them on, with the most recent cards coming off first. This is known as first in, last out (FILO).

### Stack Manipulation[11]

These are the most common operators and many of these have variations, like `2DUP`, or `-ROT`.

| Word   | Definition                                                                                    |
| ------ | --------------------------------------------------------------------------------------------- |
| `DROP` | `( w – )`                                                                                     |
| `NIP`  | `( w1 w2 – w2 )`                                                                              |
| `DUP`  | `( w – w w )`                                                                                 |
| `OVER` | `( w1 w2 – w1 w2 w1 )`                                                                        |
| `TUCK` | `( w1 w2 – w2 w1 w2 )`                                                                        |
| `SWAP` | `( w1 w2 – w2 w1 )`                                                                           |
| `PICK` | `( S:... u – S:... w )` (e.g. `n PICK` will duplicate the nth item from the top of the stack) |
| `ROT`  | `( w1 w2 w3 – w2 w3 w1 )`                                                                     |

## Cheatsheet

| Word              | Definition                   | Description                                       |
| ----------------- | ---------------------------- | ------------------------------------------------- |
| `variable [name]` |                              | Create global variable `[name]`.                  |
| `!`               | `( value addr -- )`          | Store value at `addr` to `value`                  |
| `@`               | `( addr -- value )`          | Load `value` from `addr`                          |
| `dump`            | `( addr u -- )`              | Show `u` lines of memory at `addr`                |
| `cells`           | `( c -- b )`                 | Get how many bytes `b` that `c` cells occupies    |
| `.`               | `( x -- )`                   | Pop element off the stack and print to console    |
| `.S`              | `( x1 ... xn -- x1 ... xn )` | Print the contents of the stack non-destructively |

## References

1. https://www.forth.com/starting-forth/
2. https://hackaday.com/2017/01/27/forth-the-hackers-language/
3. http://forth.org/compilers.html
4. https://wiki.xxiivv.com/site/forth.html
5. https://www.complang.tuwien.ac.at/forth/gforth/Docs-html/Files-Tutorial.html
6. https://www.forth.com/wp-content/uploads/2018/01/Starting-FORTH.pdf
7. http://www.forth.org/OffeteStore/1003_InsideF83.pdf
8. Bootloader of JonesFORTH: [Writeup](https://rwmj.wordpress.com/2010/08/07/jonesforth-git-repository/), [Code](https://github.com/nornagon/jonesforth/blob/master/jonesforth.S)
9. https://gitlab.cs.washington.edu/fidelp/frustration
10. [Forth Primitives](https://users.ece.cmu.edu/~koopman/stack_computers/appb.html)
11. [Stack Manipulation](https://www.forth.com/starting-forth/2-stack-manipulation-operators-arithmetic/#Stack_Maneuvers)
12. [Beginner's Guide to Forth](https://galileo.phys.virginia.edu/classes/551.jvn.fall01/primer.htm)
13. [Forth Tutorial from the gforth manual](https://www.complang.tuwien.ac.at/forth/gforth/Docs-html/Tutorial.html#Tutorial)
14. [Build your own forth](http://www.figuk.plus.com/byof.htm)
