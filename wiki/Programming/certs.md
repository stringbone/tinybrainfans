---
title: Certificates/PEM Files
description: I get out of date errors a lot, so this is a fix.
---

Sometimes when trying to clone a {{git}} repo off the internet, I run into this error: `SSL certificate problem: certificate has expired`. On {{old versions of Mac OSX|MacOS}}, your system's {{SSL}} cert may not update, so you should update it manually[1].

> OpenSSL on macOS does not use the system keychain (which makes sense as it's a cross platform library) but rather has its own .pem file containing its root certificates. Even though my systems have a newer version of OpenSSL installed using homebrew and/or MacPorts, the system-wide OpenSSL pem file located at /etc/ssl/cert.pem was out of date and did not include the ISRG Root X1 certificate.
>
> The solution:
>
> * Rename /etc/ssl/cert.pem to something else. (I suggest /etc/ssl/cert.pem.org)
> * Download the latest cacert.pem from https://curl.se/docs/caextract.html
> * Rename it to cert.pem
> * Copy it to /etc/ssl/cert.pem
>
> Now curl and any other app using OpenSSL can access websites signed using current Let's Encrypt certificates.
>
> Alternatively, the MacPorts package curl-ca-bundle installs a .pem file containing ISRG Root X1 to /opt/local/etc/openssl/cert.pem which can be used as well.
>
> Other possible solutions:
>
> * Manually add the ISRG Root X1 certificate to /etc/ssl/cert.pem
> * Configure OpenSSL to use a different .pem file for its root certificates, such as /opt/local/etc/openssl/cert.pem

## References

1. https://stackoverflow.com/questions/69521959/ssl-certificate-problem-certificate-has-expire-in-macos

