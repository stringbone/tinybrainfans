---
title: sort
description: "Unix's `sort` command is super powerful and I use it often."
---

`sort` sorts the input lines by a given item within each line.

## Basic sorting

`sort {file}` or `echo "test" | sort`

| Option | Args    | Description                                                                                                   |
| ------ | ------- | ------------------------------------------------------------------------------------------------------------- |
| `-k`   | `n[,m]` | Sort by field number `n`, and any further provided field numbers (e.g. `m`) if `n`'s are equal.               |
| `-n`   |         | Sort numerically. e.g. `10` will come after `1`.                                                              |
| `-h`   |         | Sort by numerical value, but take into account the SI suffix, if present. e.g. `10K` will come before `2M`.   |
| `-r`   |         | Reverse the input.                                                                                            |
| `-t`   | `c`     | Set the delimiter `c` for each field. e.g. a `,` (comma) for a {{CSV}} file. Default is any amount of spaces. |

## References

1. https://unix.stackexchange.com/questions/169200/sort-command-in-unix-with-numeric-sort
