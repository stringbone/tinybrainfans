---
title: fc
description: fc allows you to open a temporary file and execute what's written in the shell.
---

fc allows you to open a temporary file in your default editor and execute what's written in the {{shell}}.

## Usage

Running `fc` by itself will load up your editor with the last executed command. On exit, it will execute the contents within that shell.

Using the `-l [start] [end]` option lists the history of recent commands executed from start to end. Without any start or end parameters, it will list the last 15 commands executed.

## References

1. https://www.unix.com/man-page/linux/1/fc/

