---
title: Storybook
description: Storybook is a framework to manually test isolated UI components.
---

Storybook is a framework to manually test isolated UI components, and is available for use with {{React}}, {{Vue}}, and other {{frameworks|Frameworks (Javascript)}}. The advantages of using Storybook versus something else like a live website or CodeSandbox examples are that you can isolate or combine components in meaningful and specific ways, and also that the parameters being passed into these components can be changed dynamically for easy testing.

## Getting Started

*This is assuming you have an existing {{node}} project and are using some kind of {{yarn}} or {{npm}} program to manage your dependencies.*

Go into your project's root folder and run `npx storybook init` and follow the instructions. Through this, it adds a script in your `package.json`, so from there, you should be able to run `npm run storybook`, or `yarn start storybook` or whatever is specific for you.

This should show a URL or open a browser window which shows the UI.

## References

1. https://storybook.js.org/
2. https://storybook.js.org/docs/react/get-started/introduction

