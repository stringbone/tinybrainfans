---
title: Testing Vanilla Javascript
description: Testing vanilla Javascript is simple, but different than testing a React project.
---

Testing vanilla {{Javascript}} is not as straightforward and laid out as {{React}} or other {{frameworks|Frameworks (Javascript)}}, so it takes some finangling.

Most simple would be to use a single webpage only for the tests. To do this, you can use {{Jasmine}}'s standalone SpecRunner[3], a zero-dependencies test runner in a webpage that has language very similar to {{Jest}}.

If you want to test a function or component, you can add the file that contains it as a `script` in the SpecRunner's {{HTML}}, followed by the spec file that tests it. Jasmine will recognize the tests and then show which passed or failed. It's nice and clean and lets you unit test without having to pull in bulky {{node}}/{{npm}} environments. You can follow their instructions[3] for an easy setup and implementation.

## References

1. https://dev.to/thawkin3/how-to-unit-test-html-and-vanilla-javascript-without-a-ui-framework-4io
2. https://dev.to/aurelkurtula/unit-testing-with-vanilla-javascript-the-very-basics-7jm
3. https://jasmine.github.io/pages/getting_started.html
