---
title: Go
description: Go is a compiled programming language.
---

## Install and Run

Use {{homebrew}}: `brew install go`. To run an app or a repo, use `go run run.go`.

