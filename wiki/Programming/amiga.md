---
title: Amiga
description: The Amiga is a computer that gained popularity in the 80's and 90's.
---

## Internet

From @GabeMoralesVR@social.linux.pizza[1]:

> number of different ways -- you can image the file onto an actual floppy disk and run it that way. If you make an ASM-ONE floppy, put it into your amiga, and boot, it'll boot straight into the editor. Alternatively, if you have a hard drive on your amiga, you can copy the lha file (it's the amiga version of zip) to the hard drive and unzip it there using a network adapter. The easiest way these days, however, is to use a gotek floppy emulator and pop it on a USB drive.
>
> assuming you have an amiga with no hard drive and don't have a gotek and want to run asm-one there, the easiest way to do that would be to use a null serial cable to link your PC to the amiga. They sell serial to USB cables online you can use. You'd link one end of the cable to the amiga's serial port, and the other end would be USB that plugs into your PC. Then you'd boot into amiga workbench and copy the file over serially and run ASM-ONE from workbench.
>
> if you're playing with a real amiga, depending on the model, I'd recommend either popping in a CF card kit, which lets you use a Compact flash card as though it was an amiga harddrive, or grabbing a gotek floppy emulator, which is a box that the amiga sees as a floppy drive, but has a USB port so you can put adf images (amiga floppy disk images) onto a USB drive, that the amiga will read as though they were floppies.
>
> video on gotek: https://www.youtube.com/watch?v=cUxsmQxJZ3c
> Honestly, using a gotek is probably the easiest way to do all this, but you'd have to order one online.
>
> here's a video on using ASM-ONE to create some amiga demos: https://www.youtube.com/watch?v=p83QUZ1-P10
>
> another way you could do it -- you could buy a plipbox for your amiga. That's a parallel port device you add to the amiga that gives it an ethernet port, so you can connect it to a modern network. With a plipbox, you could connect your amiga to your modern network, and access your PC through FTP, and copy the ASM-ONE files to the amiga that way. Plipbox: https://www.youtube.com/watch?v=f5A5f2l3GkE

## References

1. https://social.linux.pizza/@GabeMoralesVR/
