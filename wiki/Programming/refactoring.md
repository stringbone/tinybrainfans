---
title: Refactoring
description: Refactoring is improving your code while not affecting the end result.
---

Refactoring is a big part of {{test-driven development}}.

> I characterize refactoring-the-discipline with these features:
>
> * changing code incrementally
> * keeping the code “working”
> * intent to improve the design
>
> The act of keeping the code “working” corresponds to the idea of “not changing observable behavior”. But since preserving all observable behavior is strictly speaking impossible to guarantee in general, I focus on keeping the code “working”.[1]

## References

1. https://blog.thecodewhisperer.com/permalink/what-is-refactoring

