---
title: Interpreters and Compilers
description: The goal of an interpreter or a compiler is to translate a source program in some high-level language into some other form.
---

The goal of an {{interpreter}} or a {{compiler}} is to translate a source program in some high-level language into some other form.

A compiler transforms **source code** into **machine language** into **machine actions**. An interpreter interprets the **source code** and converts that into **machine actions** directly.

## Parts

| Word           | Definition                                                                                                                                     |
|----------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| token          | A chunk of source code that represents a given type of data, like `if`, `else`, `string`, `number`.                                            |
| lexeme         | The sequence of data that makes up a token. Like `if`, `else`, `"waka waka"`, `1234`.                                                          |
| parsing        | Recognizing a phrase in a stream of tokens.                                                                                                    |
| formal grammar | Describes which strings from an alphabet of a formal language are valid according to the language's syntax.                                    |
| terminals      | Symbols that may appear in the outputs of the production rules of a formal grammar and which cannot be changed using the rules of the grammar. |
| non-terminals  | Symbols that are replaced by groups of terminal symbols according to the production rules.                                                     |

## References

1. [Let's build a simple interpreter](https://ruslanspivak.com/lsbasi-part1/)

