---
title: Parsers
description: Parsers are a way of converting input data from a string into meaningful tokens and data.
---

## Activities

Parse something simple (like an INI file, `Key=Value`) with a finite state machine

## References

1. https://lukaszwrobel.pl/blog/math-parser-part-1-introduction/
2. https://www.scaler.com/topics/infix-to-postfix-conversion/
3. https://www.youtube.com/watch?v=dDtZLm7HIJs
4. https://www.youtube.com/watch?v=bxpc9Pp5pZM
5. https://www.youtube.com/watch?v=r6vNthpQtSI
6. https://www.youtube.com/watch?v=tH5AOX9929g
7. https://www.youtube.com/watch?v=ggxEzR2VRNU
8. https://medium.com/dailyjs/compiler-in-javascript-using-antlr-9ec53fd2780f

