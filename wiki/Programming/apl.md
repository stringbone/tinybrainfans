---
title: APL
description: APL is an array programming language.
---

APL is an array programming language. It's essentially purely a mathematical language that seems like it's entirely an exercise in chunking composable primitive array actions together. These chunks are represented with {{unicode}} symbols, so make sure you are using a font with extended symbols or this page will make no sense at all.

For instance, mulitplying all elements in an array in something like {{Javascript}} could look like:

```javascript
// Starting with list `a`
// const a = [1,2,3,4,5];
const sum = a.reduce((acc, num) => acc * num);
```

whereas in APL, it looks like this

```apl
⍝ Starting with list `A`
⍝ A←1 2 3 4 5
×/A
```

All operators in APL are unicode characters and have two different behaviors: one is if given a single argument, the other is if given multiple arguments.

In the example above, the `/` character **reduces** the array to a scalar (a single element), applying the command to it's left to the previous output and the next value. It repeats this process until all elements are consumed. The command that is used in the reduce is `×`, which is the multiplication command.

## Editor

You should use the Dyalog editor. You can download it from their website and it makes development _possible_. This editor helps you enter all these symbols with help on what they do, it is indispensable.

### Configuration

Once you have it open, you should apply these settings in the REPL:

-   `]boxing on` - This adds boxes around the different array elements. This is essentially required and I'm unsure why it's off at the start.

## Glossary

<ds>
    <dt>Scalar</dt>
    <dd>a single element</dd>
</ds>

## References

1. [APL (programming language) - Wikipedia](https://en.wikipedia.org/wiki/APL_%28programming_language%29)
2. [Dyalog APL](https://www.dyalog.com/)
3. [Advent of Code 2021 in APL #1! - YouTube](https://www.youtube.com/watch?v=DNYxfoCEVEM)
4. [BQN Tutorial: Working with list](https://mlochbaum.github.io/BQN/tutorial/list.html)
