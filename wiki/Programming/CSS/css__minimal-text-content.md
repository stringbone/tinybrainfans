---
title: Minimalist CSS
description: Small bits of CSS that do a lot.
---

## Boilerplate

This {{CSS}} code makes really beautiful text-centric websites that are responsive and look nice with only like 54 bytes or something? I use this as a basis all the time for making nice looking pages.

```css
main {
  max-width: 38rem;
  padding: 2rem;
  margin: auto;
}
```

A tiny bit more space yields a nice result, too[3]:

```css
html {
  max-width: 70ch;
  padding: 3em 1em;
  margin: auto;
  line-height: 1.75;
  font-size: 1.25em;
}
```

## Modern CSS Reset/Normalize

This is from Josh W. Comeau[5] and is a great way to make CSS do what you expect.

```css
/* 1. Use a more-intuitive box-sizing model */
*, *::before, *::after { box-sizing: border-box; }

/* 2. Remove default margin */
* { margin: 0; }

body {
  /* 3. Add accessible line-height */
  line-height: 1.5;
  /* 4. Improve text rendering */
  -webkit-font-smoothing: antialiased;
}

/* 5. Improve media defaults */
img, picture, video, canvas, svg { display: block; max-width: 100%; }

/* 6. Inherit fonts for form controls */
input, button, textarea, select { font: inherit; }

/* 7. Avoid text overflows */
p, h1, h2, h3, h4, h5, h6 { overflow-wrap: break-word; }

/* 8. Improve line wrapping */
p { text-wrap: pretty; }
h1, h2, h3, h4, h5, h6 { text-wrap: balance; }

/* 9. Create a root stacking context */
#root, #__next { isolation: isolate; }
```


## References

1. https://jrl.ninja/etc/1/
2. {{Classless CSS}}
3. https://www.swyx.io/css-100-bytes
4. https://github.com/ajusa/lit
5. https://www.joshwcomeau.com/css/custom-css-reset/

