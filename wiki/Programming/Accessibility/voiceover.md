---
title: VoiceOver
description: VoiceOver is an accessibility tool by and for MacOS.
---

VoiceOver is an {{accessibility}} tool by and for MacOS.

## Getting Started

To activate Voiceover to be used, go into your System Preferences and enable it from your Accessibility settings.

You can customize your `VO` key between holding `Caps Lock` or the combination of `Control + Option`. Below, `VO` refers to this.

## Navigation

Using `tab` will move you to the next interactive element[3]. This puts you in "form mode".

Using `VO + arrow keys` will move you through the document in DOM order. Up and down will move you inside or outside of the current element, and left and right will move you to the previous or next element. This is "browse mode".

Use the rotor! It is much easier to move around the whole page than by using tab or the arrow keys.

## Cheatsheet

Using `Shift` on any of the "Next" actions will go to the previous element.

`Ctrl` will stop messages from playing.

| Keystroke   | Effect                              |
|-------------|-------------------------------------|
| `cmd + F5`  | De/activate VoiceOver               |
| `VO + Down` | Enter web page                      |
| `VO + Up`   | Exit page                           |
| `VO + A`    | Read all                            |
| `VO + U`    | Open rotor (categorical navigation) |
| `VO + H`    | Next heading                        |
| `VO + I`    | Next image                          |
| `VO + L`    | Next link                           |
| `VO + J`    | Next form control                   |

## References

1. [Voiceover commands for web page testing](https://accessibility.psu.edu/screenreaders/voiceover/)
2. [Mac VoiceOver Testing the Simple Way](https://cloudfour.com/thinks/mac-voiceover-testing-the-simple-way/)
3. [Interactive elements](https://html.spec.whatwg.org/multipage/dom.html#interactive-content)

