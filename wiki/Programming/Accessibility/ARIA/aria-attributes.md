---
title: ARIA Attributes (Reference)
description: This is a reference of various ARIA attributes.
---

## `expanded`[1]

It is "is a good solution for many situations where an element's visibility should can be toggled"[1]. If the element creates some kind of disclosure based on a trigger, use `aria-expanded`. This can be used in conjunction with `aria-haspopup`, but isn't really necessary.

## References

1. [Marking elements expandable using aria-expanded - ADG](https://www.accessibility-developer-guide.com/examples/sensible-aria-usage/expanded/#real-world-use)
