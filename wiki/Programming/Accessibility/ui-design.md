---
title: Accessible User Interface Design
description: A good UI is an accessible UI.
---

## Jakob's Law[2]

> Users spend most of their time on other sites. This means that users prefer your site to work the same way as all the other sites they already know. Design for patterns for which users are accustomed.

## Nielsen's 10 Usability Heuristics for User Interface Design[1]

1. Visibility of System Status: Keep users informed.
2. Match Between the System and the Real World: The design should speak the users' language.
3. User Control and Freedom: Give users an "emergency exit" to leave an unwanted action without having to go through an extended process.
4. Consistency and Standards: Use the same words for the same things, follow conventions (see Jakob's law[2]).
5. Error Prevention: Eliminate error-prone conditions or present confirmation for users and let them know that where they are going _is_ error-prone.
6. Recognition Rather than Recall: Information required to use the design (e.g. field labels or menu items) should be visible or easily retrievable when needed.
7. Flexibility and Efficiency of Use: Allow users to tailor and customize frequent actions.
8. Aesthetic and Minimalist Design: Interfaces should not contain information that is irrelevant or rarely needed.
9. Help Users Recognize, Diagnose, and Recover from Errors: Precisely indicate the problem, and constructively suggest a solution.
10. Help and Documentation: It’s best if the system doesn’t need any additional explanation. Otherwise, provide documentation.

## Names

If you are making a form, just ask for a full name[3]. Similar to handling datetime values, there is not usually a good reason to try and handle every single edge case. Instead, just allow what people say their name is and put it in your database. (We have many assumptions about names[6] and it is quite frustrating for anyone that is not the "normal" of the society that creates the system; just ask Patrick McKenzie while he was living in Japan[6])

> During both our E-Commerce Checkout Usability and M-Commerce Usability studies users consistently considered their name a single whole – I am not “James” and “Newman”, I am “James Newman”. Therefore, users often enter their entire name in the “First name” field and then upon advancing to the next field, discover that they must now enter their last name. They then go back to delete their last name from the “First name” field, only to advance to the “Last name” field again to complete it.
>
> While numerous sites ask for the user’s name in two or more fields it simply is not good usability. Of course it can be difficult to discover this if you’re not doing usability tests since all subjects we observed noticed and corrected the error before submitting the form (thus not showing up in most form tracking web statistics). It is therefore not a critical error to make but it does introduce needless friction to the checkout experience. Amazon seems to have reached the same conclusion and instead asks for the user’s “Full name”.[4]

If you want their more "informal" name with which to address them, just ask!

```html
<form>
    <label for="full-name">
        Full name
        <input type="text" id="full-name" name="full-name" />
    </label>
    <label for="salutation">
        What should we call you?
        <input type="text" id="salutation" name="salutation" />
    </label>
</form>
```

## References

1. [10 Usability Heuristics for User Interface Design](https://web.archive.org/web/20240210140105/https://www.nngroup.com/articles/ten-usability-heuristics/)
2. [Jakob's Law](https://web.archive.org/web/20240210140144/https://www.nngroup.com/videos/jakobs-law-internet-ux/)
3. [Personal names around the world](https://www.w3.org/International/questions/qa-personal-names)
4. [Mobile Form Usability: Avoid Splitting Single Input Entities](https://baymard.com/blog/mobile-form-usability-single-input-fields)
5. [Names - Gov.UK](https://design-system.service.gov.uk/patterns/names/)
6. [Falsehoods Programmers Believe About Names](https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/)
