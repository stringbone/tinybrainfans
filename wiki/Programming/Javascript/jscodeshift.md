---
title: JSCodeShift
description: JSCodeShift is a Javascript Codemod toolkit.
---

JSCodeShift is a Javascript Codemod toolkit.

## Getting Started

// How to 

## References

1. [JSCodeShift on Github](https://github.com/facebook/jscodeshift)
2. [CodeshiftCommunity](https://www.codeshiftcommunity.com/)
3. [AST Explorer (lets you execute jscodeshift on sample code)](https://astexplorer.net/)
4. [Writing your very first codemod with jscodeshift | by Andrew Levine | Medium](https://medium.com/@andrew_levine/writing-your-very-first-codemod-with-jscodeshift-7a24c4ede31b)
5. JSCodeShift API: For [nodes](https://github.com/facebook/jscodeshift/blob/main/src/collections/Node.js) and [collections](https://github.com/facebook/jscodeshift/blob/main/src/Collection.js)

