---
title: Frameworks (Javascript)
description: Javascript frameworks are abundant and everywhere, so I wanted to collate a list with some blurbs and first impressions.
---

{{Javascript}} frameworks are abundant and everywhere, so I wanted to collate a list with some blurbs and first impressions.

Disclaimer that ease of development should **not** be your only criteria when picking a framework. There are many things that are sacrificed the more involved your client's workload gets[13-15].

## Frameworks

{{Alpine.js|Alpine}}: A really minimal framework that also allows functionality to be added to the {{HTML}} markup. Surprisingly full featured, has side-effect handling and a global store, too.

Backbone[4]: A jQuery solution that is more of a way to model and organize the data you are going to be passing around. Uses templating from [Underscore](https://underscorejs.org/) to build components. [A tutorial is here](https://adrianmejia.com/backbone-dot-js-for-absolute-beginners-getting-started/).

Fresh[10]: Server-side framework that allows selective hydration for *very* fast pages. Uses {{Preact}} (which is a smaller {{React}}) and {{Typescript}}.

htmx[6] and \_hyperscript[8]: Super lightweight and lets you bake in your actions directly to the HTML. If your app is simple enough, means no extra JS is needed at all. Wild stuff.

Marko[3]: Can write your functionality into the HTML similar to Vue.

{{React}}: The classic. Makes SPA's slick and clean using components, but requires quite a few dependencies and {{Webpack}} and {{Babel}}, so is a bit unwieldy.

{{SolidJS}}: A healthy blend of the React-like declarative reactive framework, with the benefit of compiled vanilla {{Javascript}} code that isn't difficult to manipulate in the way that React often prohibits.

{{Svelte}}[7]: A framework that has no dependencies; all the tooling, building, compiling are made in house. Seems to look really good, and I would love to use this in the future.

{{Vue}}: Allows functionality to be written in your HTML components, along with accompanying {{CSS}} and JS for each component.

## References

1. https://reactjs.org/
2. https://github.com/vuejs/vue
3. https://markojs.com/
4. https://backbonejs.org/#Getting-started
5. https://alpinejs.dev/
6. https://htmx.org/
7. https://svelte.dev/
8. https://hyperscript.org/docs/
9. [Pretty comprehensive list of frameworks and their many different types](https://thomasorus.com/list-of-javascript-frameworks.html)
10. https://fresh.deno.dev/
11. https://vanillajstoolkit.com/libraries/
12. https://www.solidjs.com/
13. [Making the world's fastest website and other mistakes](https://dev.to/tigt/making-the-worlds-fastest-website-and-other-mistakes-56na)
14. [radEventListener: a Tale of Client-side Framework Performance](https://css-tricks.com/radeventlistener-a-tale-of-client-side-framework-performance/)
15. [Frontend Performance: React SSR and the Uncanny Valley](https://crystallize.com/blog/frontend-performance-react-ssr-and-the-uncanny-valley)

