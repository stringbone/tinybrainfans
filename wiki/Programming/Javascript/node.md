---
title: Node
description: Node is a server environment in Javascript.
---

Node is a server environment in {{Javascript}}. Node is bundled with {{NPM}} and is used for most webapps these days.

## References

1. [How to deploy your node app on Linux](https://web.archive.org/web/20180212163211/https://certsimple.com/blog/deploy-node-on-linux)

