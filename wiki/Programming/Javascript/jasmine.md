---
title: Jasmine
description: Jasmine is a Javascript testing framework.
---

Jasmine is a {{Javascript}} {{testing}} framework.

## Getting Started

If you are using this to {{test vanilla Javascript|Testing Vanilla Javascript}}[2], you can either install it as a standalone[3] or using {{npm}}. Jasmine can either be run in the command line or using a browser spec file.

## Running Specific Tests

You can **f**ocus on specific tests or suites by prefacing `describe` or `it` with `f` (`fdescribe` or `fit`).

## References

1. [Matchers](https://jasmine.github.io/api/edge/matchers.html)
2. https://jasmine.github.io/pages/getting_started.html
3. https://github.com/jasmine/jasmine#installation

