---
title: Web Development Resources
description: Things that can make web development a little more bearable.
---

- [Free Public APIs](https://github.com/public-apis/public-apis)
- {{Classless CSS}}
- {{|Javascript Frameworks|Frameworks (Javascript)}}
- [Making the world’s fastest website, and other mistakes](https://dev.to/tigt/making-the-worlds-fastest-website-and-other-mistakes-56na)
- [Maintaining JavaScript applications in the long term](https://9elements.com/blog/maintaining-javascript-applications-in-the-long-term/)
