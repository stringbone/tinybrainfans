---
title: Accessibility For Web Design
description: Accessibility means making everything easier for everyone.
---

*These are the notes from Derek Featherstone's course on accessibility in web design. It's a fairly quick course full of good stuff, I reommend checking it out.*

Making something {{accessible|Accessibility}} doesn't just mean technology. It also means good design, development, and content.

## Personas/Psychographic Profiles

Use personas or psychographic profiles to check that you are meeting people's needs. Personas and psychographic profiles represent mindsets, motivations, goals, as well as functional needs. When you are designing, ask how each of your personas would use what you are building. Would they use it the way you expected? If they don't, does it still work? Would they be happy using it? What would they be missing?

### Examples

* Giovanni
    * 27, single, gallery artist
    * Prefers simple and clear instructions
    * Looking for a gift
    * On the autism spectrum
* Rose
    * 34, married, student and journalist
    * Travels for work
    * Uses podcasts to stay current
    * Lives by phone
    * Has very little patience
    * Is almost completely deaf since birth
* Mary
    * 48, married, two teenagers, musician and professor
    * Teaches with podcasts and video
    * Travels with family
    * Works quickly when focused
    * Is blind
* Sean
    * 31, UI engineer, engaged
    * Stays up to date with tech
    * Works long hours
    * Shops only online
    * Low vision, needs 400% with magnifier
    * Doesn't use touch, only keyboard and magnifier
* Maxine
    * 68, widowed with grandkids, insurance claim provider
    * Hopes to retire
    * Needs help from the house
    * Has strong people skills
    * Has arthritis from computer work
* Renata
    * Product owner in insurance
    * Lost both arms in the military
    * Loves podcasts
    * Uses phone on the go, and tablet at home
    * Uses feet on touch devices

## Vision

### Colorblindness

1 in 12 men and 1 in 200 women are colorblind. That is 320 million people worldwide!

Use different shapes and/or lines, or assistive text to not *rely* on color. You can and should still use color though, because it is nice and aids in usability for those that *can* see color.

Let's say you have a component which relies on color. What are some ways you can make it accessible? Some common strategies are utilizing shapes or patterns alongside color, properly organizing content so that color is not the only thing distinguishing the order, or adding further textual elements.

For example, how could one convey success or failure alongside color to not rely solely on green and red? Some ideas are:

* Using a check or an 'X' for success/failure
* Rounded or hexagonal borders to contain the message text
* Prefacing the message with "Success"/"Failure"
* Dark text on light background for success and light text on dark background for failure

Using multiple methods to convey the meaning is the best way to ensure that the meaning gets across, regardless of ability.

### Contrast Requirements

The appropriate amount of contrast between foreground and background text are set by WCAG, specifically from their 2.0 spec:

* 4.5:1 for meaningful text, icons, and graphics
* 3:1 for large or bold items

Use contrast checker[3] to see if colors work well together, and Jonathan Snook's color contrast checker[3], which lets you modify the colors to get them where you want with regard to the WCAG guidelines.

When developing a design system, always use compliant colors and show warnings for colors that should not be used together.

### Animation and Motion

Animation and motion are cool for design but can cause vertigo, nausea, or headaches for some people with vestibular disorders. We need to use animation **responsibly**.

* Let users turn it off
* Don't rely on it
* Define a logical start and end
* Consider using dissolve effects instead of motion effects

People with photosensitive epilepsy can be triggered by flashing lights, as content flashing more than three times per second can trigger seizures. Content warnings should be used if flashing lights are present.

### Low Vision

> [Having low vision is] like looking through a straw all the time.

To test usability for those with low vision, use the straw test: get a straw or hold up your fist and look through it at your content. This limits your view considerably and renders a lot of otherwise benign components unusable.

A design principle that you can use to help those with low vision is the principle of proximity: related elements should be grouped together near one another to show their relationships.

### Keyboard Usage

Keyboard usage is not just for the non-sighted, but also for those with physical ailments or restrictions, like arthritis. This means that it isn't just about people using screen readers, you also need to make a nicely visible focus that is easy to recognize as they tab through your site.

Though using `outline: none;` and not replacing it is not helpful to meet the end of visible focus outlines, using it and *replacing it* is, since the default is not visible enough; in dark designs, it's really bad, so make sure that your focus state is visible.

**If you remove a native part of the browser, your replacement has to be as good or better.**

Plan your `:focus` states:

* Reuse `hover` for `focus`
* Use `text-decoration: underline;`
* Use `background-color` and `color` changes

**Test it fully with a keyboard!** Test test test, as Gerard K. Cohen says[4].

## Keyboard interaction

You **must** be able to complete all tasks relevant to your site using a keyboard.

`div`s are a last resort. Users can't tab onto `div`s by default. Use the appropriate {{semantic elements|Semantic HTML}} at all times.

Not everything has to be tabbable to be accessible. Focus on the function of the interface and let that guide your decision making.

### Traps

A keyboard trap is a situation where the keyboard gets stuck in a part of the page. Examples:

* A field where tab is interpreted as a character and not a way to select other fields, or {{Javascript}} won't let you leave the field until it is filled out
* Fields that automatically tab to the next field when complete; because the Javascript sees the field previous is filled out, the user can't go back, they have to cycle all the way around
* Fields that start in one direction and then end up in a recursive loop that doesn't allow certain fields to be accessed (think like a lasso where one path starts but then gets stuck in the loop)

These scenarios can be avoided through accurate testing that includes these scenarios, as well as documenting the tab order in the design and what each field should do when interacted with.

### Linear Flow

The content should flow linearly when using tabs for a keyboard user. When designing the page, think about the flow of interaction. Using an e-commerce site as an example. You have a billing page with an order summary, billing information, and the button to proceed to the next page.

* Order Summary
    * If inaccurate, the user should be able to modify the content
    * If accurate, the user should not have to do anything
* Billing Information
    * The user should be able to enter in all their billing information
* Next Page/Confirm
    * If pressed, the user will be taken to the confirmation screen

We can see that the user may want to modify their order before proceeding if the summary is not what they expected. Putting the summary after billing would make this a pain if the billing is already filled out. Similarly, if the 'next' button is in between the two sections and not after, there would be a lot of backtracking to get to where you want to go (or worse, they may not even notice the summary at all and proceed without attending to it). The order becomes much clearer as you view the site through the lens of a keyboard user.

These changes can be purely structural and not affect the design either, making this a trivial change for the developers but a huge win for usability overall.

## Touch interfaces

There are many kinds of touch interfaces, beyond the standard "touch":

* Touch
* Multitouch
* Gestures
* 3D touch

People are not all the same. We use different fingers, thumbs, combinations, feet, toes, nose, or none of those to do stuff. Some people may never touch the screen at all. It **must** work for **everyone**.

### Target sizes for guidance

* Microsoft: 34px x 34px
* Apple: 44px x 44px
* Google 48px x 48px

These may not even be big enough! Make your targets bigger and make them accessible. Your design should accommodate these sizes at *least*.

### Gestures

Gestures are useful for accessibility, allowing unsighted users to traverse a page easily and generally map complex interactions to be used more easily by more people. However, not all people can use them. Can these gestures have alternatives like double clicking or alternatives that are less precise?

For gestures and their documentation with regard to accessibility, check Microsoft, Apple and Google's docs.

If what you want conflicts with a gesture that already exists, use something else. Don't rely on the one option, provide a different one!

Last but not least, how would the operation work if the gesture couldn't be done?

## Media

To make media accessible, we first need to understand the purpose of the content. This means that we know what the producers *and* the consumers of this media need.

For instance, in educational content, students may use videos by pausing and rewinding a lot to study, going frame by frame, speeding it up or slowing it down, zooming in, etc. For podcasts, this could mean downloading the content for offline use, reading a transcript, or scanning the podcast for certain sections using the web player. Consider how people create and consume the content being provided and make performing those actions accessible.

### Text Equivalents

There are three different content types:

* Informational content: media that is important in it's own right and needs a text equivalent
* Functional content: images in links or form controls used for navigation of browser tasks; also needs a text equivalent.
* Decorative content: not important to the page, non-functional. No text equivalent needed. (If this was gone, would the functionality of this page or the meaning of the content be affected?)

The text equivalents provided must be **equivalent**. Show notes are not a substitute for a podcast. Cliff notes are not a substitute for a book.

For images, `alt` tags are not required if there is text associated with or directly next to the image itself. For instance, if you have a page for a shirt for sale with different colors to select, you don't need `alt` text for a blue swatch that has the word "Blue" next to it. You can use an empty `alt` tag (`alt=""`) to ensure assistive technologies skip the image description.

{{`alt` text|Accessibility}} is not just for reading out the correct thing. Voice recognition software requires that be accurate as well. For instance, if you have a link with an image that has "Click here" rasterized into the image but with no alt text attached, you can't say "Click 'click here' link", since "click here" is not written anywhere for the technology to recognize. It might recognize it if you say "Click 'button-click-here.png' link", but obviously that's not anywhere near a solution.

Ask yourself:

* What is the overall purpose of the visual?
* What's the best way to convey that?
* Which parts are decorative, informational, or functional?

Videos can provide captions for deaf users, but need audio descriptions for blind users. Audio descriptions are a text equivalent of the functional and informational video content, but sometimes you want all three types in there.

## Forms

Label {{forms|HTML Forms}} using `label` elements so that there is a relationship between the field and the text supporting it. This makes the prompts clearer, bigger click/touch targets, and acts as a memory aid for those with memory problems.

Every form field needs a `label`, or if necessary an {{ARIA label|ARIA Labels}} (e.g. `aria-labelled-by`). This helps with assistive technology, too, as the labels will give context in what to select or use. There should be one to one relationships for labels to fields.

### Placeholder Attributes

While placeholders are helpful, having the placeholder disappear sucks if you have memory issues. Placeholders also can make people think it's already filled out and be confusing. Having low vision makes placeholders pretty difficult to read. An alternative that works okay is a label that visually rests in the field itself and on selection, moves out of the way. Regardless, a programmatic label must be visible.

### {{HTML5 Form|HTML Forms}} Inputs

Types like `tel`, `email`, and `password` add extra useful information for all users. Mobile devices use them to create specific interfaces for users, but desktop is not there yet. However, in the future, this semantic information could become very useful and should be used to futureproof your sites.

### Validation and Errors

Validation is hard to get right, but here are some guidelines to make it a little easier. It should:

* Establish consistent placement and pattern or design
* Be visually close to the field
* Provide a clear and simple error message
    * No jargon
    * At a low reading level for those where the language is not their first)
* Be programmatically connected to the associated field
    * Put inside the label element
    * Use `aria-describedby`

Think of how your errors work in *your* design:

* How will you handle all the specific scenarios of your application?
* What will you do about longer errors?
* How will you make the text resizing responsive?
* How can you accommodate other languages?

## Visual Flexibility

Which resolution do we design for? **All of them**. Design with flexibility in mind, allowing resizing, magnification, and better scrolling.

### Reflowing content

Linear order matters; your content should be in a logical order.

* Reflow multi-column forms to a single stack
* Prioritize content for the smallest size
* Allow pinch and zoom

A tablet does not mean they are using touch. Viewport size does NOT mean they are using a specific device!

Flexibility allows different line lengths, text sizes, and zoom levels. Remember that everything can be changed and your site still **must** work. You can set zoom to only affect text, so that images and other media stays put. You should be able to resize to 200% and everything should still be usable. At all sizes, your site should:

* Show all relevant content
* Have no overlapping of content
* Provide meaning and functionality
* Not become too narrow

### Content

Your content should be written in a way that lets people make decisions quickly. If someone only read the headings on your page, would they be able to understand what's going on? The first sentence of each paragraph?

Page titles are important. They are the first thing announced when visiting a page, and it confirms the user is at the right place. The titles should be unique and should go from specific to general (e.g. `Order Form -- ZomboCom Industries`. The page title and the `h1` tag should be symbiotic and assist each other.

Use meaningful markup within the page in the way it was designed to be used. Use the HTML that makes the most {{semantic|Semantic HTML}} sense. The least semantic elements are `div`, `span`, and `section`. Use these *only if necessary*.

## References

1. [Accessibility for Web Design course from Derek Featherstone](https://www.linkedin.com/learning/accessibility-for-web-design/an-ecommerce-purchase-14870326?autoplay=true&resume=false)
2. "Color Accessibility Workflows" by Garri Coady
3. [Contrast checker](https://webaim.org/resources/contrastchecker/) and [Jonathan Snook's color contrast checker](https://snook.ca/technical/colour_contrast/colour.html)
4. https://www.deque.com/blog/aria-spec-for-the-uninitiated-part-2/

