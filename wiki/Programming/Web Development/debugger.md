---
title: Debugger in Chrome
description: The debugger command in Chrome is really useful for diagnostics and testing.
---

The `debugger` command can be used anywhere in your source code or in the console of the dev tools in Chrome to initiate the built-in debugger. This can be useful if you want to debug somewhere deep in the code for manual testing. This can be used alongside or instead of a ton of `console.log` statements.

```javascript
const addHundred = (var) => {
    const newVar = 100 + var;
    // This line will trigger the debugger while in this function
    debugger;
    return newVar;
}
```

There are situations where you are trying to debug a problem in the browser, but certain states (hover, CSS popover, etc.) don't allow proper use of the inspection tool. In these instances, you can run the following in your console, trigger the desired state, and then wait for the debugger to start:

```javascript
// Wait five seconds and then start the debugger
setTimeout(function () {
    debugger;
}, 5000);
```

## References

1. [css - Freeze screen in chrome debugger / DevTools panel for popover inspection? - Stack Overflow](https://stackoverflow.com/a/28454481/14857724)
