---
title: XAMPP
description: XAMPP is a tool for developing locally using the LAMP stack.
---

XAMPP is a tool for developing locally using the LAMP stack ({{Linux}}, {{Apache}} server, {{MySQL}}, {{Perl}}/{{PHP}}/{{Python}}).

## Getting started

1. Install XAMPP.
2. Open XAMPP.
3. Start your Apache server, MySQL, and the FTP server.
4. Go to `localhost` in your browser.
5. Hooray!

## PHP My Admin

You can access a GUI for MySQL via `http://localhost/phpmyadmin/`. You may need to adjust the port based on your settings.

## References

1. [PHP for Beginners](https://www.youtube.com/watch?v=BUCiSSyIGGU)
2. [XAMPP Homepage](https://www.apachefriends.org/index.html)
