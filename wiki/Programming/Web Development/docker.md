---
title: Docker
description: Docker is a way to normalize and simplify deployment of software.
---

Docker is a way to normalize and simplify deployment of software.

## Glossary

| Word               | Description                                                                   |
|--------------------|-------------------------------------------------------------------------------|
| Daemon (`dockerd`) | Listens for API requests and manages docker objects.                          |
| Client (`docker`)  | CLI, how most users interact with Docker.                                     |
| Desktop            | A GUI that makes building and sharing images easier.                          |
| Image              | A read-only configuration that can be deployed onto a machine running Docker. |
| Registry           | A place to store Docker images. Docker Hub is a public registry.              |
| Container          | A runnable version of an image.                                               |

> Shamelessly taken from [Eric Pearson/@nyloneric](https://gist.github.com/NylonEric/372516f2fe3c9f708d20e7e2f77007cb#file-2021-06-23-md), as I haven't documented my own process yet.

## Create a Container

Make the thing: https://flaviocopes.com/docker-node-container-example/

[Create a simple Node.js Hello World Docker Container from scratch](https://flaviocopes.com/docker-node-container-example/)

You will run into errors, so:

- `=> [internal] load metadata for ``docker.io/library/node:14`
  - Solved with: `sudo DOCKER_BUILDKIT=0 docker build . -t user/node-web-app`
  - discussion: https://stackoverflow.com/questions/64221861/failed-to-resolve-with-frontend-dockerfile-v0
- [Failed To Resolve With FrontEnd DockerFIle.v0](https://stackoverflow.com/questions/64221861/failed-to-resolve-with-frontend-dockerfile-v0)
  - more details about docker file: https://nodejs.org/en/docs/guides/nodejs-docker-webapp/enter

Container: `docker exec -it <container id> /bin/bash``docker run -p 49160:8080 -d <your username>/node-web-app`

[Docker Hub](https://hub.docker.com/signup)
[Dockerizing a Node.js web app | Node.js](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/)
I also really like [Mosh Hamadani tutorials](https://www.youtube.com/watch?v=pTFZFxd4hOI)

## Make a dockerfile

```dockerfile
FROM node:14

COPY package*.json ./
COPY tsconfig.json ./
COPY yarn.lock ./

RUN yarn install

COPY . .

RUN yarn build

EXPOSE 3000

CMD ["yarn", "start"]
```

## Build

```bash
$ docker build -t NAME .
```

## Run

```bash
$ docker run -dp 3000:3000 NAME
```

The package should now be available on your local machine


## References

1. https://www.freecodecamp.org/news/the-docker-handbook/
2. https://docs.docker.com/get-started/overview/
3. https://scribe.rip/swlh/what-exactly-is-docker-1dd62e1fde38

