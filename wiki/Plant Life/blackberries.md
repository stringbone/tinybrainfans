---
title: Blackberries
description: The Himalayan blackberry is an invasive plant and should NOT be intentionally planted. But it has a zillion uses.
---

This is an invasive plant and should NOT be intentionally planted. But it has a zillion uses.

## Leaves

The leaves can be dried out (or fermented) and used for {{tea|blackberry leaf tea}}.

## Canes

The young canes can be used to make {{cordage}}. The older canes can be use for constructing light structures[3], like a garden trellis or decorative fencing.

## Berries

Of course, you can eat the berries. You can also use them to make jam, compote, etc.

## References

1. [Blackberry – Health Benefits and Side Effects](https://www.herbal-supplement-resource.com/blackberry.html)
2. [Exploring the Antioxidant Potential of Blackberry and Raspberry Leaves: Phytochemical Analysis, Scavenging Activity, and In Vitro Polyphenol Bioaccessibility](https://pmc.ncbi.nlm.nih.gov/articles/PMC10740815/)
3. [Blackberry cane trellis](https://rainyleaf.com/2014/06/01/blackberry-trellis/)
