---
title: Consulting/Freelancing
description:
---

## Pay

> - Decide how much you want to make. Let's use a nice round number. USD$100,000 (Not saying this is your number, yours may be USD$250,000, or whatever. This is just an example.)
> - Remember to add health insurance, business insurance, error & omission insurance, taxes and fees and everything else. (Call an insurance broker, techinsurance.com, etc.)
> - Let's call this compiled number USD$150,000. (again, just an example.)
> - Double it. USD$300,000. You can expect no more than 50% utilization, and you have to spend time finding the next client/gig/opportunity. So you will only make half a years worth of hourly salary.
> - Convert to hourly. There are 2080 working hours in a year. So you can have a vacation, call it 2000. Divide 300,000 by 2,000. That's 150. So your hourly rate is $150/hour.
> - If you are 50% utilized, you will make USD$150,000 per year. With six months of time to find work, have a vacation, keep your certs alive, and expand your skill set.

## Work

> if one baker can make a loaf of bread in an hour in an oven, that does not mean that ten bakers could make ten loaves in one hour in the same oven, due to the oven's limited capacity.[4]

### Work Triangle

The client can only have two[4]:

- Cheap
- Fast
- Good

If you are doing all three, you should charge more, for the sake of yourself and the community.

## References

1. https://www.reddit.com/r/talesfromtechsupport/comments/l2pqsl/handover_of_work_total_disaster/
2. https://thomasorus.com/freelancing-notes.html
3. https://mtlynch.io/tinypilot-redesign/
4. [Project management triangle - Wikipedia](https://en.wikipedia.org/wiki/Project_management_triangle)
