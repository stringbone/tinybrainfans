---
title: Best Practices
description:
---

## Don't Work With Psychopaths

> I saw a tweet today, asking “If you could tell one piece of advice to yourself at 21, what would it be?” and without hesitation I thought, don’t work with psychopaths. You’d think you could handle it but you’d be wrong. I always thought I could, and I was wrong. You can’t outsmart a pathology.[1]

## Diary and Delta File

This is a two parter but I keep them together because it's easier.

### Diary

Keep a diary of what you do every day. This has a ton of benefits:

- Keeping track of what you did most recently so you can pick it up later
- Context of your work and why you did it
- Accomplishments, work done, and kudos from coworkers/clients for promotion/job searches (e.g. {{STAR Stories}})
- Real-time rubber ducking problems via conversation with yourself
- Questions that you don't have time to answer now but could benefit you later
- Links to cover your ass if shit hits the fan and the blame falls on you for something that was done

### Delta File

Maintain a written document, for your eyes only, where you capture observed behaviors (and consequences) that you find objectionable in yourself and others. I find this useful for keeping yourself sane in cases of being gaslit by your employer, as well as documenting behavior that _should_ be documented for future reference. You never know when you will need these details and they could 100% save your ass someday.

> The purpose is to understand, change or act differently should you find yourself in the same position. It allows you to reflect over time on actions that you find problematic; sometimes you learn that you were naïve and when presented in the same situation you then understand why a person acted in a certain way, other times you can make a different choice and rise to the occasion — it helps you prepare for that moment.
>
> A short-term side benefit of the Delta File, is how it’s a great way to let go of tolerable crap you have to deal with. When you see something objectionable and there is nothing you or anyone can do about it, just writing it down and acknowledging it’s “a bad thing” can help you handle the emotional burden.

## Regular Self-Assessment

### Internal

This is super useful for maintaining an up-to-date set of useful information in your current job. If you are keeping a regular running list of what you have done every day, then a regular distillation of that can help you keep track of your accomplishments and your trajectory within your workplace. I do this on different scales: weekly, monthly, and quarterly.

For each one, I throw all my accomplishments from the more-regular assessment (weekly I look at last 7 days in diary, monthly I look at last month's weeklies, etc.) together and consolidate them into the largest possible accomplishment for a given project. This helps me keep me on track for knowing where I am in terms of larger scale goals, but in the meta of the job (promotion, etc.) and the work itself.

When it comes time for promotion, to move on to a different company, or whatever, you will have a lot of really useful context for demonstrating your ability on different levels of granularity.

### External

When you are writing your assessment for _others_, you need to go about things a bit differently. Most of this can be summarized as "know your audience".

- **Impact**: What impact did your work have? Make sure it is obvious and easy to find if the reader skims (they will).
- **Quantify**: "Worked on a huge problem for a long time that required working with a lot of teams to deliver a new project." What is big? What is a lot? "Worked on a problem spanning three major code bases over six months, collaborating with the frontend platform teams at Foo, Bar, and Baz to develop and deliver the Quasar module."
- **Give context**: If you minimized the time it took to get the Foo into the Bar module, tell them why minimizing the time is good. Maybe there is a metric directly related to the time spent there and the money that gets saved.
- **Avoid jargon**: Generally, you want to write for around a sixth-grade reading level. There are scenarios where you have to write with more complexity due to the nature of the work being done, but assume that your superiors are not well versed in the details. Let your mom read it and see if they can get the gist. Use [Hemingway](https://stringbone.codeberg.page/hemingway-standalone/) to proof your writing.
- **Demonstrate desirable skill/behaviors**: Certain promotions are looking for a demonstration of behavioral change or skill proficiency. If it is called out in the documentation related to the new role, use that language _exactly_.
- **Keep it short**: Show what you did and how you did it. Your assessment should be easy to read in that it should not overwhelm the reader by length. If they see a tome of text, they will likely not read it all, depending on the circumstance.
- **Front load it**: Summarizing of a lot of the preceding points, start broad and slowly zero in on specifics _if necessary_. If people skim, they will understand. If someone wnats to know more, they can dive in. But if you put the most important stuff in the middle, it will get lost.

## References

1. [Don’t work with psychopath](https://scribe.rip/@livlab/dont-work-with-psychopaths-b1a848914455)
2. [The Delta File | Manager Tool](https://www.manager-tools.com/2009/08/delta-file)
3. [Your resume probably won't be seen by a human (and how to fix that)](https://gomakethings.com/your-resume-probably-wont-be-seen-by-a-human-and-how-to-fix-that/)
