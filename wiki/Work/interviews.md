---
title: Job Interviews
description:
---

What are **you** trying to get from this position? What do **they** want from you in taking this position?

## Show Confidence

> Hiring managers want someone who has done the same job somewhere else OR someone who can ramp up fast. They also want fresh ideas. Here’s how to deliver on all of these.
>
> * Review the job posting (if you have it).
> * Write down which things you have experience with and how you’ve been successful with each.
> * Then, write down which things you don’t have experience with and what your plan is to fill those gaps, learn quickly, and start delivering.
> * Last, think of a situation where you shared and/or implemented a solution or idea. What problem did it solve? What was the impact? How did you execute?
>
> Your goal during an interview is to give the interviewer confidence. The confidence to hire you.

## Questions to Ask

* What is your favorite part about this job?
* What are the biggest challenges facing the team right now?
* What is a typical day for a person in this position?
* What is the work environment and company culture like?
* What are the goals for the company? Short term and long term
* How is performance measured?
* What are the company policies for learning and development?
* Who was your best hire and why?
* How do you approach the ongoing education of your employees?
* Do you have a regular process in place to gather feedback from your employees? What's an example of something that you have done based on that feedback?

## The Joel Test[3]

> I've come up with my own, highly irresponsible, sloppy test to rate the quality of a software team. The great part about it is that it takes about 3 minutes. [I]t’s easy to get a quick yes or no to each question. You don’t have to figure out lines-of-code-per-day or average-bugs-per-inflection-point. Give your team 1 point for each “yes” answer. A score of 12 is perfect, 11 is tolerable, but 10 or lower and you've got serious problems. The truth is that most software organizations are running with a score of 2 or 3, and they need serious help, because companies like Microsoft run at 12 full-time.
> - Joel Spolsky

* Do you use source control?
* Can you make a build in one step?
* Do you make daily builds?
* Do you have a bug database?
* Do you fix bugs before writing new code?
* Do you have an up-to-date schedule?
* Do you have a spec?
* Do programmers have quiet working conditions?
* Do you use the best tools money can buy?
* Do you have testers?
* Do new candidates write code during their interview?
* Do you do hallway usability testing?

## References

1. https://www.codecademy.com/resources/blog/software-developer-interview-questions-and-answers/
2. https://github.com/Twipped/InterviewThis
3. https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code/
4. https://www.linkedin.com/posts/josh-fullmer_if-you-have-an-interview-coming-up-read-activity-6977982774162980865-ATHm
5. https://www.nowhiteboard.org/
6. https://www.interviewbit.com/technical-interview-questions/

