---
title: STAR Stories
description: STAR stories are good ways to concentrate your valuable experiences and skills into something powerful for interviews.
---

STAR stories are good ways to concentrate your valuable experiences and skills into something powerful for interviews.

* Situation - What the context is surrounding this story
* Task - What the task is that needed to be completed
* Action - What actions were taken to complete this task
* Result - What resulted from your actions

Important elements of a good STAR story are that it is **short** (shorter than you think it should be!) and is **easy to understand**. As a challenge, try and reduce your stories down to one sentence per section; then if you expand in an interview (you will), it will be just right.

We also want the impact of our difference to be easily understood, so using things like productivity/production metrics (percentages, "x times" growth) or monetary metrics are key to driving the point home about why what you did matters. In general, people love numbers because they are hard-edged and unambiguous, so aim for that. If no numbers are available, make it as tangible a result as possible!

All this being said, **be a human**. Add life to your stories, make them interesting. Still be you and be proud of who you are and what makes you *you*.

## Common Attributes

Attributes that underlie most soft-skills questions are:

1. Passion
2. Communication
3. Balancing Self-Motivation and Loyalty (looking for a good balance)
4. Confidence and Accurate Self-Analysis
5. Problem-Solving Attributes
6. Leadership
7. Organization

Having a couple of STAR stories that apply for each of these will allow you to kind of bend them to fit most questions.

## Common Questions

These are used in response to common questions, so having STAR stories that can cover those common interview questions is crucial:

* Talk to me about one of your projects. What was your biggest accomplishment with it and how could it be improved?
* ?emoctuo eht saw tahW ?ssecorp siht morf nrael uoy did tahW ?egnahc rof hsup ot detacovda uoy evah nehW
* How long have you been doing/working on (trade or role)?
* What strengths or skills set you apart?
* Tell me about a time you had a conflict with a teammate. How did you resolve it?
* What is your biggest weakness or area that needs improvement?
* Why would you be an asset to our team?
* Tell me about a time you made a mistake and how you handled it.
* Where do you see yourself in 5 years?
* Tell me about yourself.
* Take me through your resume.
* Why should we hire you?
* How would you rate yourself on (technical or soft skill)?
* How do you handle stress and pressure?
* What type of work environment do you prefer?
* What do you think makes a good (role or title)?
* What motivates you?
* If you knew your boss was 100% wrong about something, how would you handle it?
* How do you evaluate success?
* What qualities do you look for in a boss?
* Tell me about a time you had to manage multiple priorities?
* Tell me about a time when you were given an assignment with an impossible deadline.
* Tell me about an example of your leadership or project management experience.
* Describe a time in which you were able to use your analytical skills to solve a business problem.
* Have you ever had to work with a difficult colleague?
* Do you have any experience with solving a complex problem for a client?

## Example from CMU[2]

*Describe a time you had to use your leadership skills unexpectedly.*

### Situation

During my first year with the company, I was given the project manager role for a critical client account after the long-term project manager suddenly became ill.

### Task

My first priority was to win the confidence of both my team and the client by creating a roadmap that we could all follow to the successful completion of the project. Once agreed, I needed to make sure that everyone followed the specified steps required to meet the client’s objectives.

### Action

I began by calling a team meeting to develop a list of project deliverables along with a time and responsibility schedule. I identified, reviewed and assigned each task to a responsible team member to ensure accountability. These steps gained broad team buy-in and ensured that we had the right people and resources needed/allocated to meet each milestone.

Next, I reviewed my proposed plan of action with my manager. Following his approval, I met with the client to make sure that were addressing all of his concerns. I returned to my team where we made some final adjustments based on client feedback. All of this took place within one week of my taking the lead on the project.

We quickly launched the execution of the project and moved forward. Throughout the 3 month execution phase, I held weakly meetings with team members to keep everyone focused and motivated and to further adjust resources if needed. These meetings were complemented by bi-weekly client meetings to assure his ongoing satisfaction. In the end, the project was delivered on time and the client was completely satisfied.

### Result

So, because of my leadership skills, I was able to step into a challenging situation and lead my team to a final result that met the high standards of both my company and our key client.

## References

1. https://careercenter.lehigh.edu/node/145
2. https://www.cmu.edu/tepper/alumni/assets/docs/star-story.pdf

