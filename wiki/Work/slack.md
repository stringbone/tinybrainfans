---
title: Slack
description: Slack is a communications software for large teams.
---

## Make Slack Not So Noisy

Slack is very noisy unless you set certain settings a specific way. This is how I keep it not so anxiety-inducing:

* General
    * **NO SLACK ON YOUR PHONE**. If you are on call, then make sure you're getting paid for it.
    * Leave channels that are not your job (except fun ones, obvs)
    * Mute channels that are not important except for emergencies
* Preferences
    * Notifications
        * DM's, mentions, and keywords
        * Notify me on thread replies
        * 9am to 5pm
        * Notification sound for messages, huddles, and DMs
        * Show badge
    * Sidebar
        * Always show: DMs, mentions, drafts & sent, saved items
        * Unread only
        * Don't show pictures next to DMs
    * Themes
        * Sync to OS
        * Aubergine theme
    * Messages
        * Compact
        * Full and display names
        * Don't display who is typing
        * Show times with 24-hour clock
        * Don't convert my emoticons into emojis
        * Show images and files uploaded to slack
        * Show linked images and files
        * Show text previews
    * Language & Region
        * Set time automatically
        * Enable spellcheck
    * Accessibility
        * Underline links to websites
        * Automatically play gifs
    * Maek as read
        * Start me at newest message, but leave unseen unread
        * No prompt to confirm if I mark as read
    * Audio & Video
        * When in call, set status to "On a call"
        * When in huddle, set status to "On a huddle"
        * Send a warning if starting a huddle with more than 150 members
        * Blur background when in a video huddle
        * Allow shortcut to mute
        * Automatically leave if screen saver starts or comp locks
    * Privacy and visibility
        * Slack Connect to no one
        * Share only within the org
    * Advanced
        * Enter starts a newline, Cmd+Enter sends a message
        * Don't send channel suggestions or surveys
        * Disable hardware acceleration

## References

1. https://www.getro.com/blog/make-slack-a-less-noisy-place-to-work/

