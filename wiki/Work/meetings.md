---
title: Meetings
description: You should have as few as possible.
---

## How to have less meetings

### Preemptive communication

Communicate to stakeholders early and often. This should include:

- what you are working on (and why if necessary),
- what you have done,
- what is blocking and if you need help.

This is what most meetings are anyway.

### Documentation

Though it can be, and often is, helpful for your own career, you should assume documentation is for others to read. This will help you write it in a way that doesn't have assumed context. There are different types of documentation:

- Logging your work, progress, and/or status
- Scribing what happened
- Instructions

Logs are part of preemptive communication. If you keep good logs, you can send those living documents to your stakeholders and they will know at a glance how things are going. THis is similar to how {{Carmack plan files|The Carmack plan}} were developed.

Scribing codifies when and why certain decisions were made. This is useful for covering your ass, but also extremely useful for explaining why decisions were made in the future. Having this record of who agreed on what and why is invaluable for people who question why things are done a given why. Especially with things like developing software, where decisions are sometimes made based on bugs or random conversations, documenting _why_ is essential for those considerations making it into future versions.

Instructions do many things. They can help you better understand a process, clarify missing steps, document missing but necessary information, find unforeseen issues, save time trying to remember how to do it in the future, help your colleagues if you are away, etc. There is a reason why humans have been writing stuff down forever.

## References

1. [A high documentation and low meeting culture](https://www.tremendous.com/blog/the-perks-of-a-high-documentation-low-meeting-work-culture/)
