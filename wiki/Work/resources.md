---
title: Tech Job Resources
---

## References

1. https://github.com/hng/tech-coops
2. https://www.linkedin.com/posts/nicholascmartin_internationaldevelopment-humanitarianaid-activity-6933070598277394434-rAfj?utm_source=linkedin_share&utm_medium=member_desktop_web
3. https://www.inkl.com/news/the-meltdown-at-basecamp-shows-even-small-tech-firms-are-sociopathic?share=GNlYdXtokGo
