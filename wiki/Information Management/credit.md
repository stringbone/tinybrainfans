---
title: Credit Monitoring
description: Monitoring your credit score and protecting your credit is easy and free.
---

## Credit Protection

Monitoring and protecting your credit activity is a good way to help ensure (but not guarantee) you don't end up with credit cards or accounts taken out in your name, or similar scams. There are two types of protections that you can take, both needing to be done with the big three credit bureaus: Experian, Transunion, and Equifax.

Unfortunately, a good reason why you should do these regularly is that agencies like the big three have data breaches[5] and generally don't seem to care too much about security in a meaningful way. So bad actors exist, but these guys aren't helping either, so adding the extra protection can at least make you a less easy target.

### Fraud Alerts

A fraud alert "is free and notifies creditors to take extra steps to verify your identity before extending credit"[2]. This needs to be redone every year, but is largely painless, save for filling out annoying forms.

There is also an extended fraud alert, that is much more dire and requires certain criteria to be placed before it can be used.

> It requires a copy of a valid police or law enforcement agency report, or a Federal Trade Commission Identity Theft Report, and lasts for 7 years. With an extended fraud alert, a lender or creditor is required to verify your identity in person or by phone at a number you provide before opening new accounts or making changes to existing accounts.[3]

You can sign up for fraud alerts at the links below[1-3].

### Credit Freezes[4]

Credit freezes are a last resort to totally lock down your credit. You won't be able to open **anything** with your credit, so especially if you are considering getting a house or opening a new credit line, be very careful. It's not easy to just open and close.

## Credit Scores

You can get a free credit report from the federally run site[6]. Other places let you check for a fee, but I generally try and stay away from those because I sense too many strings attached.

## References

1. https://www.experian.com/fraud/center.html
2. https://www.transunion.com/fraud-alerts
3. https://www.equifax.com/personal/credit-report-services/credit-fraud-alerts/
4. https://www.equifax.com/personal/credit-report-services/credit-freeze/
5. https://en.wikipedia.org/wiki/2017_Equifax_data_breach
6. https://www.annualcreditreport.com/index.action

