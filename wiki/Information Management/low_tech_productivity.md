---
title: Low-/No-Tech Productivity
description: Low-/No-tech productivity tools are ways to distance yourself from technology when it is not helpful and possibly hurtful.
---

Low-tech productivity tools are ways to distance yourself from technology when it is not helpful and possibly hurtful.

## No-Tech

No-tech meaning no computers involved in the usage of the actual tool.

### Hipster PDA

A hipster PDA consists of:

-   A stack of index cards (traditionally) or scrap paper cut to a small size
-   A binder clip
-   A writing tool

Put them together and you have a portable tool for solving problems, taking notes, todo lists, whatever.

### PocketMod

A pocketmod is a small booklet made out of a standard letter size or A4 paper. Either pre-printed or just blank, this can be useful for reference material, note taking, or simple templated tools (e.g. a financial ledger, {{paper computers|Paper Computing}}, tabletop RPG's).

I used {{Pocketmods|pocketmod}} as a ledger for all my debits and credits when I had my own spreadsheet budgeting system pre-[YNAB](https://youneedabudget.com/).

You can find [a PocketMod SVG template here (1kb)](pmtemplt.svg). Or you can use this version from zserge[18].

This can be folded whatever way works for you. Easiest way is to just fold the width in half, and then fold the length in quarters from the outside in[17]. Then you essentially have a book that opens into a gatefold, that opens into a four-page inside.

The PocketMod way[4] is to make these same folds and crease them, to then cut along the spine where the four inner rectangles meet (the page should still be one continuous piece but now with a single cut). This lets you fold the length in half and then grab both ends and push together, making for something much more akin to a booklet. If this is confusing (sorry), check out their website[4] for a better how-to.

### Noguchi Filing System[13]

The short version of this is just use a LRU cache with your files or folders. But more verbosely:

1. Get manila folders and cut off the top end with the flap, so that any papers inside stick out of the top.
1. Write a one to two word description on the long edge, with the papers sticking out the right hand side.
1. Put all your folders in the file cabinet.

You are done. When you need a folder, search for it. When you are done, put it at the very front. The folders you need the most often will always be easily accessible, where the ones you need least will take longer.

## Low-Tech

Low-tech meaning eschewing complicated, fragile, or problematic systems (e.g. Adobe, Evernote) for something simpler[7].

### {{Plaintext}} Everything

A plain TXT file (or Markdown) can easily accomplish the 80/20 of task management, calendars, record keeping, or whatever else, and there are many many examples[5,6,14]. Advantages are it is universal, completely customizable, {{easily searchable|grep}}, cross platform, cloud synced through something like Dropbox, and nimble. Disadvantages are not a "sexy" GUI, potentially takes more maintenance, requires some planning.

### {{Shell}} Scripts

This takes a bit more investment and planning, but learning how to make scripts in your {{shell}} ({{bash}}, {{zsh}}, etc.) can automate otherwise time consuming or annoying tasks[11]. A UNIX terminal/shell is available on pretty much every computer these days, so these are also immensely portable. For example:

-   to-do list management[8,9]
-   convert youtube captions to an ebook[10]
-   ocean noise generator[12]

### {{Hammock Driven Development}}

This is more of a method, but I think it should get a mention here. Big point of this method is that you should be spending most of your time solving problems and most of that time should be **away** from the computer.

## References

1. http://www.43folders.com/2004/09/03/introducing-the-hipster-pda
2. https://netninja.com/hipsterpda/
3. https://en.wikipedia.org/wiki/Hipster_PDA
4. https://pocketmod.com/
5. https://plaintextproject.online/links.html
6. https://danlucraft.com/blog/2008/04/plain-text-organizer/
7. https://kairos.technorhetoric.net/20.2/inventio/stolley/
8. http://www.blossomassociates.org/Plan/
9. https://github.com/todotxt/todo.txt-cli
10. https://github.com/milofultz/sub2pub
11. https://github.com/alebcay/awesome-shell#command-line-productivity
12. https://merveilles.town/@exquisitecorp/107359264170996361
13. https://fernandogros.com/the-noguchi-filing-system/
14. https://sive.rs/plaintext
15. https://useplaintext.email/
16. https://archive.org/about/offline-archive/
17. [Pocketfold](https://3skulls.itch.io/pocketfold)
18. [zserge - zine](https://github.com/zserge/zine)
