---
title: Anki
description: Anki is a spaced repetition language learning tool.
---

Anki is a spaced repetition {{language}} learning tool.

## Getting Started

Download Anki from their [homepage](https://apps.ankiweb.net/) for your OS. Mobile users will need to install it on their non-mobile device first before using the Ankiweb portal. You can also use their mobile apps, but to get started, that's simple.

Go to the [AnkiWeb portal](https://ankiweb.net/decks/), log in, and look up the shared decks to try some out. You'll probably find anything you are looking for, but for more specifics, you will want to make your own deck.

## Make a Deck

Use "Create a Deck" from the bottom of the main window and name it what you want.

### Making Cards

For a basic card, add what you want the front to say and then what the back will say and finish.

If you want _both_ sides of the card to be used to flash[3] (e.g. shown Spanish word to guess English word, and vice versa), use the "Basic (and reversed)" card type. If your card type is already "Basic", you can change the note type of your cards in the card browser.

## References

1. https://apps.ankiweb.net/
2. https://ankiweb.net/decks/
3. [Making flipped cards](https://inv.tux.pizza/watch?v=DnbKwHEQ1mA)
4. [Anki FAQ/Forum](https://faqs.ankiweb.net/getting-help.html)
