---
title: Knitting
description: Knitting is creating stuff out of fiber, like yarn.
---

Knitting is creating stuff out of fiber, like yarn. You can also {{use|reuse}} it for sewing, if you need it.

## References

1. [How to knit for beginners (video)](https://www.youtube.com/watch?v=p_R1UDsNOMk)
2. [How to knit for beginners (webpage)](https://nimble-needles.com/tutorials/how-to-knit-for-beginners/)
3. [Single cast-on knitting method](https://www.simple-knitting.com/single-cast-on.html)
