---
title: Bivy Sack
description: A bivy sack is a waterproof cocoon for your sleeping bag, a substitute for a tent.
---

The humble bivy sack is just that: a sack. It is probably most useful as an emergency shelter, though some masochists use it as their main camping shelter, maybe along with a tarp.

## Supplies

* Tyvek (3ft or 10ft width)
* Carpet tape (seems like 2" is standard, so probably that?)
* Velcro strips (same width as carpet tape)
* Scissors

## Steps

1. Measure Tyvek for the length/width f your body/sleeping bag.
    1. For 3ft Tyvek, use the 3ft as width and measure the length of your body plus a foot or so. I found 7ft for my ~5'9" length was good. Cut at that length, and then repeat for another piece.
    2. For 10ft Tyvek, use the 10ft as the length, and make a kind of hot dog bun shape to go over your body. Add as much width as you see necessary, though 3-3.5 feet is probably fine. Cut so when it overlaps and meets at the edges, it is the proper width. You can cut down the length to whatever you want, though I found ~7 foot was good for me as a ~5'9" person.
2. Prepare the Tyvek. It is LOUD at this stage and this step will make it much less brutally loud. Essentially, you want to twist and crumple and squeeze the Tyvek as much as possible. This can be done by hand, or you put it into the washing machine. Crumple it up so it fits in, then run with no detergent, cold water, and a short gentle cycle. Pull it out and then put it into the dryer on low-no heat with some towels and let it tumble for a bit.
3. Put carpet tape down on one piece of Tyvek on the bottom edge (by the feet), the top edge, completely on one side, and halfway up the other side. On the incomplete side by the top, put a small ~5" strip down from the top edge towards the feet. This will eventually become the hood once it's all connected.
4. Connect the two pieces of Tyvek together, bringing all the edges together, connecting all the carpet tape to the opposite piece of the Tyvek. Some people like having the logo inside or out, it does not matter for breathability.
5. Let the tape set for a while to ensure it is fully adhered. Doesn't need to be too long, but I dunno, 15-20m or so? Ensure it's still well together during that time.
6. Optional step: on the corners, put a small piece of carpet tape just towards the center of the Tyvek, and then fold the corner in so the corner is stuck pointing towards the rest of the Tyvek. This will make it so there isn't a funny looking hard edged corner.
7. Turn it all inside out by pulling the bottom out through the open hole. Your seams should inside now.
8. Put your Velcro strips along the incomplete edge from where the tape ends on both sides. You should now have an unbroken side of Tyvek => Velcro => Tyvek. I found that the Velcro strips didn't really stick, so I ended up using carpet tape to stick them to the Tyvek, as well.

## References

1. https://yewtu.be/watch?v=o_UHoJwLiWs
2. https://www.survivalistboards.com/threads/do-tyvek-bivy-bags-actually-work.341632/
3. https://outdoorloops.com/how-to-make-a-bivy-sack/
4. https://thetrek.co/the-thru-hikers-guide-to-tyvek/
5. https://yewtu.be/watch?v=DHuggFSfyAo
6. https://www.theultralighthiker.com/2018/03/02/tyvek/
7. https://www.instructables.com/Lightweight-Minimalist-Bug-Net-for-Bivvy-Bag/
8. https://www.stitchbackgear.com/articles/easy-to-make-bug-bivy
9. https://www.briangreen.net/bbb/2011/07/tarp-bivy-combo-your-sub-1lb-shelter.html
10. https://www.cicerone.co.uk/the-book-of-the-bivvy-third
11. https://www.instructables.com/Lightweight-Minimalist-Bug-Net-for-Bivvy-Bag/

