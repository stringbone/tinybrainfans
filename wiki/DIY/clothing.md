---
title: Clothing
description: Making clothing is something I want to do, for fun but also to gain independence from gross retailers.
---

Making clothing is something I want to do, for fun but also to gain independence from gross retailers.

## {{Reuse}}

Don't throw away your old clothes! There are many options before they end up in the garbage.

-   If they're still wearable, take them to a thrift store.
-   If they have holes or tears, you can {{mend|Mending}} them.
-   If they have too many so they can't be repaired, save the intact bits for making {{patches|Sewing}}. Particularly things like denim are great for patches.
-   If all else fails, you can use them for rags.

## References

1. [Plans for medieval tunics](http://forest.gen.nz/Medieval/articles/Tunics/TUNICS.HTML), and [more tunics plans as well](https://web.archive.org/web/20041204160332/http://home1.gte.net/kmvogt/ktunic1.html)
2. [Made a jacket out of coffee bean sacks](https://old.reddit.com/r/sewing/comments/wdjdlr/the_best_smelling_jacket_i_made_a_bomber_jacket/)
3. [Zero waste patterns](https://www.birgittahelmersson.com/products/zero-waste-patterns-book)
