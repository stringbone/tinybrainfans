---
title: Miscellaneous DIY Resources
description:
---

Different ideas for making and {{reusing|reuse}} things.

-   http://otherthingsmuseum.com/collection
-   [Heat people not spaces](https://www.lowtechmagazine.com/2015/02/heating-people-not-spaces.html) / [Kotatsu](https://en.wikipedia.org/wiki/Kotatsu)
-   https://seasonedcitizenprepper.com/preparedness-downloads/
-   https://old.reddit.com/r/AskReddit/comments/444cnh/what_are_some_free_services_on_the_internet/cznc9la/
-   https://maudbausier.com/ETCP-chair.html
-   https://www.builditsolar.com/
-   https://solar.lowtechmagazine.com/2022/03/how-to-build-bike-generator.html
-   {{Use|reuse}} old paper to [make paper](https://www.paperslurry.com/2014/05/19/how-to-make-handmade-paper-from-recycled-materials/)
-   [Dry Laundry Detergent](https://www.thespruce.com/how-to-make-your-own-laundry-detergent-1387952)
-   [Liquid Laundry Detergent using baking soda and washing soda](https://homesteading.com/diy-liquid-laundry-soap/) and [one using salt and baking soda](https://www.apartmenttherapy.com/make-your-own-dr-bronners-laun-138151)
-   [Evaporative cooling](https://solar.lowtechmagazine.com/2012/04/how-to-keep-beverages-cool-outside-the-refrigerator.html)
-   [Hair Rinses](https://yewtu.be/watch?v=k_bVVwxdwxA)
-   https://www.motherearthnews.com/sustainable-living/nature-and-environment/conifer-tree-pitch-fire-starter-zmaz85ndzgoe/
