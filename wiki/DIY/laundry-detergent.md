---
title: Laundry Detergent
description:
---

## Liquid

This recipe is from Apartment Therapy[1]. One load of laundry uses 1/4 cup of detergent.

### Ingredients

- 1 cup **castile liquid {{soap}}**
- 1 cup **{{baking soda}}**
- 2 cups **warm {{water}}**
- 1/3 cup **{{salt}}**
- Enough **{{water}}** to fill the rest of the container (you'll want some more water)

### Materials

- Bowl or bucket for mixing that can hold at least one gallon and some kind of marking at the one gallon mark
- Utensil for mixing
- Jar or bottle for storage

### Instructions

1. Mix the **warm water** with the **salt** and **baking soda** until fully combined.
2. Add the **liquid soap** and fill the remainder of the container (up to one gallon) with water.

## References

1. https://www.apartmenttherapy.com/make-your-own-dr-bronners-laun-138151

