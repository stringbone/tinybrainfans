---
title: Knots
description: Knots.
---

## References

1. http://eight45.net/knots.html
2. [The Ashley Book Of Knots](https://archive.org/details/ashley-book-of-knots-by-clifford-w-ashley)
3. [5 knots, learned progressively](https://www.youtube.com/watch?v=2RTyVjZmKp0)
