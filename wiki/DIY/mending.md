---
title: Mending
description: Mending clothes is a great way to extend the life of your clothes. And you can make it artful, too!
---

Mending clothes is a great way to extend the life of your clothes. I prefer making them visible[1] and very clashing with the primary clothing piece, as it looks cool.

## Simple

For a basic hole in something low impact, like a shirt, the side of a sock, a purse, etc., you can do something like this.

### Materials

-   Heavy duty thread, synthetic/acrylic yarn, or some other thicker thread. (You want to make sure this thread is somewhat close to the size of the fabric's current threads. Otherwise, you may make more holes or add too much strain to the fabric.)
-   Scissors
-   Needle(s) (Sashiko needles are best, as they can work for most clothing and thread thicknesses.)
-   Scrap of fabric or patch to cover the hole (optional, usually use this if the hole is larger).

### Techniques

For tears or rips, you can essentially "suture" it up with a {{ladder stitch|sewing}}, starting at one end and alternating on each side of the tear until the tear is closed.

For smaller holes, I "weave" a patch out of thread, starting with multiple rows, and following with over/under for each column.

For bigger holes, I follow the sashiko method[2] with a patch.

## References

1. [Visible mending - Wikipedia](https://en.wikipedia.org/wiki/Visible_mending)
2. [Sashiko - Wikipedia](https://en.wikipedia.org/wiki/Sashiko)
3. {{Reuse}}
