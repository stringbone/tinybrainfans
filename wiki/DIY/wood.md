---
title: Wood
---

Ideas of how to {{reuse}} scrap wood:

-   [Plyo box](https://www.youtube.com/watch?v=KLKbIkq_t50)
-   [Viking chair](https://www.instructables.com/One-Board-Minimalist-Chair/)
-   [Bench from pallets](https://xuv.be/uH-bench-open-source-public-bench.html)

Generally you shouldn't use pallet wood because it really sucks and is filled with nails and screw ends. It's good for bulky stuff but not worth too much modification.
