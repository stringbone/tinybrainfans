---
title: Cordage
description: Cordage is created out of plant matter and can be used to make rope, clothing, tools, etc.
---

Cordage is {{created|reuse}} out of plant matter and can be used to make rope, clothing, tools, etc.

## References

1. [From blackberry brambles](https://yewtu.be/watch?v=3SJdWjSEN6g)
2. [Simple Cordage Instructions](https://yewtu.be/watch?v=62PvfIR0xDg)
3. [From nettles](https://yewtu.be/watch?app=desktop&v=Tss2m6SbjTA)
