---
title: Sewing
description: Sewing is a simple, cheap, and useful skill to have for creating and fixing clothes and textiles.
---

Sewing is a simple, cheap, and useful skill to have for creating and fixing {{clothes|clothing}} and textiles. I use it quite frequently when doing {{visible mending|mending}}, hemming frayed shirt or jean bottoms, patching clothes, or adjusting clothes to better fit me.

## Getting started

You can get started by getting a basic travel sewing kit from any drug store, pharmacy, or department store. Or make your own with an Altoids tin and a trip to your local quilting shop[6] (I'd just ask them, everyone who works at a place like that rules). You should have at minimum:

-   Needles
-   Pins
-   Thread
-   Thimble
-   Scissors

If you are looking to do any projects on tougher fabric or with really big thread (e.g. yarn), I recommend getting sashiko[5] needles as they have larger eyes and are made for mending stuff like denim.

## Stitches and how I use them

-   Straight/running stitch[4] - adding a patch.
-   Backstitch[5] - same as straight stitch, but much stronger and uses more thread.
-   Whipstitch[1] - smaller hems, stopping fraying on patches.
-   Blanket stitch[2] - hemming edges, joining multiple pieces of fabric together at the edge.
-   Ladder stitch[3] - closing seams in fabric, fixing rips/tears.

## References

1. [Whipstitch](https://en.wikipedia.org/wiki/Whip_stitch)
2. [Blanket stitch](https://www.youtube-nocookie.com/embed/S9zegUYdPmg)
3. [Ladder stitch](https://en.wikipedia.org/wiki/Ladder_stitch)
4. [Straight/running stitch](https://en.wikipedia.org/wiki/Straight_stitch)
5. [Sashiko](https://en.wikipedia.org/wiki/Sashiko)
6. [Altoids tin sewing kit](https://www.instructables.com/altiods-sewing-kit/)
