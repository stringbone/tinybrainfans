---
title: Mobile Home Insulation
description:
---

## Types

* Fiberglass batts/rolls: relatively cheap, can DIY (nail gun I think)
* Blown in fiberglass: fast, needs professionals, kinda pricey, otherwise same as fiberglass batts/rolls
* Spray foam: fast, needs pros, triple cost of fiberglass, kinda sick, very good insulator and water resistant and pest resistant
* Rock wool: very heavy, fireproof, soundproof sorta, seems costly

## R-values

The higher it is, the better it will insulate. You can stack them together linearly (R15 with R15 on top = R30).

> If your home has a vaulted ceiling, the roof insulation will have a maximum value of R30. A layer of quality R22 insulation is used in the subfloor and R11 in the walls.[1]

In PNW, seems that these are general values to use[2]:

* Attic: R49-60
* Walls:
    * 2x4: R13-15
    * 2x6: R19-21
* Floors: R25-30
* Crawlspace: R25-30

## References

1. [Insulation in Manufactured Homes | Clayton Studio](https://www.claytonhomes.com/studio/how-well-are-manufactured-homes-insulated/)
2. [All About Insulation R-Values - The Home Depot](https://www.homedepot.com/c/ab/insulation-r-values/9ba683603be9fa5395fab9091a9131f)
3. [Spray Foam Insulation  | Family Handyman](https://www.familyhandyman.com/article/spray-foam-insulation/)

