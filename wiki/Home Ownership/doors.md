---
name: Doors
description: Doors are a pain in the ass.
---

Install prehung doors. Doing the rest yourself is a pain in the ass.

## What you need to know[1]

- The clearance between the bottom of the door and the finish floor should be 3/8-inch or less.

### Glossary

- Rough opening: measurement from stud to stud (facing the opening, left/right by up/down).
- Jamb depth: using the wall adjacent to the door, the depth from front to back, including plaster or drywall (e.g. drywall + stud + drywall)
- Swing direction/handedness: easy to confuse, figure out which side of the wall it should be on and which direction it should swing.

## References

1. [Installing a prehung door](https://www.thisoldhouse.com/doors/21016350/how-to-install-a-prehung-door)
