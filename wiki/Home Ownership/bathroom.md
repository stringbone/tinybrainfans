---
title: Bathroom Remodeling
description: This is a bunch of stuff I need/learned for installing elements of a bathroom.
---

In general, I defer to plumbers to do plumbing, especially when it comes to sewer drainage. I'd rather spend a few bucks now and trust that poop won't one day pool underneath my house based on something I overlooked.

## References

1. [Installing a tub spout on a copper pipe](https://www.youtube.com/watch?v=fmPeRkzSQY0)
2. [Tub shower valve install mistakes and how to avoid them](https://www.youtube.com/watch?v=kvtl6nZhLSI)
3. [Installing a standard tub/shower rough valve](https://www.youtube.com/watch?v=C-UR8nJD1Bk)
