---
title: Weatherproofing
description: Weatherproofing your home is prophylactic work.
---

## Grading

You want the ground around your house to have a pitch of one inch per foot from your foundation to four feet out. Use soil up against your house to get the level up to four inches above the average ground level, then use a level to check. If you have your level at one end touching your house and the soil reaches it, and four feet out it is four inches above the ground, and the level is level, you're good.

## Skirting for Mobile Homes

### Types

* Vinyl: standard, cheap, needs repair and replacement often but good for budget
* Wood: Looks good but needs constant upkeep, can rot, pests
* Metal: pricier but is metal, so holds up well
* Faux Stone/Brick: Synthetic, lasts longer than vinyl and still fairly inexpensive, can install yourself
* Brick/Stone: Needs professional, very pricey, looks cool

## References

1. [How to Properly Grade Around Your Home](https://yewtu.be/watch?v=5hYIda7tWqA)
2. [The Ultimate Mobile Home Skirting Guide | Mobile Home Living](https://mobilehomeliving.org/mobile-home-skirting/)
3. [How We Made Vinyl DIY RV Skirting for Winter (for only $200!)](https://rvinspiration.com/cold-weather-rving/cheap-diy-rv-skirting/)
4. [Reused Billboard Vinyl: 13-16oz, White, With Pipe Sleeves - billboardtarps.com](https://billboardtarps.com/shop/billboard-vinyl/reused-billboard-vinyl-13-16oz-black-with-pipe-sleeves-copy/)

