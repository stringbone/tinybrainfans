---
title: Drywall
description: Drywall is often used in walls and ceilings.
---

## Removing Drywall

### Equipment Needed

-   Drywall saw
-   Razor knife
-   Electrical tape
-   Pry bar
-   Hammer
-   Magnet for finding screws
-   Some kind of straight edge for marking (e.g. a level)
-   PPE
    -   Goggles
    -   Particulate masks (N95 or similar)
    -   Gloves

### Safety/Protection

Before you start, make sure you don't hurt yourself or the elements around it (ceiling panels, floor boards, etc.)

1. Check for electrical (via receptacles in the wall, lights, etc.). If there is some, flip the breaker off for those fixtures to ensure your own protection.
1. Check for plumbing the same way (via sinks, etc.). This is more difficult, as there may be pipes in the wall that you don't know about, but do your best. Safe bet is to just turn off your water.
1. Remove all fixture covers, like light switch covers, socket covers, vent grills, light covers, etc.
1. If necessary, remove the fixtures themselves. Most electrical sockets and the like will have screws keeping them in and have the wiring connected using twist caps. Unscrew these and untwist the caps, covering the wire ends with electrical tape.
1. Remove any baseboards and moulding. On manufactured homes, this is everywhere; the ceiling, the floor, sometimes down the wall to cover up where the drywall panels meet. Use a prybar to get them off.
1. Use a razor knife to score multiple times along the corner where the drywall meets the ceiling. There is a paper bead that if left uncut could rip off parts of your ceiling.

### Screws

If you are lucky, someone put in your drywall using only screws and you can just unscrew them all before you get started. Finding them is the difficult part, but if you can find them, do this first.

### Using a Drywall Cutout Tool

This is essentially a router that has a fixed depth with a bit _made_ for drywall. If you are doing a big reno or have to remove a ton of ceiling and walls, I recommend buying one as it will make this whole process much faster than by hand.

1. Use a stud finder to mark where one stud is.
1. Find a spot in your drywall where you _know_ there won't be an obstacle (electric, stud, etc.). Push the drywall saw through the wall wherever you are looking to remove and cut out a small window.
1. Measure the depth of the drywall, and set the depth of your cutout tool to match it.
1. Once this is set, you can largely cut anywhere.

For walls, I had success cutting as close the edges (walls/ceilings) as possible, cutting a small window near the top where I could put my hands and pull. If you use slight amounts of pressure, it should loosen from the screws/staples and pull away as a whole sheet.

For ceilings, pulling them down can be really dangerous and they are heavy. Clear away a space on the floor for the drywall to potentially fall. I cut somewhere around a 3- or 4-foot wide piece and about 5 feet long (along the decline). Again, make a hand hold near the top and pull slightly to loosen the drywall from the staples and/or screws. It should pull away as the whole piece you cut out.

### Wall Removal

The goal here is to make a cut horizontally from one end of your wall to the other, to make removal easier.

1. Use a stud finder to mark where your studs are. This will save you from accidentally cutting into them, but also make it easy to judge where to start/stop.
1. Find a spot in your drywall where you _know_ there won't be an obstacle (electric, stud, etc.). Push the drywall saw through the wall wherever you are looking to remove and cut horizontally until you reach the stud (your angle should be slight, certainly not perpendicular to the wall; too obtuse an angle and you may cut wiring/plumbing).
1. When reaching the stud, adjust your angle until it is almost flat and cut the drywall while avoiding the stud.
1. Once past the stud, return to the original angle and continue. _Stay vigilant on avoiding electric and plumbing!_
1. When you reach the corner, your corner studs will likely have a metal bead running along it. Stop before you hit that, as it will definitely dull your blade.
1. If your pieces are up high, you will want to repeat the previous steps but vertically, to cut the pieces into manageable chunks. Make sure you don't get absolutely slapped by a huge piece of drywall coming at you.
1. With your pry bar, find a spot between the studs and put the pry bar in the cut you made. Using either that or the claw of your hammer, pull back on the dry wall to separate it from the studs. Adhesive, nails, or screws could be holding it on, and we want to remove or loosen those as best we can.
1. Once the sheet has been loosened, you should be able to use your hands to pull back on the sheet, hopefully getting the whole sheet out at once. It'll probably break off in chunks, but do your best.

### Ceiling Removal

I _highly highly_ recommend using a drywall cutout tool (see above). If you only need to do a small amount, a drywall saw should be fine. Follow the steps for wall removal, remaining aware of electrical, plumbing, etc.

## References

1. [How To Remove Drywall from a Wall](https://yewtu.be/watch?v=_SNgBjdf8d0)
2. [Cutting Out Drywall Ceiling from Roof Leak Step by Step](https://www.youtube.com/watch?v=ghlwGKezAVE)
