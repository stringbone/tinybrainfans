<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="X-UA-Compatible" content="ie=edge" /><meta name="description" content="ARIA attributes are a set of roles and attributes that define ways to make the web more accessible to people with disabilities." /><!-- Bing --><meta name="msvalidate.01" content="45CBBE1BD8265A2217DFDA630EB8F84A" /><title>Tiny Brain Fans - ARIA Attributes</title><link rel="stylesheet" href="tinystyle.css" /><link rel="stylesheet" href="prism.css" /></head><body class="theme">
<main id="main"><article id="content"><h1 id="title">ARIA Attributes</h1><p>ARIA attributes are a set of roles and attributes that define ways to make the web more <a href="accessibility.html">accessible</a> to people with disabilities. Note that ARIA does <strong>not</strong> add functionality!</p>
<blockquote>
<p>There is a saying &quot;No ARIA is better than bad ARIA.&quot; In WebAim's survey of over one million home pages, they found that Home pages with ARIA present averaged 41% more detected errors than those without ARIA. While ARIA is designed to make web pages more accessible, if used incorrectly, it can do more harm than good. -- Gerard K. Cohen[1]</p>
</blockquote>
<h2>ARIA Support</h2>
<p>ARIA support varies from browser to browser and from each assistive device to another. You can't query for if an ARIA role or attribute is supported (and often, ARIA standards are not specified for certain technologies), so the only thing you can do is test, test, test. This is another reason why no ARIA is usually better than bad ARIA.</p>
<h2>The 5 Rules of ARIA[6]</h2>
<ol>
<li>Don't use ARIA (if you can use <a href="semantic-html.html">HTML</a> instead)</li>
<li>Don't change native semantics. <em>The ARIA role will take precedence over the HTML, so don't mess with it.</em></li>
<li>All interactive ARIA roles need to be operable by keyboard. <em>If it is operated by mouse, it must also be operated by keyboard.</em></li>
<li>Don't use <code>role=&quot;presentation&quot;</code> or <code>aria-hidden=&quot;true&quot;</code> on visible focusable elements.</li>
<li>All interactive elements must have an accessible name.</li>
</ol>
<h2><a href="semantic-html.html">Semantic HTML</a></h2>
<blockquote>
<p>Developers should prefer using the correct semantic HTML element over using ARIA, if such an element exists. For instance, native elements have built-in keyboard accessibility, roles and states. <strong>However, if you choose to use ARIA, you are responsible for mimicking the equivalent browser behavior in script</strong>.[1]</p>
</blockquote>
<p>Here are some examples of <strong>bad</strong> ARIA usage:</p>
<pre><code class="language-html">&lt;div role=&quot;banner&quot;&gt;&lt;/div&gt;
&lt;div role=&quot;complementary&quot;&gt;&lt;/div&gt;
&lt;div role=&quot;form&quot;&gt;&lt;/div&gt;
&lt;div role=&quot;main&quot;&gt;&lt;/div&gt;
&lt;div role=&quot;navigation&quot;&gt;&lt;/div&gt;
&lt;div role=&quot;region&quot;&gt;&lt;/div&gt;
&lt;div role=&quot;contentinfo&quot;&gt;&lt;/div&gt;
&lt;div role=&quot;search&quot;&gt;&lt;/div&gt;
</code></pre>
<p>Here are those examples being used with <strong>good</strong> <a href="semantic-html.html">semantic HTML</a>:</p>
<pre><code class="language-html">&lt;header&gt;&lt;/header&gt;
&lt;aside&gt;&lt;/aside&gt;
&lt;form&gt;&lt;/form&gt;
&lt;main&gt;&lt;/main&gt;
&lt;nav&gt;&lt;/nav&gt;
&lt;section&gt;&lt;/section&gt;
&lt;!-- A section tag needs an accessible name to truly replace the &quot;region&quot; role  --&gt;
&lt;footer&gt;&lt;/footer&gt;
&lt;div role=&quot;search&quot;&gt;&lt;/div&gt;
&lt;!-- No associated semantic HTML --&gt;
</code></pre>
<p>Another example is anchor tags (<code>&lt;a href=&quot;#&quot;&gt;</code>) are handled differently than buttons (<code>&lt;button&gt;</code>), so adding a <code>role=&quot;button&quot;</code> to your <code>a</code> tag will result in unexpected behavior for the user, since they are expecting a <code>button</code> but the interaction is that of an <code>a</code> tag.</p>
<blockquote>
<p>At a certain point, HTML alone is not enough. And what I will say is that ARIA should never really be an option, but when it is, it's your only option.[2]</p>
</blockquote>
<h2><code>role=&quot;presentation&quot;</code> and <code>aria-hidden=&quot;true&quot;</code>[9]</h2>
<p>The <code>presentation</code> role strips the element of all semantics. So if a user can focus on that element, they will have no guidance on how to handle it or what it is for. For certain elements, this also strips all the semantics of the children (<code>&lt;ul&gt;</code> for example). For others, it does not (<code>&lt;a&gt;</code> or <code>&lt;button&gt;</code> will always be focusable and in the accessibility tree).</p>
<p>The <code>hidden</code> aria attribute does not affect whether or not it is visible, like a <code>display: none;</code> might in <a href="css.html">CSS</a>. It will still be rendered and visible for the user, so setting <code>hidden</code> won't provide the user with any additional help in understanding the element, nor will it hide it from them.</p>
<blockquote>
<p>When you’re thinking about removing an element from the accessibility tree first think, “Am I removing the element all together or just the semantics?”[9]</p>
</blockquote>
<h2>Accessible Names</h2>
<p>Adding accessible names will help give users of assistive devices context regarding certain elements. For instance, if there is a bare <code>&lt;input /&gt;</code>, the user will just be told there is an input. But with an accessible name, the user can be told what the input is for, what it relates to, etc.</p>
<p>Certain HTML elements provide these accessible names natively. For instance, the <code>&lt;input /&gt;</code> example is best served by using an associated <code>&lt;label&gt;</code> with the proper attributes.</p>
<p>While having a one-to-one programmatic label that is native to HTML is ideal, there are other methods.</p>
<h3><code>aria-labelledby</code></h3>
<p><code>aria-labelledby</code> references the ID of another DOM element, whose text content will be read to the user. This attribute also has the advantage of allowing that text content to be translatable by Google Translate and other services, and the ability to chain multiple IDs to compose a more complex label (e.g. <code>aria-labelledby=&quot;first-name last-name&quot;</code>).</p>
<h3><code>aria-label</code></h3>
<p>The value of the <code>aria-label</code> attribute will be read out as is and does not have the advantages of <code>aria-labelledby</code>.</p>
<h2>Accessible Roles[4]</h2>
<p>ARIA roles are the way you provide semantics to <a href="html.html">HTML</a> elements, and ultimately how an assistive device user will know what something is. They are a contract between you and the user. But while they are a contract, they do not provide interaction or functionality; they just tell the user what they can <em>expect</em> from a given element, and <strong>you</strong> must provide the functionality.</p>
<p><strong>You cannot make your own roles. You must use the ones in the ARIA standard.</strong></p>
<h3>Role Types</h3>
<h4>Widgets</h4>
<p>Widget roles are meant to indicate <strong>interactive</strong> elements. Interactive elements all require three different attributes: <code>name</code>, <code>role</code>, and <code>value</code>. This is because dynamic visual changes must be presented programmatically.</p>
<p>There are two types of widget roles: standalone and composite roles.</p>
<p>Standalone widgets exist on their own or as part of a composite. You can see the list of standalone widgets on the ARIA roles standard page[4], and they include <code>checkbox</code> and <code>link</code>.</p>
<p>Composite widgets are parent roles or containers for standalone widget roles. These have a <strong>very specific standard</strong> to adhere to. The relationships between parent and child roles are very specific and <strong>must</strong> be done a particular way. Some roles can <strong>only</strong> be used as a child to a specific composite role.</p>
<h4>Document Structure</h4>
<p>These are mostly non-interactive, but provide context like <code>heading</code>, <code>image</code>, <code>list</code>, and <code>table</code>. <a href="semantic-html.html">Semantic HTML</a> is often provided to handle this.</p>
<h4>Live Region</h4>
<p>Live region roles are pieces of content that are updated dynamically and announced by assistive technology. These don't need to be actively focused by a user in order to be announced. Some examples are chat logs, sports scores, and timers.</p>
<p>Certain elements are naturally live regions[11], like those with the <code>alert</code> role. Any element can be a live region by adding the <code>aria-live</code>[10] attribute and setting it to either <code>polite</code> or <code>assertive</code>. <code>polite</code> should always be used unless it is <strong>absolutely necessary</strong> that the user is interrupted to hear whatever is in the live region. This is very rare.</p>
<p>Some other attributes that are useful for customizing the behavior of live regions are <code>aria-atomic</code> and <code>aria-relevant</code>.</p>
<h4>Window</h4>
<p>Window roles are in-window pop-ups. They have special meaning in document structure, and so are not considered composite roles.</p>
<h3>Supported States and Properties[5]</h3>
<p>To find what kind of states and properties are allowed for a given <code>role</code> or semantic element, we can check out the definition of the role we are using and have that help inform our decision. For instance, for the <code>button</code> role, there are two supported states and properties: <code>aria-expanded</code> and <code>aria-pressed</code>. Each of these states have their own documentation which will provide the allowed values for their attributes. Then, using <a href="javascript.html">Javascript</a>, we can update these values in the markup in real time to help provide the accessibility that a given element requires.</p>
<h2>Testing Accessibility</h2>
<p>In the browser, there is a separate accessibility tree, similar to the DOM tree, that can be inspected in the devtools. While this is convenient, this is <strong>not</strong> enough to say that the site is truly accessible, and it must be tested using actual assisted devices.</p>
<h2>References</h2>
<ol>
<li><a href="https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA" target="_blank">https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA</a></li>
<li><a href="https://www.youtube.com/watch?v=O2F99bA32UU" target="_blank">Gerard K. Cohen's talk on the ARIA Spec for the Uninitiated</a>, and the blog post <a href="https://www.deque.com/blog/aria-spec-for-the-uninitiated-part-1/" target="_blank">part 1</a>, <a href="https://www.deque.com/blog/aria-spec-for-the-uninitiated-part-2/" target="_blank">part 2</a>, <a href="https://www.deque.com/blog/aria-spec-for-the-uninitiated-part-3/" target="_blank">part 3</a></li>
<li><a href="https://www.w3.org/TR/wai-aria/" target="_blank">Official ARIA Standard</a></li>
<li>ARIA Roles: <a href="https://www.w3.org/TR/wai-aria/#roles_categorization" target="_blank">standard</a>, <a href="https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles" target="_blank">MDN</a></li>
<li><a href="https://www.w3.org/TR/wai-aria/#x6-4-global-states-and-properties" target="_blank">Supported States and Properties</a>, and <a href="https://www.w3.org/TR/wai-aria/#role_definitions" target="_blank">Definition of roles and where you can find appropriate states and properties</a></li>
<li><a href="https://www.w3.org/TR/aria-in-html/#notes2" target="_blank">% rules of ARIA</a></li>
<li><a href="https://www.w3.org/WAI/ARIA/apg/" target="_blank">WAI-ARIA Authoring Practices</a></li>
<li><a href="https://yewtu.be/watch?v=qdB8SRhqvFc" target="_blank">ARIA, Accessibility APIs and coding like you give a damn! – Léonie Watson / Front-Trends 2015</a></li>
<li><a href="https://timwright.org/blog/2016/11/19/difference-rolepresentation-aria-hiddentrue/" target="_blank">role=&quot;presentation&quot; and aria-hidden=&quot;true&quot;</a></li>
<li><a href="https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-live" target="_blank"><code>aria-live</code></a></li>
<li><a href="https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Live_Regions#roles_with_implicit_live_region_attributes" target="_blank">ARIA live regions</a></li>
</ol>
<p class="last-modified">Last modified: 202401040446</p></article></main><footer><nav><p><a href="index.html">Home</a></p><ul><li>
Built using
<a href="http://codeberg.org/stringbone/swiki" rel="noopener"
>{{SWIKI}}</a
></li><li><a href="http://codeberg.org/stringbone/" rel="noopener"
>Codeberg</a
></li><li><a href="http://milofultz.com/" rel="noopener">milofultz.com</a></li><li><a href="https://merveilles.town/@milofultz" rel="me noopener"
>Mastodon</a
></li><li><a href="https://fediring.net/previous?host=milofultz.com">←</a>
&nbsp;<a href="https://fediring.net/">Fediring</a>
&nbsp;<a href="https://fediring.net/random">(random)</a>
&nbsp;<a href="https://fediring.net/next?host=milofultz.com">→</a></li></ul></nav></footer><script src="prism.js"></script></body></html>
